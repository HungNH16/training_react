import React, { Component } from 'react';
import ProductList from './../../components/PropductList/ProductList';
import ProductItem from './../../components/ProductItem/ProductItem';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {fetch_products_request, delete_products_request} from '../../actions/index';
class ListPage extends Component {
    componentDidMount() {
       this.props.fetchAllProducts();
    }
    onDelete = (id) => {
        this.props.onDeleteProducts(id);
    }
    render() {
        var {products} = this.props;
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <Link to='/product/add' className="btn btn-info" id="mr-10">Thêm Sản Phẩm</Link>
                <ProductList>
                    {this.showProducts(products)}
                </ProductList>
            </div>
        );
    }
    showProducts = (products) => {
        var result = null;
        if (products.length > 0) {
            result = products.map((product, index) => {
                return (<ProductItem
                    key={index}
                    product={product}
                    index={index}
                    onDelete={this.onDelete}
                />)
            })
        }
        return result;
    }
}

const mapStateToProps = state => {
    return {
        products: state.products
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllProducts: () => {
            dispatch(fetch_products_request());
        },
        onDeleteProducts: (id) => {
            dispatch(delete_products_request(id));
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(ListPage);
import { StyleSheet } from 'react-native';
// Theme config
import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
    checkboxLabel:{
        color: Colors.black,
        fontWeight: 'bold',
        fontSize: Fonts.size.medium,
    },
    container:{
        flexDirection: 'row',
    },
    checkboxBorder: {
        flexDirection: 'row',
        flex:1,
        justifyContent: 'flex-end',
    },
    Check:{
     width: 15,
    height: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.8,
    borderColor: Colors.blue,
    },
    Checkbtn:{
        paddingRight:7
    }
});
import React, { Component } from 'react';
import { Button } from 'reactstrap';
import "../fontawesome-free-5.7.2-web/css/all.css"
import { connect } from 'react-redux';
import * as actions from './../actions/index';
class TaskForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            status: false
        }
    }
    componentWillMount() {
        if (this.props.itemEdit) {
            this.setState({
                id: this.props.itemEdit.id,
                name: this.props.itemEdit.name,
                status: this.props.itemEdit.status
            });
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps && nextProps.itemEdit) {
            this.setState({
                id: nextProps.itemEdit.id,
                name: nextProps.itemEdit.name,
                status: nextProps.itemEdit.status
            });
        } else {
            this.onClear();
        }
    }
    onChange = (event) => {
        var target = event.target;
        var name = target.name;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }
    onCloseForm = () => {
        this.props.onCloseForm();
    }
    onSave = (event) => {
        event.preventDefault();
        this.props.onSaveTask(this.state);
        //Hủy và đóng form
        this.onClear();
        this.onCloseForm();
    }
    onClear = () => {
        this.setState({
            name: '',
            status: false
        });
    }
    render() {      
        var { id } = this.state;
        if(!this.props.isDisplayForm) return null;
        return (
            <div className="panel panel-warning">
                <div className="panel-heading">
                    <h3 className="panel-title">
                        {id !== '' ? 'Cập nhật công việc' : 'Thêm công việc'}
                        <span><i
                            className=" fa fa-times-circle text-right"
                            onClick={this.onCloseForm}>
                        </i></span>
                    </h3>
                </div>
                <div className="panel-body" id="form1">
                    <form onSubmit={this.onSave}>
                        <div className="form-group">
                            <label>Tên: </label>
                            <input type="text"
                                className=" form-control"
                                name="name"
                                value={this.state.name}
                                onChange={this.onChange}
                            />
                        </div>
                        <label>Trạng thái:</label>
                        <select className="form-control"
                            name="status"
                            value={this.state.status}
                            onChange={this.onChange}
                        >
                            <option value={true}>Kích Hoạt</option>
                            <option value={false}>Ẩn</option>
                        </select><br />
                        <div className="text-center">
                            <Button type="submit" className="btn btn-warning">
                                <span className="fa fa-plus mr-5"></span>Lưu Lại
                            </Button>&nbsp;
                             <Button
                                type="button"
                                className="btn btn-danger"
                                onClick={this.onClear}
                            >
                                <span className="fa fa-close mr-5"></span>Hủy Bỏ
                             </Button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isDisplayForm: state.isDisplayForm.form,
        itemEdit : state.itemEdit
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onSaveTask: (tasks) => {
            dispatch(actions.saveTask(tasks));
        },
        onCloseForm: () => {
            dispatch(actions.closeForm())
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TaskForm);
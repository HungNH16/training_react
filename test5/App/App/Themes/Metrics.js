import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');
const marginHorizontal = 20;

// Used via Metrics.baseMargin
const metrics = {
  marginHorizontal,
  marginVertical: 10,
  baseMargin: 10,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  containerWidth: width < height ? width - (marginHorizontal * 2) : height - (marginHorizontal * 2),
  navBarHeight: (Platform.OS === 'ios') ? 64 : 54,
  buttonRadius: 4,
  icons: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 45,
    xl: 50,
  },
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 200,
  },
};

export default metrics;

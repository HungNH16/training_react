/**
 * Vehicle Management saga
 */

import { put, takeLatest } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import { ADD_VEHICLE, LOAD_VEHICLE_REQUEST_ACTION } from '../constants'
import { addNewVehicleSuccess, addNewVehicleFailed, loadVehicleSuccessAction, loadVehicleFailureAction } from '../actions'
import vehicleSaga, { addVehicle, searchVehicle } from '../saga';

describe('addVehicle Saga', () => {
    let addVehicleGenerator;
    beforeEach(() => {
        addVehicleGenerator = addVehicle({ params: { page: 1 } });
        const selectDescriptor = addVehicleGenerator.next().value;
        expect(selectDescriptor).toMatchSnapshot();
    });

    it('addNewVehicleSuccess successfully', () => {
        const response = [
            {
                '3plId': '',
            },
        ];
        const putDescriptor = addVehicleGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(addNewVehicleSuccess(response)));
    });

    it('should call the addNewVehicleFailed action if the response errors', () => {
        const putDescriptor = addVehicleGenerator.throw({ response: { data: { errors: [] } } }).value;
        expect(putDescriptor).toEqual(put(addNewVehicleFailed([], [])));
    });
})


describe('searchVehicle Saga', () => {
    let searchVehicleGenerator;
    beforeEach(() => {
        searchVehicleGenerator = searchVehicle({ params: { page: 1 } });
        const pagination = { pagination: { currentPage: 1 } };
        const selectDescriptor = searchVehicleGenerator.next().value;
        expect(selectDescriptor).toMatchSnapshot();

        const callDescriptor = searchVehicleGenerator.next(fromJS(pagination)).value;
        expect(callDescriptor).toMatchSnapshot();
    });

    it('loadVehicleSuccessAction successfully', () => {
        const response = { "data": [{ "3plID": 22, "3plName": "Omer Fritsch", "licensePlate": "EH532H", "makeModel": { "make": "Wheeler", "model": "HOnda 6" }, "volume": { "length": 2.9, "width": 9.3, "height": 4.1 }, "weight": 8.5, "status": "1", "tags": ["west"], "characteristics": [""] }] };

        const putDescriptor = searchVehicleGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(loadVehicleSuccessAction(response)));
    });

    it('should call the loadVehicleFailureAction action if the response errors', () => {
        const putDescriptor = searchVehicleGenerator.throw().value;
        expect(putDescriptor).toEqual(put(loadVehicleFailureAction('internal server')));
    });
});

describe('vehicleSagaRoot Saga', () => {
    const vehicleSagaRoot = vehicleSaga();

    it('should start task to watch for LOAD_VEHICLE_REQUEST_ACTION action', () => {
        const takeLatestDescriptor = vehicleSagaRoot.next().value;
        expect(takeLatestDescriptor).toEqual(takeLatest(LOAD_VEHICLE_REQUEST_ACTION, searchVehicle));

        const takeLatestAddVehicle = vehicleSagaRoot.next().value;
        expect(takeLatestAddVehicle).toEqual(takeLatest(ADD_VEHICLE, addVehicle));
    });
});

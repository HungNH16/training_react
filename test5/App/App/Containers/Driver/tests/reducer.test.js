import { fromJS } from 'immutable';
import driverReducer, { initialState, initialForm } from '../reducer';
import {
    LOAD_DRIVER_REQUEST_ACTION,
    LOAD_DRIVER_SUCCESS_ACTION,
    LOAD_DRIVER_FAILURE_ACTION,
    ADD_DRIVER,
    ADD_DRIVER_SUCCESS,
    ADD_DRIVER_FAILED,
    UPDATE_INPUT,
    UPDATE_ERRORS,
    UPDATE_SUCCESS_FLAG,
    RESET_FORM,
} from '../constants';

describe('Driver Reducer', () => {
    it('returns the the state with default page = "1" in action LOAD_DRIVER_REQUEST_ACTION ', () => {
        const expectedState = fromJS(initialState.toJS()).setIn(['pagination', 'currentPage'], 1);
        expect(driverReducer(undefined, { type: LOAD_DRIVER_REQUEST_ACTION })).toEqual(expectedState);
    });

    it('returns the state with vehicle data & default table pagination in action LOAD_DRIVER_SUCCESS_ACTION ', () => {
        const drivers = [{}];
        const pagination = {
            currentPage: 0,
            perPage: 10,
            total: 1,
        };
        const actionData = {
            type: LOAD_DRIVER_SUCCESS_ACTION,
            data: {
                data: drivers,
                meta: {
                    pagination,
                },
            },
        };
        const expectedState = fromJS(initialState)
            .set('drivers', drivers)
            .set('pagination', fromJS(pagination));
        expect(driverReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('returns error message(s) & corresponding invalid field(s) in action LOAD_DRIVER_FAILURE_ACTION', () => {
        const input = {
            errors: undefined,
            invalidFields: fromJS([]),
        };
        const actionData = {
            type: LOAD_DRIVER_FAILURE_ACTION,
            ...input,
        };
        const expectedState = fromJS(initialState)
            .set('errors', input.errors)
            .set('invalidFields', input.invalidFields);
        expect(driverReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('resets success flag when requesting action ADD_DRIVER', () => {
        const expectedState = fromJS(initialState).set('addSuccessFlag', false);
        expect(driverReducer(undefined, { type: ADD_DRIVER })).toEqual(expectedState);
    });

    it('shows success popup by flag in action ADD_DRIVER_SUCCESS', () => {
        const driverID = 1;
        const actionData = {
            type: ADD_DRIVER_SUCCESS,
            response: {
                data: {
                    driverID,
                },
            },
        };
        const expectedState = fromJS(initialState).set('addSuccessFlag', !!driverID);
        expect(driverReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('returns error message(s) & corresponding invalid field(s) in action ADD_DRIVER_FAILED', () => {
        const input = {
            errors: ['Driver Name already existed.'],
            invalidFields: ['name'],
        };
        const actionData = {
            type: ADD_DRIVER_FAILED,
            ...input,
        };
        const expectedState = fromJS(initialState)
            .set('errors', input.errors)
            .set('invalidFields', input.invalidFields);
        expect(driverReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('adds new value for a field in action UPDATE_INPUT', () => {
        const input = {
            name: 'name',
            value: 'HungLN3',
        };
        const actionData = {
            type: UPDATE_INPUT,
            ...input,
        };
        const expectedState = fromJS(initialState).setIn(['formValues', 'name'], 'HungLN3');
        expectedState.setIn(['formValues', input.name], input.value).set('invalidFields', expectedState.get('invalidFields').filter(field => field !== input.name));
        expect(driverReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('store new error message(s) into state in action UPDATE_ERRORS', () => {
        const input = {
            errors: ['Please complete all required fields.'],
            invalidFields: ['name', 'phone'],
        };
        const actionData = {
            type: UPDATE_ERRORS,
            ...input,
        };
        const expectedState = fromJS(initialState)
            .set('errors', input.errors)
            .set('invalidFields', input.invalidFields);
        expect(driverReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('activates/deactivates success flag in action UPDATE_SUCCESS_FLAG', () => {
        const bool = true;
        const actionData = {
            type: UPDATE_SUCCESS_FLAG,
            bool,
        };
        const expectedState = fromJS(initialState).set('addSuccessFlag', bool);
        expect(driverReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('clears all values & errors in action RESET_FORM', () => {
        const input = {
            errors: [],
            invalidFields: [],
        };
        const actionData = {
            type: RESET_FORM,
        };
        const expectedState = fromJS(initialState)
            .set('formValues', fromJS(initialForm))
            .set('errors', input.errors)
            .set('invalidFields', input.invalidFields);
        expect(driverReducer(undefined, actionData)).toEqual(expectedState);
    });
});

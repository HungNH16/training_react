import React from 'react';
import { Image, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import * as flags from './flags';

const Flag = ({
  size,
  code,
  type,
  style,
}) => {
  const flag = flags[type][`icons${size}`][code];
  const unknownFlag = flags[type][`icons${size}`].unknown;

  return (
    <Image
      source={flag || unknownFlag}
      style={[{ width: size, height: size }, style]}
    />
  );
};

Flag.defaultProps = {
  size: 16,
  type: 'flat',
  style: null,
};

Flag.propTypes = {
  size: PropTypes.oneOf([16, 24, 32, 48, 64]),
  code: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['flat', 'shiny']),
  style: ViewPropTypes.style,
};

export default Flag;

import React, { Component } from 'react';
import './Style.css'
class Employee extends React.Component {
    render() {
        return (
            <li>
                <div><b>Fullname:</b></div>{this.props.fullName}
                <div><b>Gender:</b></div>{this.props.gender}
            </li>
        );
    }
}
class EmployeeList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [
                { empId: 1, fullName: "Hung", gender: "Male" },
                { empId: 2, fullName: "Trung", gender: "male" },
                { empId: 3, fullName: "Hieu", gender: "Male" }
            ]
        };
    }

    render() {
        // Array of <Employee>
        var listItems = this.state.employees.map(e => (
            <Employee fullName={e.fullName} gender={e.gender} key={e.empId} />
        ));
        return (
            <div>
                <ul className="employee-list">
                    {listItems}
                </ul>
            </div>
        );
    }
}

export default EmployeeList;
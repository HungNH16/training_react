import React, { Component } from 'react';
import { View } from 'react-native';
import { CheckBox } from 'react-native-elements';
import { Dimensions } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import styles from '../TourItem1/styles';

const { width, height } = Dimensions.get('window');

class Boxes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          name: 'Bulk Box 1',
          text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
        },
        {
          name: 'Bulk Box 2',
          text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
        },
        {
          name: 'Bulk Box 3',
          text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
        },
        {
          name: 'Bulk Box 4',
          text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
        },
        {
          name: 'Bulk Box 5',
          text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
        },
        {
          name: 'Bulk Box 6',
          text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
        },
        {
          name: 'Bulk Box 7',
          text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
        },
        {
          name: 'Bulk Box',
          text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
        },
      ],
      checked: []
    }
  }

  componentWillMount() {
    let { data, checked } = this.state;
    let intialCheck = data.map(x => false);
    this.setState({ checked: intialCheck })
  }

  handleChange = (index) => {
    let { checked } = [...this.state.checked];
    checked[index] = !checked[index];
    this.setState({ checked });
  }

  render() {
    let { data, checked } = this.state;
    return (
      <FlatList
        data={data}
        renderItem={({ item, index }) =>
          <CheckBox
            style={styles.checked}
            title={item.name}
            onPress={() => this.handleChange(index)}
            checked={checked[index]} />
        }
      />
    );
  }
}

export default Boxes;
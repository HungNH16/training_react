/**
 *
 * Private Layout
 *
 * This component defines the common layout for web user pages,
 * which need to check authorization before access
 */

// ===== IMPORTS =====
// Modules
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
// Custom Components
import Header from '../Header';
import Footer from '../Footer';
import Sidebar from '../SideBar';
import { isLoggedIn } from '../../../services/auth';
// Constants & Styles
import { ROUTE } from '../../constants';
import '../../scss/components/layouts/layouts.scss';
// ===================

// ===== MAIN COMPONENT =====
export default ({ component: Component, ...remain }) => (
    <Route
        {...remain}
        render={() =>
            isLoggedIn() ? (
                <div className="cf-main">
                    <div className="cf-header">
                        <Header />
                    </div>
                    <div className="cf-sidebar">
                        <Sidebar />
                    </div>
                    <div className="cf-content">
                        <Component />
                    </div>
                    <div className="cf-footer">
                        <Footer />
                    </div>
                </div>
            ) : (
                <Redirect to={ROUTE.USERS_LOGIN} />
            )
        }
    />
);
// ==========================

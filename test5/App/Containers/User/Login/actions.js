/*
 *
 * Login actions
 *
 */

import { DEFAULT_ACTION, SIGIN_ACTION, SIGIN_SUCCESS } from './constants';

export function defaultAction() {
    return {
        type: DEFAULT_ACTION,
    };
}

export function siginAction(email, password) {
    return {
        type: SIGIN_ACTION,
        email,
        password,
    };
}

export function siginedAction(data) {
    return {
        type: SIGIN_SUCCESS,
        data,
    };
}

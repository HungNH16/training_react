import React from 'react';
import { View, Image } from 'react-native';
// Styles
import styles from './styles';
// Images
import { Images } from '../../Themes';

const HeaderBar = () => (
  <View style={styles.container}>
    <Image style={styles.logoImage} source={Images.launch} />
  </View>
);

export default HeaderBar;

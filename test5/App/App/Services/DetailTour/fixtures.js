// GET /scheduled-tour-plans/{tourID}
export const fetchTour1 = () => ([
  {
    type: 'pick-up',
    location: {
      name: 'DairyFarm Nature Park 1, 1123, Quezon, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '10:00 - 11:30',
    name: 'Company ABC',
  },
]);

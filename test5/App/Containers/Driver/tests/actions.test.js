import {
    loadDriverAction,
    loadDriverSuccessAction,
    loadDriverFailureAction,
    addNewDriver,
    addNewDriverSuccess,
    addNewDriverFailed,
    updateInput,
    updateErrors,
    updateSuccessFlag,
    resetForm,
} from '../actions';
import {
    LOAD_DRIVER_REQUEST_ACTION,
    LOAD_DRIVER_SUCCESS_ACTION,
    LOAD_DRIVER_FAILURE_ACTION,
    ADD_DRIVER,
    ADD_DRIVER_SUCCESS,
    ADD_DRIVER_FAILED,
    UPDATE_INPUT,
    UPDATE_ERRORS,
    UPDATE_SUCCESS_FLAG,
    RESET_FORM,
} from '../constants';

describe('Drivers actions', () => {
    it('has a type of LOAD_DRIVER_REQUEST_ACTION', () => {
        const expected = {
            type: LOAD_DRIVER_REQUEST_ACTION,
        };
        expect(loadDriverAction()).toEqual(expected);
    });

    it('has a type of LOAD_DRIVER_SUCCESS_ACTION', () => {
        const data = {};
        const expected = {
            type: LOAD_DRIVER_SUCCESS_ACTION,
            data,
        };
        expect(loadDriverSuccessAction(data)).toEqual(expected);
    });

    it('has a type of LOAD_DRIVER_FAILURE_ACTION', () => {
        const data = {};
        const expected = {
            type: LOAD_DRIVER_FAILURE_ACTION,
            data,
        };
        expect(loadDriverFailureAction(data)).toEqual(expected);
    });

    it('has a type of ADD_DRIVER', () => {
        const data = {
            '3plID': 1,
            driverName: '30-N4-2310',
            contactNumber: {
                countryCode: '65',
                number: '12345678',
            },
            status: 1,
            tags: ['#honda', '#bike'],
        };
        const expected = {
            type: ADD_DRIVER,
            data,
        };
        expect(addNewDriver(data)).toEqual(expected);
    });

    it('has a type of ADD_DRIVER_SUCCESS', () => {
        const response = {
            data: {
                driverID: 1,
            },
        };
        const expected = {
            type: ADD_DRIVER_SUCCESS,
            response,
        };
        expect(addNewDriverSuccess(response)).toEqual(expected);
    });

    it('has a type of ADD_DRIVER_FAILED', () => {
        const response = {
            errors: ['Driver Name already existed.'],
            invalidFields: ['name'],
        };
        const expected = {
            type: ADD_DRIVER_FAILED,
            ...response,
        };
        expect(addNewDriverFailed(response.errors, response.invalidFields)).toEqual(expected);
    });

    it('has a type of UPDATE_INPUT', () => {
        const input = {
            name: 'name',
            value: 'HungLN3',
        };
        const expected = {
            type: UPDATE_INPUT,
            ...input,
        };
        expect(updateInput(input.name, input.value)).toEqual(expected);
    });

    it('has a type of UPDATE_ERRORS', () => {
        const input = {
            errors: ['Please complete all required fields.'],
            invalidFields: ['name', 'phone'],
        };
        const expected = {
            type: UPDATE_ERRORS,
            ...input,
        };
        expect(updateErrors(input.errors, input.invalidFields)).toEqual(expected);
    });

    it('has a type of UPDATE_SUCCESS_FLAG', () => {
        const bool = true;
        const expected = {
            type: UPDATE_SUCCESS_FLAG,
            bool,
        };
        expect(updateSuccessFlag(bool)).toEqual(expected);
    });

    it('has a type of RESET_FORM', () => {
        expect(resetForm()).toEqual({ type: RESET_FORM });
    });
});

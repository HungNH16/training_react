import { put, takeLatest, call } from 'redux-saga/effects';
// Actions, reducers
import TourActions, { TourTypes } from './reducer';
// Services
import TourApi from '../../Services/Tour';
import Logger from '../../Services/Logger';

function* fetchTour() {
  try {
    yield put(TourActions.setIsFetchingTour(true));
    const tour = yield call(TourApi.fetchTour);

    yield put(TourActions.setTour(tour));
  } catch (error) {
    Logger.log(error);
  } finally {
    yield put(TourActions.setIsFetchingTour(false));
  }
}

export default [
  takeLatest(TourTypes.FETCH_TOUR, fetchTour),
];

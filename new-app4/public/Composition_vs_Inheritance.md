# Cô lập hóa
Có những component không hề biết trước các component con của nó sẽ là gì, ví dụ như component Sidebar hoặc Dialog vốn đơn giản chỉ là những chiếc "hộp".

Facebook khuyến cáo những component kiểu này nên sử dụng prop đặc biệt children để truyền trực tiếp những phần tử con vào đầu ra:

Ví dụ với component FancyBorder và WelcomeDialog dưới đây:
```js
function FancyBorder(props) {
  return (
    <div className={'FancyBorder FancyBorder-' + props.color}>
      {props.children}
    </div>
  );
}
function WelcomeDialog() {
  return (
    <FancyBorder color="blue">
      <h1 className="Dialog-title">
        Welcome
      </h1>
      <p className="Dialog-message">
        Thank you for visiting our spacecraft!
      </p>
    </FancyBorder>
  );
}
```
# Chuyên môn hóa
Cũng có lúc ta coi component này là trường hợp đặc biệt của component kia. Ví dụ, ta có thể nói WelcomeDialog là trường hợp đặc biệt của Dialog.

Trong React, việc này đạt được thông qua một kỹ thuật tên là "composition". Kỹ thuật này giúp một component "đặc biệt" (chính là WelcomeDialog) render một component "tổng quát" (chính là Dialog), cấu hình component tổng quát này với props:
```js
function Dialog(props) {
  return (
    <FancyBorder color="blue">
      <h1 className="Dialog-title">
        {props.title}
      </h1>
      <p className="Dialog-message">
        {props.message}
      </p>
    </FancyBorder>
  );
}

function WelcomeDialog() {
  return (
    <Dialog
      title="Welcome"
      message="Thank you for visiting our spacecraft!" />
  );
}
```
Kỹ thuật "composition" kết hợp hoàn hảo với các component dạng class:
```js
function Dialog(props) {
  return (
    <FancyBorder color="blue">
      <h1 className="Dialog-title">
        {props.title}
      </h1>
      <p className="Dialog-message">
        {props.message}
      </p>
      {props.children}
    </FancyBorder>
  );
}

class SignUpDialog extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSignUp = this.handleSignUp.bind(this);
    this.state = {login: ''};
  }

  render() {
    return (
      <Dialog title="Mars Exploration Program"
              message="How should we refer to you?">
        <input value={this.state.login}
               onChange={this.handleChange} />
        <button onClick={this.handleSignUp}>
          Sign Me Up!
        </button>
      </Dialog>
    );
  }

  handleChange(e) {
    this.setState({login: e.target.value});
  }

  handleSignUp() {
    alert(`Welcome aboard, ${this.state.login}!`);
  }
}
```
# Vậy còn kỹ thuật "Inheritance" thì sao?
Tại Facebook, chúng tôi sử dụng React trong hàng ngàn components, nhưng chưa tìm thấy trường hợp nào cần tạo component thông qua kỹ thuật "inheritance hierarchies".

Props và kỹ thuật "composition" đã cho ta mọi sự linh hoạt cần thiết để tùy biến một component, cả vẻ bề ngoài lẫn khả năng tương tác, vừa tường minh, vừa an toàn. Hãy nhớ rằng component có thể nhập props bất kỳ:
* Một giá trị dạng primitive JS (như number, string, object, v.v.)
* Một React element
* Hay là một function

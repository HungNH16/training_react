import {
    addRow,
    deleteRow,
    loadDriverData,
    loadDriverDataSuccess,
    loadVehicleAssignmentData,
    loadVehicleAssignmentDataSuccess,
    loadVehicleData,
    loadVehicleDataSuccess,
    resetState,
    revertData,
    submitVehicleAssignmentData,
    submitVehicleAssignmentDataSuccess,
    updateDriver,
    updateErrors,
    updateStatus,
    updateSuccessFlag,
    updateVehicle,
} from '../actions';
import {
    ADD_ROW,
    DELETE_ROW,
    LOAD_DATA,
    LOAD_DATA_SUCCESS,
    LOAD_DRIVER,
    LOAD_DRIVER_SUCCESS,
    LOAD_VEHICLE,
    LOAD_VEHICLE_SUCCESS,
    RESET_STATE,
    REVERT_DATA,
    SUBMIT_DATA,
    SUBMIT_DATA_SUCCESS,
    UPDATE_DRIVER,
    UPDATE_ERRORS,
    UPDATE_STATUS,
    UPDATE_SUCCESS_FLAG,
    UPDATE_VEHICLE,
} from '../constants';

describe('Vehicle Assignment actions', () => {
    it('has a type of ADD_ROW', () => {
        const expected = {
            type: ADD_ROW,
        };
        expect(addRow()).toEqual(expected);
    });

    it('has a type of DELETE_ROW', () => {
        const id = 1;
        const expected = {
            type: DELETE_ROW,
            id,
        };
        expect(deleteRow(id)).toEqual(expected);
    });

    it('has a type of LOAD_DRIVER', () => {
        const expected = {
            type: LOAD_DRIVER,
        };
        expect(loadDriverData()).toEqual(expected);
    });

    it('has a type of LOAD_DRIVER_SUCCESS', () => {
        const response = {
            data: [],
        };
        const expected = {
            type: LOAD_DRIVER_SUCCESS,
            response,
        };
        expect(loadDriverDataSuccess(response)).toEqual(expected);
    });

    it('has a type of LOAD_DATA', () => {
        const expected = {
            type: LOAD_DATA,
        };
        expect(loadVehicleAssignmentData()).toEqual(expected);
    });

    it('has a type of LOAD_DATA_SUCCESS', () => {
        const response = {
            data: [],
        };
        const expected = {
            type: LOAD_DATA_SUCCESS,
            response,
        };
        expect(loadVehicleAssignmentDataSuccess(response)).toEqual(expected);
    });

    it('has a type of LOAD_VEHICLE', () => {
        const expected = {
            type: LOAD_VEHICLE,
        };
        expect(loadVehicleData()).toEqual(expected);
    });

    it('has a type of LOAD_VEHICLE_SUCCESS', () => {
        const response = {
            data: [],
        };
        const expected = {
            type: LOAD_VEHICLE_SUCCESS,
            response,
        };
        expect(loadVehicleDataSuccess(response)).toEqual(expected);
    });

    it('has a type of RESET_STATE', () => {
        const expected = {
            type: RESET_STATE,
        };
        expect(resetState()).toEqual(expected);
    });

    it('has a type of REVERT_DATA', () => {
        const expected = {
            type: REVERT_DATA,
        };
        expect(revertData()).toEqual(expected);
    });

    it('has a type of SUBMIT_DATA', () => {
        const data = {
            status: 'Published',
            details: [
                {
                    driverID: 'DR000001',
                    driverStatus: 4,
                    vehicleLicensePlate: 'LP000001',
                    vehicleStatus: 4,
                },
            ],
        };
        const expected = {
            type: SUBMIT_DATA,
            data,
        };
        expect(submitVehicleAssignmentData(data)).toEqual(expected);
    });

    it('has a type of SUBMIT_DATA_SUCCESS', () => {
        const response = {
            data: {
                categoryID: 1,
            },
        };
        const expected = {
            type: SUBMIT_DATA_SUCCESS,
            response,
        };
        expect(submitVehicleAssignmentDataSuccess(response)).toEqual(expected);
    });

    it('has a type of RESET_STATE', () => {
        const expected = {
            type: RESET_STATE,
        };
        expect(resetState()).toEqual(expected);
    });

    it('has a type of UPDATE_DRIVER', () => {
        const rowId = 1;
        const value = { label: 'Driver 1', value: 'DR000001', status: 2 };
        const expected = {
            type: UPDATE_DRIVER,
            rowId,
            value,
        };
        expect(updateDriver(rowId, value)).toEqual(expected);
    });

    it('has a type of UPDATE_ERRORS', () => {
        const errors = [];
        const invalidFields = [];
        const expected = {
            type: UPDATE_ERRORS,
            errors,
            invalidFields,
        };
        expect(updateErrors(errors, invalidFields)).toEqual(expected);
    });

    it('has a type of UPDATE_STATUS', () => {
        const rowId = 1;
        const name = 'driverStatus';
        const value = 4;
        const expected = {
            type: UPDATE_STATUS,
            rowId,
            name,
            value,
        };
        expect(updateStatus(rowId, name, value)).toEqual(expected);
    });

    it('has a type of UPDATE_SUCCESS_FLAG', () => {
        const bool = false;
        const expected = {
            type: UPDATE_SUCCESS_FLAG,
            bool,
        };
        expect(updateSuccessFlag(bool)).toEqual(expected);
    });

    it('has a type of UPDATE_VEHICLE', () => {
        const rowId = 1;
        const value = { label: 'A1A123 - Audi Sedan', value: 'A1A123', status: 4 };
        const expected = {
            type: UPDATE_VEHICLE,
            rowId,
            value,
        };
        expect(updateVehicle(rowId, value)).toEqual(expected);
    });
});

import { createStore, applyMiddleware } from 'redux';
import Reducer from '../reducer/Reducer';
import createMiddleware from 'redux-saga'

const defaultState = {
    left_item: [
        { id: 1, category: 'sport', price: 500, name: 'football', quantity: 0, },
        { id: 2, category: 'sport', price: 300, name: 'baseball', quantity: 0, },
        { id: 3, category: 'sport', price: 100, name: 'basketball', quantity: 0, },
        { id: 4, category: 'electronics', price: 400, name: 'iphone7', quantity: 0, },
        { id: 5, category: 'electronics', price: 200, name: 'ipod', quantity: 0,},
    ],
    list_item: [],
    total:0,
    user:'',
    isLogin:false,
    isPaying:false,
    isErr:false
}
const Middleware = createMiddleware()
const Store = createStore(Reducer, defaultState, applyMiddleware(Middleware));
export default Store;
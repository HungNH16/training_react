import { StyleSheet } from 'react-native';

import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  button: {
    width: '100%',
    height: 30,
    backgroundColor: Colors.main,
  },
  text: {
    textAlign: 'center',
    lineHeight: 30,
    fontWeight: 'bold',
    fontSize: Fonts.size.small,
    color: Colors.white,
  },
});

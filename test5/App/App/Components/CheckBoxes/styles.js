import { StyleSheet } from 'react-native';
// Theme config
import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkboxBorder: {
    width: 15,
    height: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.8,
    borderColor: Colors.white,
  },
  name:{
    color: Colors.black,
    fontWeight: 'bold',
    fontSize: Fonts.size.medium, 
  },
  container:{
      paddingTop:10
  }
});

import { NOT_EMPTY } from '../../Services/Validator';

export const loginSchema = {
  login: [NOT_EMPTY],
  password: [NOT_EMPTY],
};

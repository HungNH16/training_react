import React from 'react';
import ProductList from './ProductList';
import { shallow, mount } from '../../enzyme';

const items = 
  {
    id: 1,
    name: "one2222",
    price: "1000",
    status: false
  }
describe('...', () => {
  test('shopping', () => {
    //const wrapper = shallow(<ProductList {...items} />)
     expect(items.price).toBe("1000");
    expect(items.name).toBe("one2222");
  })
});

const shop = [
  'iphone5',
  'ipad4',
  'beer',
  'dia',
];
test('shopping',() => {
  expect(shop).toContain('beer');
});

describe('list test', () => {
  const item = [
    1,
    1,
    "one2222",
    "1000",
    false,
    "Click"
  ];
  const wrapper = shallow(<ProductList item={item} />);
  expect(wrapper.find('th')).toHaveLength(item.length);
});

describe('ware',() => {
  const ware = [1,2,3];
  expect(ware).toHaveLength(3);
});
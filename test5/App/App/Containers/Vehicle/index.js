/**
 *
 * Vehicles Management
 *
 * This screen manages available vehicles of all 3PL accounts,
 * including their statistics & current working status.
 */
// ===== IMPORTS =====
// Modules
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Input, Row, Col, Container } from 'reactstrap';
import { injectIntl } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { Table, IconAction, Pagination, Button } from 'cf-web-ui-kit-components';
import { isEmpty as _isEmpty, uniq as _uniq, get as _get, isEqual as _isEqual } from 'lodash';
// Custom Components

import { injectSaga, injectReducer, validations, alphabeticalSort } from '../../utils';
import makeSelectVehicle, { selectFormValues, selectErrors, selectInvalidFields, selectSuccessFlag } from './selectors';
import reducer from './reducer';
import messages from './messages';
import saga from './saga';
import { addNewVehicle, updateSuccessFlag, resetForm, updateInput, updateErrors, loadVehicleAction, resetState } from './actions';
import AddNewButton from '../../components/TableProperties/AddNewButton';
// Constants & Styles
import { MSG, FORM, LIST, ROUTE } from '../../constants';
import '../../scss/containers/common.scss';
import '../../scss/containers/vehicles.scss';
// ===================

// ===== MAIN COMPONENT =====
export class VehiclesManagement extends React.PureComponent {
    constructor(props) {
        super(props);

        this.updateFieldMap = this.updateFieldMap.bind(this);
        this.validateAddForm = this.validateAddForm.bind(this);
        this.getAddRequest = this.getAddRequest.bind(this);
        this.handleSubmitAdd = this.handleSubmitAdd.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handlePaging = this.handlePaging.bind(this);
    }

    componentDidMount() {
        const {
            vehicle: {
                pagination: { currentPage },
            },
        } = this.props;
        this.props.searchAction({ q: '', page: currentPage });
        // Create add-new form configuration
        this.updateFieldMap();
    }

    componentDidUpdate(prevProps) {
        if (!_isEqual(_get(prevProps, 'vehicle.typeOptions'), _get(this.props, 'vehicle.typeOptions'))) {
            // Create add-new form configuration
            this.updateFieldMap();
        }
    }

    componentWillUnmount() {
        this.props.onResetState();
    }

    handleSearch = () => {
        const {
            vehicle: {
                pagination: { currentPage },
            },
        } = this.props;
        this.props.searchAction({ page: currentPage });
    };

    handlePaging = pageNumber => {
        const {
            history: { push },
        } = this.props;
        push(`${ROUTE.FLEET_VEHICLES}?page=${pageNumber}`);
        this.props.searchAction({ page: pageNumber });
    };

    updateFieldMap() {
        const {
            vehicle: { typeOptions },
        } = this.props;
        this.updatedFieldMap = FORM.VEHICLE_FORM.map(field => {
            const newField = { ...field };

            if (newField.name === 'status') {
                newField.options = newField.options.filter(opt => ['4', '5'].includes(opt.value));
            } else if (newField.name === 'type') {
                newField.options = alphabeticalSort(typeOptions, ['label']);
            }

            return newField;
        });
    }

    validateAddForm() {
        const { isRequired, isMaxlength, isTagValid, isMaxNumber, isMinNumber } = validations;
        const {
            formValues: { license, type, length, width, height, weight, status, tags, maximumWorkingHours },
        } = this.props;
        return {
            ...isRequired([
                { name: 'license', value: license },
                { name: 'type', value: type },
                { name: 'length', value: length },
                { name: 'width', value: width },
                { name: 'height', value: height },
                { name: 'weight', value: weight },
                { name: 'status', value: status },
                { name: 'maximumWorkingHours', value: maximumWorkingHours },
            ]),
            ...isMaxlength({ name: 'license', label: 'License Plate', value: license, max: 20 }),
            ...isMaxNumber({ name: 'length', label: 'Length', value: length, max: 99999.99 }),
            ...isMinNumber({ name: 'length', label: 'Length', value: length, min: 0.01 }),
            ...isMaxNumber({ name: 'width', label: 'Width', value: width, max: 99999.99 }),
            ...isMinNumber({ name: 'width', label: 'Width', value: width, min: 0.01 }),
            ...isMaxNumber({ name: 'height', label: 'Height', value: height, max: 99999.99 }),
            ...isMinNumber({ name: 'height', label: 'Height', value: height, min: 0.01 }),
            ...isMaxNumber({ name: 'weight', label: 'Weight', value: weight, max: 99999.99 }),
            ...isMinNumber({ name: 'weight', label: 'Weight', value: weight, min: 0.01 }),
            ...isMaxNumber({ name: 'maximumWorkingHours', label: 'Maximum Working Hours', value: maximumWorkingHours, max: 24 }),
            ...isMinNumber({ name: 'maximumWorkingHours', label: 'Maximum Working Hours', value: maximumWorkingHours, min: 1 }),
            ...isTagValid({ name: 'tags', value: tags }),
        };
    }

    getAddRequest() {
        const {
            formValues: { license, type, length, width, height, weight, status, tags, characteristics, maximumWorkingHours },
        } = this.props;
        return {
            licensePlate: license,
            type: type.value,
            volume: {
                length: Number(length),
                width: Number(width),
                height: Number(height),
            },
            weight: Number(weight),
            status: status.value,
            tags: tags.split(/,[ ]*/),
            characteristics: [characteristics.value],
            maximumWorkingHours: Number(maximumWorkingHours),
        };
    }

    handleSubmitAdd() {
        const { onUpdateErrors, onAddNewVehicle } = this.props;
        const errors = this.validateAddForm();

        if (!_isEmpty(errors)) {
            onUpdateErrors(_uniq(Object.values(errors)), Object.keys(errors));
        } else {
            onUpdateErrors([], []);
            onAddNewVehicle(this.getAddRequest());
        }
    }

    render() {
        const {
            vehicle: { vehicles, pagination },
            intl: { formatMessage },
            formValues,
            errors,
            invalidFields,
            addSuccessFlag,
            onUpdateInput,
            onResetForm,
            onUpdateSuccessFlag,
        } = this.props;
        const columns = [
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.licensePlate),
                        accessor: 'licensePlate',
                        width: 125,
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.vehicleMake),
                        accessor: 'makeModel',
                        Cell: original => original.row.makeModel.make,
                        width: 120,
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.modelType),
                        accessor: 'makeModel',
                        Cell: original => original.row.makeModel.model,
                        width: 110,
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.volume),
                        accessor: 'volume',
                        Cell: original => [original.row.volume.length, original.row.volume.width, original.row.volume.height].join(', '),
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.weight),
                        accessor: 'weight',
                        width: 80,
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.status),
                        accessor: 'status',
                        Cell: props => (LIST.VEHICLE_STATUSES.find(opt => opt.value === props.value) || {}).label,
                        width: 120,
                        sortable: true,
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.tags),
                        accessor: 'tags',
                        Cell: original => {
                            const tags = [];
                            original.row.tags.map(item => (item ? tags.push(`#${item}`.replace('##', '#')) : ''));
                            return tags.join(', ');
                        },
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.characteristics),
                        accessor: 'characteristics',
                        Cell: original => {
                            const characteristics = _get(original, 'row.characteristics', []);
                            return !_isEmpty(characteristics) && !_isEmpty(characteristics.filter(item => !!item)) ? original.row.characteristics.join(', ') : '-';
                        },
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.action),
                        Cell: props => (
                            <div className="d-flex">
                                {icons.map(item => (
                                    <div className="mr-2">
                                        <IconAction
                                            icon={item.icon}
                                            action={() => {
                                                const {
                                                    original: { vehicleId },
                                                } = props;
                                                item.action(vehicleId);
                                            }}
                                            key={item.name + props.original.vehicleId}
                                        />
                                    </div>
                                ))}
                            </div>
                        ),
                        accessor: 'action',
                        className: 'd-none d-md-block',
                        headerClassName: 'd-none d-md-block',
                        sortable: false,
                        width: 120,
                    },
                ],
            },
        ];
        const deleteItem = id => id;
        const editItem = id => id;

        const icons = [
            {
                name: 'edit',
                icon: <i className="fas fa-pencil-alt" />,
                action: editItem,
            },
            {
                name: 'delete',
                icon: <i className="fas fa-times" />,
                action: deleteItem,
            },
        ];

        const initialState = {
            data: vehicles,
            currentPage: pagination.currentPage,
        };

        return (
            <Fragment>
                <Helmet>
                    <title>{formatMessage(MSG.VEHICLE_PAGE_TITLE)}</title>
                </Helmet>
                <Row className="row d-flex justify-content-center align-items-center">
                    <Col>
                        <Container>
                            <Row>
                                <Col>
                                    <h3>
                                        <b> {formatMessage(MSG.VEHICLE_TABLE_HEADER)} </b>
                                    </h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Input type="text" className="form-control" name="searchbox" placeholder={formatMessage(MSG.COMMON_INPUT_SEARCH_PLACEHOLDER)} />
                                </Col>
                                <Col>
                                    <Button onClick={() => this.handleSearch()}>{formatMessage(MSG.COMMON_BUTTON_SEARCH_BAR)}</Button>
                                </Col>
                            </Row>
                            <Row className="mt-2 text-md-right text-sm-left">
                                <Col>
                                    <div className="d-inline-block">
                                        <AddNewButton
                                            title={formatMessage(MSG.VEHICLE_POPUP_ADD_NEW_TITLE)}
                                            fieldMap={this.updatedFieldMap}
                                            formValues={formValues}
                                            errors={errors}
                                            invalidFields={invalidFields}
                                            handleChangeInput={onUpdateInput}
                                            handleResetForm={onResetForm}
                                            handleSubmit={this.handleSubmitAdd}
                                            addSuccessFlag={addSuccessFlag}
                                            handleCloseSuccessPopup={() => {
                                                onUpdateSuccessFlag(false);
                                                this.handleSearch();
                                            }}
                                            successMsg={formatMessage(MSG.VEHICLE_POPUP_MESSAGE_ADD_SUCCESS)}
                                        />
                                    </div>
                                    <div className="ml-3 d-inline-block d-md-none">
                                        <div className="table-toolbar-button">
                                            <div className="icon">
                                                <i className="fas fa-times" />
                                            </div>
                                            <span className="ml-2">{formatMessage(MSG.COMMON_BUTTON_DELETE_ABOVE_TABLE)}</span>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col>
                                    <Table
                                        className="none-headergroup"
                                        columns={columns}
                                        data={initialState.data}
                                        onPaging={() => {}}
                                        showPagination
                                        pageSizeOptions={[5, 10, 20, 25, 50, 100]}
                                        defaultPageSize={pagination.perPage}
                                        noDataText={formatMessage(MSG.NO_DATA_TEXT)}
                                        PaginationComponent={() => (
                                            <Pagination itemsCountPerPage={pagination.perPage} totalItemCount={pagination.total} currentPage={pagination.currentPage} goToPage={this.handlePaging} />
                                        )}
                                    />
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                </Row>
            </Fragment>
        );
    }
}
// ==========================

// ===== PROP TYPES =====
VehiclesManagement.propTypes = {
    intl: PropTypes.object,
    formValues: PropTypes.object,
    errors: PropTypes.arrayOf(PropTypes.string),
    invalidFields: PropTypes.arrayOf(PropTypes.string),
    addSuccessFlag: PropTypes.bool,
    onUpdateInput: PropTypes.func,
    onResetForm: PropTypes.func,
    onUpdateSuccessFlag: PropTypes.func,
    onUpdateErrors: PropTypes.func,
    onAddNewVehicle: PropTypes.func,
    history: PropTypes.object,
    searchAction: PropTypes.func.isRequired,
    vehicle: PropTypes.object,
    onResetState: PropTypes.func,
};
// ======================

// ===== INJECTIONS =====
const mapStateToProps = createStructuredSelector({
    vehicle: makeSelectVehicle(),
    formValues: selectFormValues,
    errors: selectErrors,
    invalidFields: selectInvalidFields,
    addSuccessFlag: selectSuccessFlag,
});

const mapDispatchToProps = dispatch => ({
    onAddNewVehicle: params => dispatch(addNewVehicle(params)),
    onUpdateSuccessFlag: bool => dispatch(updateSuccessFlag(bool)),
    onResetForm: () => dispatch(resetForm()),
    onUpdateErrors: (errors, invalidFields) => dispatch(updateErrors(errors, invalidFields)),
    onUpdateInput: (name, value) => dispatch(updateInput(name, value)),
    searchAction: params => dispatch(loadVehicleAction(params)),
    onResetState: () => dispatch(resetState()),
});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'vehicle', reducer });
const withSaga = injectSaga({ key: 'vehicle', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(injectIntl(withRouter(VehiclesManagement)));

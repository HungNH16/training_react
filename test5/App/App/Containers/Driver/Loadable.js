/**
 *
 * Asynchronously loads the component for Driver
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));

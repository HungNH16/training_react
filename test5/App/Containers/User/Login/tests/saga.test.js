/**
 * Test sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { takeLatest, call } from 'redux-saga/effects';
import sagaHelper from 'redux-saga-testing';
import loginSaga, { loginCognito } from '../saga';
import { SIGIN_ACTION } from '../constants'
import { authenticateUser } from '../../../../../services/auth'


describe('Testing loginSaga Saga is called', () => {
    const it = sagaHelper(loginSaga());
    it('should takeEvery action SIGIN_ACTION', (result) => {
        expect(result).toEqual(takeLatest(SIGIN_ACTION, loginCognito));
    });
});


describe('loginCognito Saga', () => {
    const mockAction = {
        type: SIGIN_ACTION,
        email: 'vietnv',
        password: '234234324',
    };
    const generator = loginCognito(mockAction);
    it('should call loginCognito', () => {
        expect(generator.next().value).toEqual(call(authenticateUser, mockAction.email, mockAction.password));
    });
});

export const getTourState = state => (state.tour);
export const getTour = state => getTourState(state).tour;
export const getIsFetchingTour = state => getTourState(state).isFetchingTour;

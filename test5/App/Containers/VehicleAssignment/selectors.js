/**
 *
 * Vehicle Assignment Selectors
 *
 * This contains Redux state selectors for Vehicle Assignment screen
 */
// ===== IMPORTS =====
// Utilities, Constants & Styles
import { createSelector } from 'reselect';
import { initialState } from './reducer';
import { VEHICLE_ASSIGNMENT_DOMAIN } from './constants';
// ===================

// ===== SELECTORS =====
const selectVehicleAssignmentDomain = state => state.get(VEHICLE_ASSIGNMENT_DOMAIN, initialState);

const selectData = createSelector(selectVehicleAssignmentDomain, substate => substate.toJS().data);
const selectDrivers = createSelector(selectVehicleAssignmentDomain, substate => substate.toJS().drivers);
const selectErrors = createSelector(selectVehicleAssignmentDomain, substate => substate.toJS().errors);
const selectInvalidFields = createSelector(selectVehicleAssignmentDomain, substate => substate.toJS().invalidFields);
const selectOriginalValues = createSelector(selectVehicleAssignmentDomain, substate => substate.toJS().originalValues);
const selectRowValues = createSelector(selectVehicleAssignmentDomain, substate => substate.toJS().rowValues);
const selectSuccessFlag = createSelector(selectVehicleAssignmentDomain, substate => substate.toJS().successFlag);
const selectVehicles = createSelector(selectVehicleAssignmentDomain, substate => substate.toJS().vehicles);
// =====================

export { selectVehicleAssignmentDomain, selectData, selectDrivers, selectErrors, selectInvalidFields, selectOriginalValues, selectRowValues, selectSuccessFlag, selectVehicles };

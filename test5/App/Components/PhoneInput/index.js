import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import FAI from 'react-native-vector-icons/FontAwesome5';
import ActionSheet from 'react-native-actionsheet';
import PropTypes from 'prop-types';
// User defined component
import Input from '../Input';
import Flag from '../Flag';
// Styles
import styles from './styles';

const areaCodeList = ['+63', '+65', '+84'];
const areaCodeShortName = ['PH', 'SG', 'VN'];
const areaCodeLabel = ['Philippines', 'Singapore', 'Vietnam', 'Cancel'];

class PhoneInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      areaCode: areaCodeList[0],
      shortName: areaCodeShortName[0],
    };
    this.onChangeText = this.onChangeText.bind(this);
    this.showActionSheet = this.showActionSheet.bind(this);
    this.onActionSheetPressed = this.onActionSheetPressed.bind(this);
  }

  onChangeText(value) {
    const { onChangeText } = this.props;
    const { areaCode } = this.state;

    onChangeText({ areaCode, value });
  }

  onActionSheetPressed(index) {
    if (areaCodeList[index]) {
      this.setState({ areaCode: areaCodeList[index], shortName: areaCodeShortName[index] });
    }
  }

  showActionSheet() {
    this.ActionSheet.show();
  }

  render() {
    const { areaCode, shortName } = this.state;

    return (
      <View style={styles.phoneInput}>
        <TouchableOpacity onPress={this.showActionSheet} style={styles.countryCode}>
          <Flag size={24} code={shortName} />
          <FAI name="caret-down" size={14} />
          <Text style={styles.contryCodeText}>{areaCode}</Text>
        </TouchableOpacity>
        <Input
          {...this.props}
          style={styles.input}
          onChangeText={this.onChangeText}
        />
        <ActionSheet
          ref={(actionSheetRef) => { this.ActionSheet = actionSheetRef; }}
          title="Area code"
          options={areaCodeLabel}
          cancelButtonIndex={(areaCodeLabel.length - 1)}
          onPress={this.onActionSheetPressed}
        />
      </View>
    );
  }
}

PhoneInput.propTypes = {
  onChangeText: PropTypes.func.isRequired,
};

export default PhoneInput;

/* eslint-disable import/no-duplicates */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, FlatList, View, ScrollView } from 'react-native';
import styles from '../../Components/TourItem2/styles';
// compornent
import Boxes from '../Boxes';
import Retailer from '../Retailer/index'
import { getRetailer } from '../../Modules/Retailer/selectors';

import RetailerActions from '../../Modules/Retailer/reducer';

import { connect } from 'react-redux';

/* eslint-disable */
class TourItem2 extends Component {
    componentDidMount() { 
        this.props.fetchRetailer();
      }
    render() {
        const {Retailer1} = this.props;
        return (
            <View style={styles.TourItem2}>
                <ScrollView style={styles.container}>
                {
                 Retailer1.map((item, index) => {
                                    return <Retailer key={index} {...item} />
                                })
                            }
                            <Boxes />
                </ScrollView>
            </View>
        )
    }
}

TourItem2.propTypes = {
    Retailer1: PropTypes.array.isRequired,
};
const mapStateToProps = state => ({
    Retailer1: getRetailer(state),
  });
  const mapDispatchToProps = dispatch => ({
    fetchRetailer: () => dispatch(RetailerActions.loadRetailer()),
  })
  export default connect(mapStateToProps, mapDispatchToProps)(TourItem2);
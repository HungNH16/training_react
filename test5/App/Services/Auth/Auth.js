import jwtDecode from 'jwt-decode';
import { AsyncStorage } from 'react-native';
import Secrets from 'react-native-config';
import { CognitoUserPool, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';

export const COGNITO_TOKEN_KEY = 'COGNITO_ACCESS_TOKEN';

export function authenticateUser(email, password) {
  const userPool = new CognitoUserPool({
    UserPoolId: Secrets.COGNITO_POOL_ID,
    ClientId: Secrets.COGNITO_CLIENT_ID,
  });
  const authenticationDetails = new AuthenticationDetails({
    Username: email,
    Password: password,
  });
  const cognitoUser = new CognitoUser({
    Username: email,
    Pool: userPool,
  });

  return new Promise((resolve, reject) => {
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: (result) => {
        const token = result.getIdToken().getJwtToken();
        resolve({ token });
      },
      onFailure: reject,
    });
  });
}

export const getUserToken = async () => {
  const token = await AsyncStorage.getItem(COGNITO_TOKEN_KEY);

  return token;
};

export const setUserToken = (token) => {
  AsyncStorage.setItem(COGNITO_TOKEN_KEY, token);
};

export const getUser = async () => {
  const token = await getUserToken();

  return !token ? null : jwtDecode(token);
};

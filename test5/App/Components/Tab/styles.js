import { StyleSheet } from 'react-native';
// Share styles
import { Fonts, Colors } from '../../Themes';

export default StyleSheet.create({
  tab: {
    flex: 1,
    height: 30,
    backgroundColor: Colors.midNight,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  textContainer: {
    width: '100%',
    height: 20,
    borderRightWidth: 1,
    borderRightColor: Colors.shuttleGrey,
  },
  text: {
    color: Colors.shuttleGrey,
    fontSize: Fonts.size.small,
    fontWeight: '600',
    textAlign: 'center',
  },
  __active: {
    color: Colors.main,
  },
  __last: {
    borderRightWidth: 0,
  },
});

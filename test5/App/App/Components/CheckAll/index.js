import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import styles from '../CheckAll/styles';
import FAI from 'react-native-vector-icons/FontAwesome5';
import { Colors, Fonts } from '../../Themes';
class CheckAll extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.container}>
                <Text style={styles.checkboxLabel}>Boxes</Text>
                <View style={styles.checkboxBorder}>
                    <View style={styles.Checkbtn}>
                        <TouchableOpacity style={styles.Check}>
                            <FAI name="check" color={Colors.main} size={10} />
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.checkboxText}>Mark All</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export default CheckAll;
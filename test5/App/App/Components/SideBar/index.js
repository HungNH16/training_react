// ===== IMPORTS =====
// Modules
import React, { Fragment, PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

// Constants & Styles
import { Sidebar, MobileSidebar } from 'cf-web-ui-kit-components';
import fullLogo from '../../assets/images/logo/CF_Logo.png';
import iconLogo from '../../assets/images/logo/CF_symbol.png';
import MENU from '../../mockData/menu';
import { getUser } from '../../../services/auth';

// ===================

// ===== MAIN COMPONENT =====
class AppSidebar extends PureComponent {
    constructor(props) {
        super(props);

        this.onCloseSidebar = this.onCloseSidebar.bind(this);
    }

    onCloseSidebar() {
        document.getElementById('mobileSidebar').style.width = '0%';
        document.getElementById('sidebarShadow').style.visibility = 'hidden';
    }

    render() {
        const { location } = this.props;
        return (
            <Fragment>
                <Sidebar menu={MENU[getUser().role]} logo={{ full: fullLogo, icon: iconLogo }} activeUrl={location.pathname} className="d-none d-lg-block" homepageUrl="/" />
                <MobileSidebar menu={MENU[getUser().role]} activeUrl={location.pathname} onCloseSidebar={this.onCloseSidebar} className="d-lg-none" />
            </Fragment>
        );
    }
}
// ==========================

// ===== PROP TYPES =====
AppSidebar.propTypes = {
    /** Current browser URL */
    location: PropTypes.object,
};
// ======================

export default withRouter(AppSidebar);

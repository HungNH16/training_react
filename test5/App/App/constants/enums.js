/**
 * Enums
 *
 * This contains all types of enums for Dispatcher Platform
 */

// ===== MAPPINGS =====
const VEHICLE_FIELD_MAP = {
    licensePlate: 'license',
    'makeModel.make': 'make',
    'makeModel.model': 'model',
    'volume.length': 'length',
    'volume.width': 'width',
    'volume.height': 'height',
    weight: 'weight',
    status: 'status',
    tags: 'tags',
    characteristics: 'characteristics',
};

const DRIVER_FIELD_MAP = {
    '3plID': 'thirdPL',
    driverName: 'name',
    'contactNumber.countryCode': 'phone',
    'contactNumber.number': 'phone',
    status: 'status',
    tags: 'tags',
};

const WEEKDAY_MAP = {
    1: {
        SHORT: 'Mon',
        LONG: 'Monday',
    },
    2: {
        SHORT: 'Tue',
        LONG: 'Tuesday',
    },
    3: {
        SHORT: 'Wed',
        LONG: 'Wednesday',
    },
    4: {
        SHORT: 'Thu',
        LONG: 'Thursday',
    },
    5: {
        SHORT: 'Fri',
        LONG: 'Friday',
    },
    6: {
        SHORT: 'Sat',
        LONG: 'Saturday',
    },
    7: {
        SHORT: 'Sun',
        LONG: 'Sunday',
    },
};
// ====================

export default {
    VEHICLE_FIELD_MAP,
    DRIVER_FIELD_MAP,
    WEEKDAY_MAP,
};

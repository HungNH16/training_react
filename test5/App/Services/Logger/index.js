// Not have much meaning by now but in the future it will use for
// Error reporting, analysis on production.
const Logger = {};

Logger.log = __DEV__ ? console.log : () => {};
Logger.error = __DEV__ ? console.error : () => {};
Logger.warn = __DEV__ ? console.warn : () => {};

export default Logger;

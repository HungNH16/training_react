export default {
    HOMEPAGE: '/',
    USERS_LOGIN: '/users/login',
    TOURS: '/tours',
    FLEET_DRIVERS: '/fleet/drivers',
    FLEET_VEHICLES: '/fleet/vehicles',
    FLEET_VEHICLE_ASSIGNMENT: '/fleet/vehicle-assignment',
    REPORTS: '/reports',
    SETTINGS: '/settings',
    ABOUT: '/about',
    TERMS_OF_USE: '/terms',
};

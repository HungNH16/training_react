import React from 'react';
import {
  TouchableHighlight,
  Text,
  View,
  ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';
// Styles
import styles from './styles';

const Tab = ({
  isActive,
  label,
  onPress,
  style,
  isLast,
}) => {
  let textStyles = [styles.text];
  let textContainerStyles = [styles.textContainer];

  if (isActive) {
    textStyles = textStyles.concat(styles.__active);
  }

  if (isLast) {
    textContainerStyles = textContainerStyles.concat(styles.__last);
  }

  return (
    <TouchableHighlight
      style={[styles.tab, style]}
      onPress={onPress}
    >
      <View style={textContainerStyles}>
        <Text style={textStyles}>{label}</Text>
      </View>
    </TouchableHighlight>
  );
};

Tab.defaultProps = {
  style: {},
  isActive: false,
  isLast: false,
};

Tab.propTypes = {
  style: ViewPropTypes.style,
  onPress: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  isActive: PropTypes.bool,
  isLast: PropTypes.bool,
};

export default Tab;

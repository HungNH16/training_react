import { StyleSheet } from 'react-native';
import { Colors } from '../../Themes/index';

export default StyleSheet.create({
  input: {
    paddingLeft: 10,
    height: 40,
    width: '100%',
    backgroundColor: Colors.white,
    borderColor: Colors.shuttleGrey,
  },
  __focused: {
    borderWidth: 2,
    borderColor: Colors.main,
  },
});

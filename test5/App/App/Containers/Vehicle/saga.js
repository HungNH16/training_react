import { takeLatest, call, put, select } from 'redux-saga/effects';
import { get as _get } from 'lodash';
import request, { fetchAxios } from 'utils/request';
import { addNewVehicleSuccess, addNewVehicleFailed, loadVehicleSuccessAction, loadVehicleFailureAction, loadMakeModelSuccessAction } from './actions';
import { ADD_VEHICLE, LOAD_VEHICLE_REQUEST_ACTION } from './constants';
import { selectVehicleDomain } from './selectors';
import { getToken } from '../../../services/auth';
import { API, ENUM } from '../../constants';

export function* searchVehicle(requestData) {
    const {
        params: { page },
    } = requestData;

    const dataState = yield select(selectVehicleDomain);
    const currentPage = dataState.getIn(['pagination', 'currentPage']) || page;

    try {
        const vehicles = yield call(request, `${API.FLEET_VEHICLES}?page=${currentPage}`, { method: 'GET', headers: { token: getToken() } });
        yield put(loadVehicleSuccessAction(vehicles));

        // call FLEET_MAKE_MODELS
        const makeModels = yield call(request, `${API.FLEET_MAKE_MODELS}`, { method: 'GET', headers: { token: getToken() } });
        yield put(loadMakeModelSuccessAction(makeModels));
    } catch {
        yield put(loadVehicleFailureAction('internal server'));
    }
}

export function* addVehicle(actionData) {
    try {
        const response = yield call(fetchAxios, { url: API.FLEET_VEHICLES, method: 'post', headers: { token: getToken(), 'Content-Type': 'application/json' }, responseType: 'json', data: actionData.data });
        yield put(addNewVehicleSuccess(response));
    } catch (error) {
        const responseErrors = _get(error, 'response.data.errors', []);
        // const responseErrors = [{ weight: 'Weight cannot be added' }, { '3plID': "3PL doesn't exist" }];
        const errors = responseErrors.map(err => Object.values(err)[0]);
        const invalidFields = responseErrors.map(err => ENUM.VEHICLE_FIELD_MAP[Object.keys(err)[0]]);

        yield put(addNewVehicleFailed(errors, invalidFields));
    }
}

// Root saga manages watcher lifecycle
export default function* vehicleSaga() {
    yield takeLatest(LOAD_VEHICLE_REQUEST_ACTION, searchVehicle);
    yield takeLatest(ADD_VEHICLE, addVehicle);
}

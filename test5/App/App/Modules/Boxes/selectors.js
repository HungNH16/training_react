export const getBoxesState = state => (state.Boxes);
export const getBoxes = state => getBoxesState(state).Boxes;
// eslint-disable-next-line eol-last
export const getIsFetchingBoxes = state => getBoxesState(state).isFetchingBoxes;
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import PropTypes from 'prop-types';
// Tab
import Tab from '../Tab';
// Styles
import styles from './styles';

class TabList extends PureComponent {
  constructor(props) {
    super(props);

    const { tabs } = props;

    this.state = {
      index: 0,
      // [{key: key, ...another props}];
      // Disable this line because it's used by TabView.
      routes: Object.keys(tabs).map(key => ({ key, ...tabs[key] })), // eslint-disable-line react/no-unused-state
    };
    this.renderTab = this.renderTab.bind(this);
    this.renderTabContent = this.renderTabContent.bind(this);
  }

  renderTab({ navigationState: { routes }, jumpTo }) {
    const { tabs: tabList } = this.props;
    const { index: activeIndex } = this.state;
    const tabs = routes.map(({ key, label }, index) => (
      <Tab
        key={key}
        label={label}
        isLast={index === tabList.length}
        isActive={activeIndex === index}
        onPress={() => jumpTo(key)}
      />
    ));

    return (
      <View style={styles.tabContainer}>
        {tabs}
      </View>
    );
  }

  renderTabContent() {

  }

  render() {
    const { tabs } = this.props;
    const scenes = {};

    Object.keys(tabs).forEach((key) => { scenes[key] = tabs[key].render; });
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap(scenes)}
        onIndexChange={index => this.setState({ index })}
        initialLayout={styles.container}
        renderTabBar={this.renderTab}
      />
    );
  }
}

TabList.propTypes = {
  tabs: PropTypes.shape({ key: PropTypes.object }).isRequired,
};

export default TabList;

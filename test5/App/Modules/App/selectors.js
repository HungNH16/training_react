export const getAppState = state => (state.app);
export const getLoading = state => getAppState(state).isLoading;

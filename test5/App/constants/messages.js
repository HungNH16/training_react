/**
 * React Intl Message Object
 *
 * This contains all text objects for the Dispatcher Platform web.
 * Format:
 * - "id": message key, for multi-language.
 * - "defaultMessage": default text when messsage ID is not found.
 */

import { defineMessages } from 'react-intl';

const commonPrefix = 'app.common';
const containerPrefix = 'app.container';
const commonInputPrefix = `${commonPrefix}.input`;
const commonPopupPrefix = `${commonPrefix}.popup`;
const commonTablePrefix = `${commonPrefix}.table`;
const supplierPrefix = `${containerPrefix}.supplier`;
const driverPrefix = `${containerPrefix}.driver`;
const vehiclePrefix = `${containerPrefix}.vehicle`;
const vehicleAssignmentPrefix = `${containerPrefix}.vehicleassignment`;
const termsPrefix = `${containerPrefix}.terms`;

export default defineMessages({
    // Common Messages
    COMMON_INPUT_SELECT_OPTION_DEFAULT: {
        id: `${commonInputPrefix}.select.option.default`,
        defaultMessage: 'Select',
    },
    COMMON_INPUT_SEARCH_PLACEHOLDER: {
        id: `${commonInputPrefix}.placeholder.search`,
        defaultMessage: 'Enter Keywords',
    },
    COMMON_INPUT_TELEPHONE_PLACEHOLDER: {
        id: `${commonInputPrefix}.telephone.placeholder`,
        defaultMessage: 'e.g. 12345678',
    },
    COMMON_POPUP_MESSAGE_SUCCESS: {
        id: `${commonPopupPrefix}.message.success`,
        defaultMessage: 'Success!',
    },
    COMMON_BUTTON_SEARCH_BAR: {
        id: `${commonPrefix}.search.bar`,
        defaultMessage: 'Search',
    },
    COMMON_BUTTON_DELETE_ABOVE_TABLE: {
        id: `${commonPrefix}.delete.above.table`,
        defaultMessage: 'Delete',
    },
    COMMON_TABLE_COLUMN_HEADER_ACTION: {
        id: `${commonTablePrefix}.column.header.action`,
        defaultMessage: 'Action',
    },
    // Suppliers Management Messages
    SUPPLIER_BUTTON_TITLE_ADD_NEW: {
        id: `${supplierPrefix}.table.toolbar.button.addnew`,
        defaultMessage: 'Add New Supplier',
    },
    BUTTON_DELETE_ABOVE_TABLE: {
        id: `${commonPrefix}.delete.above.table`,
        defaultMessage: 'Delete',
    },
    NO_DATA_TEXT: {
        id: `${commonPrefix}.table.no.data.text`,
        defaultMessage: 'No content is currently available',
    },
    // Drivers Management Messages
    DRIVER_PAGE_TITLE: {
        id: `${driverPrefix}.page.title`,
        defaultMessage: 'Driver Management',
    },
    DRIVER_TABLE_HEADER: {
        id: `${vehiclePrefix}.table.header`,
        defaultMessage: 'Driver Management',
    },
    DRIVER_BUTTON_TITLE_ADD_NEW: {
        id: `${driverPrefix}.table.toolbar.button.addnew`,
        defaultMessage: 'Add New Driver',
    },
    DRIVER_POPUP_ADD_NEW_TITLE: {
        id: `${vehiclePrefix}.popup.addnew.title`,
        defaultMessage: 'Add New Driver',
    },
    DRIVER_POPUP_MESSAGE_ADD_SUCCESS: {
        id: `${driverPrefix}.popup.message.addsuccess`,
        defaultMessage: 'Driver has been successfully added.',
    },
    // Vehicles Management Messages
    VEHICLE_PAGE_TITLE: {
        id: `${vehiclePrefix}.page.title`,
        defaultMessage: 'Vehicle Management',
    },
    VEHICLE_TABLE_HEADER: {
        id: `${vehiclePrefix}.table.header`,
        defaultMessage: 'Vehicle Management',
    },
    VEHICLE_BUTTON_TITLE_ADD_NEW: {
        id: `${vehiclePrefix}.table.toolbar.button.addnew`,
        defaultMessage: 'Add New Vehicle',
    },
    VEHICLE_POPUP_ADD_NEW_TITLE: {
        id: `${vehiclePrefix}.popup.addnew.title`,
        defaultMessage: 'Add New Vehicle',
    },
    VEHICLE_POPUP_MESSAGE_ADD_SUCCESS: {
        id: `${driverPrefix}.popup.message.addsuccess`,
        defaultMessage: 'Vehicle has been successfully added.',
    },
    // Vehicle Assigment Messages
    VEHICLE_ASSIGNMENT_TITLE: {
        id: `${vehicleAssignmentPrefix}.title`,
        defaultMessage: 'Vehicle Assignment',
    },
    VEHICLE_ASSIGNMENT_TABLE_TITLE: {
        id: `${vehicleAssignmentPrefix}.table.title`,
        defaultMessage: 'Vehicle Assignment',
    },
    VEHICLE_ASSIGNMENT_TABLE_HEADER_DRIVER_NAME: {
        id: `${vehicleAssignmentPrefix}.table.header.driver.name`,
        defaultMessage: 'Driver Name',
    },
    VEHICLE_ASSIGNMENT_TABLE_HEADER_DRIVER_STATUS: {
        id: `${vehicleAssignmentPrefix}.table.header.driver.status`,
        defaultMessage: 'Driver Status',
    },
    VEHICLE_ASSIGNMENT_TABLE_HEADER_VEHICLE: {
        id: `${vehicleAssignmentPrefix}.table.header.vehicle`,
        defaultMessage: 'Vehicle',
    },
    VEHICLE_ASSIGNMENT_TABLE_HEADER_VEHICLE_STATUS: {
        id: `${vehicleAssignmentPrefix}.table.header.vehicle.status`,
        defaultMessage: 'Vehicle Status',
    },
    VEHICLE_ASSIGNMENT_TABLE_BUTTON_OPEN_EDIT: {
        id: `${vehicleAssignmentPrefix}.table.button.openedit`,
        defaultMessage: 'Edit Assignment',
    },
    VEHICLE_ASSIGNMENT_TABLE_BUTTON_ADD_ROW: {
        id: `${vehicleAssignmentPrefix}.table.button.addrow`,
        defaultMessage: 'Add Pair',
    },
    VEHICLE_ASSIGNMENT_POPUP_SUCCESS_MESSAGE: {
        id: `${vehicleAssignmentPrefix}.popup.success.message`,
        defaultMessage: 'Vehicle assignment has been successfully updated.',
    },
    // Term of Use Messages
    TERMS_BUTTON_TITLE: {
        id: `${termsPrefix}.popup.term.title`,
        defaultMessage: 'Terms of Use',
    },
});

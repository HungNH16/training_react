import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {createStore, applyMiddleware, compose} from 'redux';
import myReducer from './reducers/index';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';

const reduxTool = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const store = createStore(myReducer,compose(applyMiddleware(thunk), reduxTool));
ReactDOM.render
(
<Provider store={store}><App /></Provider>,
    document.getElementById('root'));
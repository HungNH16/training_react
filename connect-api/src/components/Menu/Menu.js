import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';

const menus = [
    {
        name: 'Trang Chủ',
        to: '/',
        exact: true
    },
    {
        name: 'Quản Lý Sản Phẩm',
        to: '/product-list',
        exact: false
    },
];
const MenuLink = ({ label, to, Exact }) => {
    return (<Route
        path={to}
        exact={Exact}
        children={({ match }) => {
            var active = match ? 'active' : '';
            return (
                <li className={active}>
                    <Link to={to}>
                        {label}
                    </Link>
                </li>
            )
        }}
    />)
}
class Menu extends Component {
    render() {
        return (
            <div>
                <div className="navbar narbar-default">
                    <a className="navbar-brand"></a>
                    <ul className="nav navbar-nav">
                        {this.showMenus(menus)}
                    </ul>
                </div>
            </div>
        );
    }
    showMenus = (menus) => {
        var result = null;
        if (menus.length > 0) {
            result = menus.map((menu,index) => {
                return (<MenuLink
                    key={index}
                    label={menu.name}
                    to={menu.to}
                    Exact={menu.exact}
                />);
            });
        }
        return result;
    }
}
export default Menu;
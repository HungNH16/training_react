export const getRetailerState = state => (state.Retailer);
export const getRetailer = state => getRetailerState(state).Retailer;
// eslint-disable-next-line eol-last
export const getIsFetchingRetailer = state => getRetailerState(state).isFetchingRetailer;
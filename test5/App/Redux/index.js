import { combineReducers } from 'redux';
import configureStore from './CreateStore';
import rootSaga from '../Sagas';

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  auth: require('../Modules/Auth/reducer').reducer,
  app: require('../Modules/App/reducer').reducer,
  tour: require('../Modules/Tour/reducer').reducer,
});

export default () => {
  let { store, sagasManager, sagaMiddleware } = configureStore(reducers, rootSaga); // eslint-disable-line

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers; // eslint-disable-line
      store.replaceReducer(nextRootReducer);

      const newYieldedSagas = require('../Sagas').default;
      sagasManager.cancel();
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas);
      });
    });
  }

  return store;
};

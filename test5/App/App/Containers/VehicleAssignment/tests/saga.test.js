import { put, takeLatest } from 'redux-saga/effects';
import { LOAD_DATA, SUBMIT_DATA } from '../constants';
import { loadVehicleAssignmentDataSuccess, updateErrors, loadDriverDataSuccess, loadVehicleDataSuccess, submitVehicleAssignmentDataSuccess } from '../actions';
import vehicleAssignmentSaga, { loadVehicleAssignment, loadDriver, loadVehicle, updateVehicleAssignment } from '../saga';
import { convertResponseAlerts } from '../../../utils';

/* eslint-disable redux-saga/yield-effects */
describe('Load Vehicle Assignment Saga', () => {
    let loadVehicleAssignmentGenerator;

    beforeEach(() => {
        loadVehicleAssignmentGenerator = loadVehicleAssignment();

        const callDescriptor = loadVehicleAssignmentGenerator.next().value;
        expect(callDescriptor).toMatchSnapshot();
    });

    it('should store data if request successfully', () => {
        const response = {
            data: [],
        };

        const putDescriptor = loadVehicleAssignmentGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(loadVehicleAssignmentDataSuccess(response)));
    });

    it('should show error message if response has any alert', () => {
        const response = {
            alerts: [
                {
                    code: '10000',
                    message: 'Please complete all required fields',
                    meta: {
                        field: 'driverName',
                    },
                },
            ],
        };

        const putDescriptor = loadVehicleAssignmentGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(updateErrors(response.alerts.map(item => item.message), [])));
    });

    it('should show structure error message if response have unexpected structure', () => {
        const response = {
            message: 'Wrong structure',
        };

        const putDescriptor = loadVehicleAssignmentGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(updateErrors(['Unexpected response structure!'], [])));
    });

    it('should show internal server error if request failed', () => {
        const putDescriptor = loadVehicleAssignmentGenerator.throw().value;
        expect(putDescriptor).toEqual(put(updateErrors(['Internal server error!'], [])));
    });
});

describe('Load Driver Saga', () => {
    let loadDriverGenerator;

    beforeEach(() => {
        loadDriverGenerator = loadDriver();

        const callDescriptor = loadDriverGenerator.next().value;
        expect(callDescriptor).toMatchSnapshot();
    });

    it('should store data if request successfully', () => {
        const response = {
            data: [],
        };

        const putDescriptor = loadDriverGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(loadDriverDataSuccess(response)));
    });

    it('should show error message if response has any alert', () => {
        const response = {
            alerts: [
                {
                    code: '10000',
                    message: 'Cannot load driver.',
                },
            ],
        };

        const putDescriptor = loadDriverGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(updateErrors(response.alerts.map(item => item.message), [])));
    });

    it('should show structure error message if response have unexpected structure', () => {
        const response = {
            message: 'Wrong structure',
        };

        const putDescriptor = loadDriverGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(updateErrors(['Unexpected response structure!'], [])));
    });

    it('should show internal server error if request failed', () => {
        const putDescriptor = loadDriverGenerator.throw().value;
        expect(putDescriptor).toEqual(put(updateErrors(['Internal server error!'], [])));
    });
});

describe('Load Vehicle Saga', () => {
    let loadVehicleGenerator;

    beforeEach(() => {
        loadVehicleGenerator = loadVehicle();

        const callDescriptor = loadVehicleGenerator.next().value;
        expect(callDescriptor).toMatchSnapshot();
    });

    it('should store data if request successfully', () => {
        const response = {
            data: [],
        };

        const putDescriptor = loadVehicleGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(loadVehicleDataSuccess(response)));
    });

    it('should show error message if response has any alert', () => {
        const response = {
            alerts: [
                {
                    code: '10000',
                    message: 'Cannot load vehicle.',
                },
            ],
        };

        const putDescriptor = loadVehicleGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(updateErrors(response.alerts.map(item => item.message), [])));
    });

    it('should show structure error message if response have unexpected structure', () => {
        const response = {
            message: 'Wrong structure',
        };

        const putDescriptor = loadVehicleGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(updateErrors(['Unexpected response structure!'], [])));
    });

    it('should show internal server error if request failed', () => {
        const putDescriptor = loadVehicleGenerator.throw().value;
        expect(putDescriptor).toEqual(put(updateErrors(['Internal server error!'], [])));
    });
});

describe('Load Vehicle Saga', () => {
    let loadVehicleGenerator;

    beforeEach(() => {
        loadVehicleGenerator = loadVehicle();

        const callDescriptor = loadVehicleGenerator.next().value;
        expect(callDescriptor).toMatchSnapshot();
    });

    it('should store data if request successfully', () => {
        const response = {
            data: [],
        };

        const putDescriptor = loadVehicleGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(loadVehicleDataSuccess(response)));
    });

    it('should show error message if response has any alert', () => {
        const response = {
            alerts: [
                {
                    code: '10000',
                    message: 'Cannot load vehicle.',
                },
            ],
        };

        const putDescriptor = loadVehicleGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(updateErrors(response.alerts.map(item => item.message), [])));
    });

    it('should show structure error message if response have unexpected structure', () => {
        const response = {
            message: 'Wrong structure',
        };

        const putDescriptor = loadVehicleGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(updateErrors(['Unexpected response structure!'], [])));
    });

    it('should show internal server error if request failed', () => {
        const putDescriptor = loadVehicleGenerator.throw().value;
        expect(putDescriptor).toEqual(put(updateErrors(['Internal server error!'], [])));
    });
});

describe('Update Vehicle Assignment Saga', () => {
    let updateVehicleAssignmentGenerator;
    beforeEach(() => {
        const action = {
            type: SUBMIT_DATA,
            data: {
                status: 'Published',
                details: [
                    {
                        driverID: 'DR000001',
                        driverStatus: 4,
                        vehicleLicensePlate: 'LP000001',
                        vehicleStatus: 4,
                    },
                ],
            },
        };
        updateVehicleAssignmentGenerator = updateVehicleAssignment(action);

        const callDescriptor = updateVehicleAssignmentGenerator.next().value;
        expect(callDescriptor).toMatchSnapshot();
    });

    it('update vehicle assignment successfully', () => {
        const response = {
            data: {
                categoryID: 1,
            },
        };
        const putDescriptor = updateVehicleAssignmentGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(submitVehicleAssignmentDataSuccess(response)));
    });

    it('receives validation errors', () => {
        const error = {
            response: {
                alerts: [
                    {
                        code: '100000',
                        message: 'Please complete all required fields',
                        type: 'ERROR',
                        meta: {
                            field: 'data.0.driverName',
                        },
                    },
                ],
            },
        };
        const errorObj = convertResponseAlerts(error);
        const putDescriptor = updateVehicleAssignmentGenerator.throw(error).value;
        expect(putDescriptor).toEqual(put(updateErrors(errorObj.errors, errorObj.invalidFields)));
    });
});

describe('Vehicle Assignment Root Saga', () => {
    const vehicleAssignmentSagaRoot = vehicleAssignmentSaga();

    it('should start task to watch for LOAD_DATA action', () => {
        const takeLatestDescriptor = vehicleAssignmentSagaRoot.next().value;
        expect(takeLatestDescriptor).toEqual(takeLatest(LOAD_DATA, loadVehicleAssignment));
    });
});

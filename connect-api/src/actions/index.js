import * as types from '../constants/ActionTypes';
import callApi from '../utils/apiCaller';
export const fetch_products_request = () => {
    return dispatch => {
        return callApi(`products`, 'GET', null).then(res => {
            dispatch(fetch_products(res.data))
        });
    }
}

export const fetch_products = (products) => {
    return {
        type: types.FETCH_PRODUCTS,
        products
    }
};

export const delete_products_request = (id) => {
    return dispatch => {
        return callApi(`products/${id}`, 'DELETE', null).then(res => {
            dispatch(delete_products(id))
        })
    }
}

export const delete_products = (id) => {
    return {
        type: types.DELETE_PRODUCT,
        id
    }
} 

export const add_products_request = (product) => {
    return dispatch => {
        return callApi(`products` , 'POST', product).then(res => {
            dispatch(add_products(res.data))
        })
    }
}

export const add_products = (product) => {
    return {
        type: types.ADD_PRODUCT,
        product
    }
}

export const edit_products_request = (id) => {
    return dispatch => {
        return callApi(`products/${id}` , 'GET' , null).then(res => {
                dispatch(edit_products(res.data))
        })
    }
}

export const edit_products = (product) => {
    return {
        type: types.EDIT_PRODUCT,
        product
    }
}

export const update_products_request = (product) => {
    return dispatch => {
        return callApi(`products/${product.id}`, 'PUT', product).then(res => {
            dispatch(update_products(res.data))
        })
    }
}

export const update_products = (product) => {
    return {
        type: types.UPDATE_PRODUCT,
        product
    }
}
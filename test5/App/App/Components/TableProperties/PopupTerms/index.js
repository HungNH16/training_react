/**
 *
 * Common Add-New Button For Table
 *
 * This component defines a common button that handles data addition
 * for tables all around the web app.
 * It's placed in the table header toolbar.
 */
// ===== IMPORTS =====
// Modules
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, Label, FormGroup } from 'reactstrap';
import {withRouter} from 'react-router-dom';
import { Popup } from 'cf-web-ui-kit-components';
// Custom Components
import { TableToolbarButtonStyle } from '../../Styles';
// Constants & Styles
import '../../../scss/components/popups/terms.scss';
import '../../../scss/_variables.scss';
import {getUserAgreement, setUserAgreementFor, getUser, setToken} from '../../../../services/auth';
// ===================

// ===== SUB-COMPONENT =====
const FormContent = () => (
    <div className="termOfUse">
        <div className="termOfUseContentContainer">
            <div className="termOfUseContent">
                <Label className="termOfUseContentHeading">Term & Conditions</Label>
                <p>
                    Please read these terms and conditions of use carefully before using this mobile application (the “<b>App</b>”).
                    By accessing or using this App or any functionality thereof on any computer, mobile phone, tablet, console
                    or other device (together “<b>Devices</b>” and each a “<b>Device</b>”), you confirm that you have read, understood and agreed
                    to be bound by these Terms and Conditions of Use and any other applicable law.
                    <br/><br/>
                    If you do not agree to these Terms and Conditions of Use, please do not access or use the App or any functionality thereof.
                    <br/><br/>
                    <b>1.Owner and Operator Information</b> 
                    <br/><br/><br/>
                    1.1 The App is owned and operated by Connected Freight Pte Ltd,
                    a company whose registered office is at 9 North Buona Vista Drive, The 2.3
                </p>
            </div>
        </div>
        <div className="checkboxContainer">
            <FormGroup>
                <Label>
                    <Input
                        id="checkboxLabel"
                        type="checkbox" 
                        className="form-check-input"
                    />
                    I have read and agree to the Terms of Use
                </Label>
            </FormGroup>
        </div>
    </div>
);

// ===== MAIN COMPONENT =====
class PopupTerms extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            isOpen: !getUserAgreement(),
        };

        this.onCancel = this.onCancel.bind(this);
        this.onContinue = this.onContinue.bind(this);
    }


    onCancel() {
        this.setState(
            ({ isOpen }) => ({
                isOpen: !isOpen,
            }),
            () => {
                setToken();
                this.props.history.push('/users/login')
            }            
        );
    }

    onContinue() {
        const currentUser = getUser();        
        setUserAgreementFor(currentUser.username);
        this.setState({isOpen: false});
    }

    render() {
        const { isOpen } = this.state;
        return (
            <TableToolbarButtonStyle>
                {/* Add-New Pop-up */}
                <Popup
                    isOpen={isOpen}
                    backdrop="static"
                    title="Terms of Use"
                    popupClass="add-new-popup"                  
                    component={FormContent}
                    button={{
                        primary: {
                            title: 'CONTINUE',
                        },
                        secondary: {
                            title: 'CANCEL',
                            className: 'grey-btn',
                        },
                    }
                    }
                    handleExecution={this.onContinue}
                    hidePopup={this.onCancel}
                />               
            </TableToolbarButtonStyle>
        );
    }
}

// ===== PROP TYPES =====
FormContent.propTypes = {
};
FormContent.defaultProps = {

};
PopupTerms.propTypes = {
    history: PropTypes.any,
};
PopupTerms.defaultProps = {
    history: {
        push: () => {},
    },
};
// ======================

export default withRouter(PopupTerms);

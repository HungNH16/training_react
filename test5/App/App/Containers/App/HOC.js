import React from 'react';
import PropTypes from 'prop-types';
import { loginTimeout } from 'config.json';
import { setToken } from '../../../services/auth';

export const SESSION_KEY = 'signoutTime';
const events = [
    'load',
    'mousemove',
    'mousedown',
    'click',
    'scroll',
    'keypress',
];

export default function(ComposedClass) {
    const AutoLogout = class AutoLogout extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                signoutTime: 1000 * 60 * loginTimeout,
            };
        }

        componentDidMount() {
            events.forEach(item => {
                window.addEventListener(item, this.resetTimeout);
            });

            this.setTimeout();
        }

        componentWillUnmount() {
            this.clearTimeoutFunc();
            events.forEach(item => {
                window.removeEventListener(item, this.resetTimeout);
            });
        }

    setTimeout = () => {
        if (!Date.now) {
            Date.now = () => new Date().getTime();
        }
        localStorage.setItem(
            SESSION_KEY,
            Date.now() + this.state.signoutTime - 8000,
        );
        this.logoutTimeout = setTimeout(this.logout, this.state.signoutTime);
    };

    clearTimeoutFunc = () => {
        localStorage.removeItem(SESSION_KEY);
        if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
    };

    resetTimeout = () => {
        this.clearTimeoutFunc();
        this.setTimeout();
    };

    logout = () => {
        if (localStorage.getItem(SESSION_KEY) <= Date.now()) {
        // Send a logout request to the API
            this.destroy();
        } else {
            this.resetTimeout();
        }
    };

    destroy = () => {
        // clear the session
        setToken();
        if (this.props.history) this.props.history.push('/users/login');
    };

    render() {
        return <ComposedClass {...this.props} />;
    }
    };
    AutoLogout.propTypes = {
        history: PropTypes.any,
    };

    AutoLogout.defaultProps = {
        history: {
            push: () => {},
        },
    };
    return AutoLogout;
}

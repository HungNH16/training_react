import React, { PureComponent } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import FAI from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';
// Styles
import styles from './styles';
// Colors, Fonts
import { Colors, Fonts } from '../../Themes';

class Retailer extends PureComponent {

  render() {
    const {
      type,
      name,
      text,
      packages,
    } = this.props;
    let timeLineLineStyle = [styles.timeLineLine];
    let timeLineIconColor = Colors.blue;
    let detailTypeLabelStyle = [styles.detailTypeLabel];
    let detailContainerStyle = [styles.detailContainer];

    timeLineLineStyle = timeLineLineStyle.concat(styles.__dropOffTimeLineLine);
    timeLineIconColor = Colors.darkBlue;
    detailTypeLabelStyle = detailTypeLabelStyle.concat(styles.__dropOffDetailTypeLabel);
    detailContainerStyle = detailContainerStyle.concat(styles.__dropOffDetailContainer);
    return (
      <View style={styles.Retailer}>
        <View>
          <View>
            <View >
              <View>
                <View>
                  <View >
                    <View style={styles.type}>
                      <Text style={styles.typeText}>{type}</Text>
                    </View>
                  </View>
                  <Text style={styles.name}>{name}</Text>
                  <View style={styles.marketalt}>
                    <FAI name="map-marker-alt" size={Fonts.size.small} color={Colors.shuttleGrey} />
                    {<Text style={styles.marketaltText}>
                      {text}
                    </Text>}
                  </View>
                  <View style={styles.cube}>
                    <FAI name="cube" size={Fonts.size.small} color={Colors.shuttleGrey} />
                    {<Text style={styles.packages}>
                      {packages}
                    </Text>}
                    <View style={styles.Call}>
                        <TouchableOpacity style={styles.RetailerBtn}>
                          <FAI name="phone" size={Fonts.size.small} color={Colors.white} />
                          {<Text style={styles.RetailerBtnText}>Call Retailer</Text>}
                        </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
// eslint-disable-next-line no-undef
Retailer.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  packages: PropTypes.number.isRequired,
};
// eslint-disable-next-line eol-last


export default Retailer;
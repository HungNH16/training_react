/*
 * Vehicle Messages
 *
 * This contains all the text for the Vehicle container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Vehicle';

export default defineMessages({
    pageTitle: {
        id: `${scope}.pageTitle`,
        defaultMessage: 'Vehicle Management',
    },
    tableTitle: {
        id: `${scope}.tableTitle`,
        defaultMessage: 'Vehicle Management',
    },
    searchButton: {
        id: `${scope}.button.search`,
        defaultMessage: 'Search',
    },
    addNewButton: {
        id: `${scope}.button.addnew`,
        defaultMessage: 'Add New Vehicle',
    },
    deleteButton: {
        id: `${scope}.button.delete`,
        defaultMessage: 'Delete',
    },
    addNewPopupTitle: {
        id: `${scope}.popup.addnew.title`,
        defaultMessage: 'Add New Vehicle',
    },
    licensePlate: {
        id: `${scope}.licensePlate`,
        defaultMessage: 'License Plate',
    },
    plName: {
        id: `${scope}.plName`,
        defaultMessage: '3PL Name',
    },
    vehicleMake: {
        id: `${scope}.vehicleMake`,
        defaultMessage: 'Vehicle Make',
    },
    modelType: {
        id: `${scope}.modelType`,
        defaultMessage: 'Model Type',
    },
    volume: {
        id: `${scope}.volume`,
        defaultMessage: 'Volume (L/W/H)',
    },
    weight: {
        id: `${scope}.weight`,
        defaultMessage: 'Weight',
    },
    tags: {
        id: `${scope}.tags`,
        defaultMessage: 'Tag(s)',
    },
    status: {
        id: `${scope}.status`,
        defaultMessage: 'Status',
    },
    action: {
        id: `${scope}.action`,
        defaultMessage: 'Action',
    },
    characteristics: {
        id: `${scope}.characteristics`,
        defaultMessage: 'Characteristics',
    },
    enterKeywords: {
        id: `${scope}.enterKeywords`,
        defaultMessage: 'Enter Keywords',
    },
});

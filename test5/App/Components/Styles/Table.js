import styled from 'styled-components';

export const TableToolbarButtonStyle = styled.div`
    .table-toolbar-button {
        display: inline-block;
        position: relative;
        text-align: center;
        vertical-align: middle;
        color: $text-orange;
        font-size: 13px;
        &:hover {
            cursor: pointer;
        }
        .icon {
            display: inline-block;
            width: 22px;
            height: 22px;
            border: 2px solid;
            border-radius: 50%;
        }
    }
`;

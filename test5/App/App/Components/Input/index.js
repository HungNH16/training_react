import React, { Component } from 'react';
import { TextInput, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
// Styles
import styles from './styles';

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = { isFocusing: false };
    this.onBlur = this.onBlur.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }

  onBlur() {
    this.setState({ isFocusing: false });
  }

  onFocus() {
    this.setState({ isFocusing: true });
  }

  render() {
    const {
      onChangeText,
      value,
      placeholder,
      style,
      ...rest
    } = this.props;
    const { onFocus, onBlur } = this;
    const { isFocusing } = this.state;
    let inputStyle = [styles.input, style];

    if (isFocusing) {
      inputStyle = inputStyle.concat([styles.__focused]);
    }

    return (
      <TextInput
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
        onFocus={onFocus}
        onBlur={onBlur}
        placeholder={placeholder}
        {...rest}
      />
    );
  }
}

Input.defaultProps = {
  value: '',
  placeholder: '',
  style: null,
};

Input.propTypes = {
  onChangeText: PropTypes.func.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  style: ViewPropTypes.style,
};

export default Input;

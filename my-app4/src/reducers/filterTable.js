import * as types from './../constants/ActionTypes';
var initialState = {
    name: '',
    filterStatus: -1
};
var myReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FILTER_TABLE:
            return {
                name: action.filter.name,
                filterStatus: parseInt(action.filter.status, 10)
            };
        default: return state;
    }
};
export default myReducer;
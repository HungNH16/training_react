import { StyleSheet } from 'react-native';
// Metrics and colors
import { Metrics, Colors } from '../../Themes';

export default StyleSheet.create({
  container: {
    height: 50,
    width: Metrics.screenWidth,
    backgroundColor: Colors.background,
    paddingVertical: 10,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoImage: {
    height: 30,
    resizeMode: 'contain',
  },
});

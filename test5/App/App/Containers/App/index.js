/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import Login from 'containers/User/Login/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { PrivateLayout } from '../../components/Layouts';
import VehiclePage from '../Vehicle/Loadable';
import DriverPage from '../Driver/Loadable';
import VehicleAssignmentPage from '../VehicleAssignment/Loadable';
import withAutoLogout from './HOC';
import { isLoggedIn } from '../../../services/auth';
import { ROUTE } from '../../constants';
import { GlobalStyle, AppStyle } from '../../components/Styles';
import '../../scss/containers/common.scss';

// Add auto logout check for HomePage component.
const enhancedHomePage = withAutoLogout(HomePage);

const App = () => (
    <AppStyle>
        <Helmet titleTemplate="%s" defaultTitle="Connected Freight">
            <meta name="description" content="Connected Freight" />
        </Helmet>
        <Switch>
            <Route exact path={ROUTE.USERS_LOGIN} render={() => (isLoggedIn() ? <Redirect strict to={ROUTE.HOMEPAGE} /> : <Login />)} />
            <PrivateLayout exact path={ROUTE.HOMEPAGE} component={enhancedHomePage} />
            <PrivateLayout exact path={ROUTE.FLEET_VEHICLES} component={VehiclePage} />
            <PrivateLayout exact path={ROUTE.FLEET_DRIVERS} component={DriverPage} />
            <PrivateLayout exact path={ROUTE.FLEET_VEHICLE_ASSIGNMENT} component={VehicleAssignmentPage} />
            <PrivateLayout path="*" component={NotFoundPage} />
        </Switch>
        <GlobalStyle />
    </AppStyle>
);

export default withRouter(App);

/**
 *
 * Default Layout
 *
 * This component defines the common layout for public pages,
 * which are available for all guests
 */

// ===== IMPORTS =====
// Modules
import React from 'react';
import { Route } from 'react-router-dom';
// Constants & Styles
import '../../scss/components/layouts/layouts.scss';
// ===================

// ===== MAIN COMPONENT =====
export default ({ component: Component, ...remain }) => (
    <Route
        {...remain}
        render={() => (
            <div className="cf-main">
                <div className="cf-content">
                    <Component />
                </div>
            </div>
        )}
    />
);
// ==========================

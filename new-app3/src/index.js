import React from 'react';
import ReactDOM from 'react-dom';
import FormComp from './App';

ReactDOM.render(<FormComp />, document.getElementById('root'));

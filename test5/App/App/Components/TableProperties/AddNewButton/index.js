/**
 *
 * Common Add-New Button For Table
 *
 * This component defines a common button that handles data addition
 * for tables all around the web app.
 * It's placed in the table header toolbar.
 */
// ===== IMPORTS =====
// Modules
import React, { Component } from 'react';
import { Container, Row, Col, Input, Label, Form, FormGroup, Nav, NavItem, FormFeedback, CardImg } from 'reactstrap';
import Select from 'react-select';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { isEmpty as _isEmpty, get as _get } from 'lodash';
import { Popup } from 'cf-web-ui-kit-components';
// Custom Components
import { TableToolbarButtonStyle } from '../../Styles';
// Constants & Styles
import { MSG } from '../../../constants';
import successIcon from '../../../assets/images/icons/Success.png';
import '../../../scss/components/popups/add_new.scss';
import '../../../scss/components/popups/success.scss';
import WorkingHourBlock from '../WorkingHour';
// ===================

// ===== SUB-COMPONENT =====
const InputField = ({ fieldId, label, type, name, options, handleChangeInput, message, customInput: CustomInput, getInputProps, inputs, invalidFields, multiple, transformValue, ...remains }) => {
    const defaultOnChange = ({ name: inputName, type: inputType }) => e => {
        if (inputType === 'select') {
            handleChangeInput(inputName, e);
        } else {
            handleChangeInput(inputName, _get(e, 'target.value'));
        }
    };
    const defaultOnBlur = ({ name: inputName, type: inputType, transformValue: inputTransformValue }) =>
        inputTransformValue
            ? e => {
                if (inputType === 'select') {
                    handleChangeInput(inputName, inputTransformValue ? inputTransformValue(e) : e);
                } else {
                    handleChangeInput(inputName, inputTransformValue ? inputTransformValue(_get(e, 'target.value')) : _get(e, 'target.value'));
                }
            }
            : undefined;
    return (
        <FormGroup row className="align-items-center">
            <Label xs={4} sm={4} md={3} lg={3} for={fieldId}>
                {label}
            </Label>
            <Col lg="9" md="9" sm="8" xs="8">
                {(!_isEmpty(CustomInput) && <CustomInput invalid={invalidFields.includes(name)} {...remains} {...getInputProps} />) ||
                    (!_isEmpty(inputs) && (
                        <Row>
                            {inputs.map(inp => (
                                <Col lg={inp.colSize} md={inp.colSize} sm={inp.colSize} xs={inp.colSize}>
                                    <Input
                                        {...remains}
                                        key={`input_${inp.name}`}
                                        id={inp.fieldId}
                                        name={inp.name}
                                        type={inp.type}
                                        value={inp.value}
                                        placeholder={inp.placeholder}
                                        onChange={defaultOnChange({ ...inp })}
                                        onBlur={defaultOnBlur({ ...inp })}
                                        invalid={invalidFields.includes(inp.name)}
                                    />
                                </Col>
                            ))}
                        </Row>
                    )) ||
                    (type === 'select' && (
                        <Select
                            name={name}
                            isMulti={multiple}
                            options={options}
                            isLoading={_isEmpty(options)}
                            placeholder={message.defaultOption}
                            onChange={defaultOnChange({ name, type })}
                            onBlur={defaultOnBlur({ name, type, transformValue })}
                            className={invalidFields.includes(name) ? 'cf-select--is-invalid' : ''}
                            classNamePrefix="cf-select"
                            {...remains}
                        />
                    )) || (type === 'workingHours' && (
                    <WorkingHourBlock handleChangeInput={handleChangeInput} invalidFields={invalidFields} {...remains} />
                )) || (
                    <Input
                        id={fieldId}
                        name={name}
                        type={type}
                        onChange={defaultOnChange({ name, type })}
                        onBlur={defaultOnBlur({ name, type, transformValue })}
                        invalid={invalidFields.includes(name)}
                        {...remains}
                    />
                )}
            </Col>
        </FormGroup>
    );
};
const FormContent = ({ fieldMap, formValues, errors, invalidFields, handleChangeInput, message }) => (
    <Form>
        <FormGroup row>
            <Col>
                <Nav vertical>
                    {errors.map((err, index) => (
                        <NavItem key={`error_${index + 1}`}>
                            <FormFeedback className="d-block">{`* ${err}`}</FormFeedback>
                        </NavItem>
                    ))}
                </Nav>
            </Col>
        </FormGroup>
        {fieldMap.map(({ name, inputs, ...fieldProps }) => {
            let newInputs = inputs;
            if (!_isEmpty(newInputs)) {
                newInputs = newInputs.map(inp => ({ ...inp, value: formValues[inp.name] }));
            }
            return (
                <InputField
                    key={`input_${name}`}
                    name={name}
                    inputs={newInputs}
                    value={formValues[name]}
                    invalidFields={invalidFields}
                    handleChangeInput={handleChangeInput}
                    message={message}
                    {...fieldProps}
                />
            );
        })}
    </Form>
);
const SuccessContent = ({ message }) => (
    <Container>
        <Row>
            <Col>
                <CardImg src={successIcon} />
            </Col>
        </Row>
        <Row>
            <Col>
                <Label>{message.successLabel}</Label>
            </Col>
        </Row>
        <Row>
            <Col>
                <span>{message.successMsg}</span>
            </Col>
        </Row>
    </Container>
);
// =========================

// ===== MAIN COMPONENT =====
class AddNewButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
        };

        this.togglePopup = this.togglePopup.bind(this);
    }

    componentWillUpdate({ addSuccessFlag: nextAddSucessFlag }) {
        if (!this.props.addSuccessFlag && nextAddSucessFlag) {
            this.togglePopup();
        }
    }

    togglePopup() {
        const { handleResetForm } = this.props;
        this.setState(
            ({ isOpen }) => ({
                isOpen: !isOpen,
            }),
            () => handleResetForm(),
        );
    }

    render() {
        const { title, fieldMap, size, handleSubmit, formValues, errors, invalidFields, handleChangeInput, handleCloseSuccessPopup, addSuccessFlag, successMsg, intl, customButtonConfig } = this.props;
        const { isOpen } = this.state;
        return (
            <TableToolbarButtonStyle>
                <div role="presentation" className="table-toolbar-button" onClick={this.togglePopup}>
                    <div className="icon">
                        <i className="fas fa-plus" />
                    </div>
                    <span className="ml-2">{title}</span>
                </div>
                {/* Add-New Pop-up */}
                <Popup
                    isOpen={isOpen}
                    backdrop="static"
                    title={title}
                    size={size}
                    popupClass="add-new-popup"
                    button={
                        !_isEmpty(customButtonConfig)
                            ? customButtonConfig
                            : {
                                primary: {
                                    title: 'Save',
                                },
                                secondary: {
                                    title: 'Cancel',
                                    className: 'grey-btn',
                                },
                            }
                    }
                    component={FormContent}
                    getContentProps={{
                        fieldMap,
                        formValues,
                        errors,
                        invalidFields,
                        handleChangeInput,
                        message: {
                            defaultOption: intl.formatMessage({ ...MSG.COMMON_INPUT_SELECT_OPTION_DEFAULT }),
                        },
                    }}
                    handleExecution={handleSubmit}
                    hidePopup={this.togglePopup}
                />
                {/* Success Popup */}
                <Popup
                    isOpen={addSuccessFlag}
                    popupClass="success-popup"
                    button={{
                        primary: {
                            title: 'OK',
                            className: 'success-btn',
                        },
                    }}
                    component={SuccessContent}
                    getContentProps={{
                        message: {
                            successLabel: intl.formatMessage({ ...MSG.COMMON_POPUP_MESSAGE_SUCCESS }),
                            successMsg,
                        },
                    }}
                    handleExecution={handleCloseSuccessPopup}
                />
            </TableToolbarButtonStyle>
        );
    }
}
// ==========================

// ===== PROP TYPES =====
InputField.propTypes = {
    fieldId: PropTypes.string,
    label: PropTypes.string,
};
InputField.defaultProps = {
    fieldId: '',
    label: '',
};
FormContent.propTypes = {
    fieldMap: PropTypes.arrayOf(
        PropTypes.shape({
            fieldId: PropTypes.string,
            label: PropTypes.string,
            type: PropTypes.string,
            name: PropTypes.string,
            placeholder: PropTypes.string,
        }),
    ),
    formValues: PropTypes.object,
    errors: PropTypes.arrayOf(PropTypes.string),
    invalidFields: PropTypes.arrayOf(PropTypes.string),
    handleChangeInput: PropTypes.func,
    message: PropTypes.shape(PropTypes.string),
};
FormContent.defaultProps = {
    fieldMap: [],
    formValues: {},
    errors: [],
    invalidFields: [],
    handleChangeInput: () => { },
    message: {},
};
SuccessContent.propTypes = {
    message: PropTypes.shape(PropTypes.string),
};
SuccessContent.defaultProps = {
    message: {},
};
AddNewButton.propTypes = {
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    fieldMap: PropTypes.arrayOf(
        PropTypes.shape({
            fieldId: PropTypes.string,
            label: PropTypes.string,
            type: PropTypes.string,
            name: PropTypes.string,
            placeholder: PropTypes.string,
        }),
    ),
    formValues: PropTypes.object,
    errors: PropTypes.arrayOf(PropTypes.string),
    invalidFields: PropTypes.arrayOf(PropTypes.string),
    handleChangeInput: PropTypes.func,
    handleResetForm: PropTypes.func,
    handleSubmit: PropTypes.func,
    handleCloseSuccessPopup: PropTypes.func,
    addSuccessFlag: PropTypes.bool,
    successMsg: PropTypes.string,
    size: PropTypes.number,
    intl: PropTypes.object,
    customButtonConfig: PropTypes.func,
};
AddNewButton.defaultProps = {
    title: '',
    fieldMap: [],
    formValues: {},
    errors: [],
    invalidFields: [],
    handleChangeInput: () => { },
    handleSubmit: () => { },
    handleCloseSuccessPopup: () => { },
    addSuccessFlag: false,
    successMsg: '',
    customButtonConfig: {},
};
// ======================

export default injectIntl(AddNewButton);

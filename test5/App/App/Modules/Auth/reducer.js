import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  login: ['payload'],
  setError: ['payload'],
});

export const AuthTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

const INITIAL_STATE = Immutable({
  error: null, // Authenticate error.
});

/* ------------- Reducers ------------- */

const setError = (state, { payload: error }) => state.merge({ error });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_ERROR]: setError,
});

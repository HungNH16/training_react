/**
 * Table Component: No Data
 * 
 * This component defines what to show when table has no record
 */
// ===== IMPORTS =====
// Modules
import React from 'react';
// ===================

// ===== MAIN COMPONENT =====
export default ({ children }) => children && <div className="rt-noData">{children}</div>;
// ==========================

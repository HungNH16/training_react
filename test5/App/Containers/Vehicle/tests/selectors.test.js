import { fromJS } from 'immutable';
import makeSelectVehicle, { selectVehicleDomain, selectFormValues, selectErrors, selectInvalidFields, selectSuccessFlag } from '../selectors';
import { initialState } from '../reducer';

describe('selectVehicleDomain', () => {
    it('Expect to have selectVehicleDomain ', () => {
        const mockedState = fromJS({
            vehicle: initialState,
        });
        expect(selectVehicleDomain(mockedState)).toEqual(initialState);
    });
});

describe('makeSelectVehicle', () => {
    it('makeSelectVehicle', () => {
        const makeSelector = makeSelectVehicle();
        expect(makeSelector(initialState)).toEqual(initialState.toJS());
    });
});

describe('selectFormValues', () => {
    it('Expect to have form values', () => {
        expect(selectFormValues(initialState)).toEqual(initialState.toJS().formValues);
    });
});

describe('selectErrors', () => {
    it('Expect to have array of error messages', () => {
        expect(selectErrors(initialState)).toEqual(initialState.toJS().errors);
    });
});

describe('selectInvalidFields', () => {
    it('Expect to have array of invalid field names', () => {
        expect(selectInvalidFields(initialState)).toEqual(initialState.toJS().invalidFields);
    });
});

describe('selectSuccessFlag', () => {
    it('Expect to have success pop-up flag', () => {
        expect(selectSuccessFlag(initialState)).toEqual(initialState.toJS().addSuccessFlag);
    });
});

/**
 *
 * Login
 *
 */

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { injectIntl } from 'react-intl';
import { Input, FormGroup, Button, Form, Label } from 'reactstrap';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { Redirect } from 'react-router-dom';
import makeSelectLogin from './selectors';
import reducer from './reducer';
import saga from './saga';
import './style.scss';
import '../../../scss/_variables.scss';
import '../../../fonts/fontawesome-free/css/all.css';
import messages from './messages';
import { siginAction } from './actions';
import { isLoggedIn } from '../../../../services/auth';

const RedText = styled.div`
    height: 30px;
    color: #ff0000;
    font-family: 'Ubuntu, Regular' sans-serif;
    font-size: 13px;
    vertical-align: top;
    ::before {
        content: '!';
        padding: 0px 5px;
        border-radius: 50%;
        background: #ff0000;
        color: #000000;
        margin-right: 7px;
    }
`;

export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = async event => {
        const { target } = event;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const { name } = target;
        await this.setState({
            [name]: value,
            errMsg: '',
        });
    };

    getInitialState() {
        return {
            isPasswordVisible: false,
            password: '',
        };
    }

    submitForm(event) {
        event.preventDefault();
        const { email, password } = this.state;
        const {
            intl: { formatMessage },
        } = this.props;

        if (email && password) {
            // call login
            this.props.siginCognito(email, password);
        } else if (!email && !password) {
            this.setState({ errMsg: formatMessage(messages.enter_username_password) });
        } else if (!email) {
            this.setState({ errMsg: formatMessage(messages.enter_username) });
        } else if (!password) {
            this.setState({ errMsg: formatMessage(messages.enter_password) });
        } else {
            this.setState({
                errMsg: formatMessage(messages.invalid_username_password),
            });
        }
    }

    render() {
        if (isLoggedIn()) {
            return <Redirect to="/" />;
        }

        const {
            intl: { formatMessage },
            login: { signedData },
        } = this.props;
        let { errMsg } = this.state;
        if (!errMsg && signedData && signedData.message) {
            errMsg = formatMessage(messages.invalid_username_password);
        }

        return (
            <Fragment>
                <Helmet>
                    <title>{formatMessage(messages.login)}</title>
                </Helmet>
                <div className="row background-login d-flex justify-content-center align-items-center">
                    <div className="col-md-3 offset-md-2">
                        <img
                            alt=""
                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAToAAABICAYAAABm3zkiAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MUExMzlGMTg0QTg1MTFFODhGNzVFNTYxQjVENEZBNDQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MUExMzlGMTk0QTg1MTFFODhGNzVFNTYxQjVENEZBNDQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxQTEzOUYxNjRBODUxMUU4OEY3NUU1NjFCNUQ0RkE0NCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxQTEzOUYxNzRBODUxMUU4OEY3NUU1NjFCNUQ0RkE0NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Po8dKEEAABglSURBVHja7F0JnBTV0X+zu7MnsFyiKAreMTEiYjQeRDCi8nlAQIz3jfHMJ4KgH0oMRkRExURjAirIIeCJBDwRQUAlgJxyyaWIoFzLsix7z/f/M2+lGaa7Xx8z28O8+v3q1z3T/apfv6O6ql5VvZCoA4gMFy1wOAH4C+BxwEMrI+H6P1S3OmKnaJabkZEhiJnyuPc8c995Rkbm/tczD7z/59+4FgqF3Ff2m1FdQreUfyM0aNCQspCVJMZ2NA6/B14AbE/GVnutrCZXzCo5V8wrbydqwoUiHA6L7GxgBjAzLMLZ2SJb/vfztXC24bz2WvbPv4338RgCw3MNq8fW18NEgwbN6MyYWyMcrgJeDzwr9npNJEPMLDpdTCtuLyqzCqMMS/eHBg0aUoHRgcH9EofewGuBcXnXt6XNxGubOonN1S2iEpjuB/P2jETycbhYSsOnASkdNwFmAouBW4HLgXOAU4FfQlWPKNIuxKEz8EJga+BRwAbASuAW4ErgbOBE0JzvoM5DcGga51IV8AHQ2qFA40Fp2ogHvUFjq+HekSb37ZT3Vio875845Me7hvI3Ge7jew3x2K39QfM7hTrlyL7pCGwLPBbYGMgpswu4DbgC+F/gJ8BZoFuDcmy3BxM0JL/DM/rLcflPj7QGgdYK+a52da6W70wsBW4ArgIuAY09dg8K+cjgjsFhIPBKM7oR/D1lUxsx6cd2IjOcE5XiyOh+VkFjVNKwP6prhhfVdWRB29DNpV/VAYNrJjv+NqAT9Xk1mSI6/1sb2g8DbzWb3HGAbfBX0J2kUPf1OLQ0ufw2aHRToDEdh/NMLh8NGusN91ox9sG4t6/C84pwKDRhdCHDfa1wWOexe9uA5EKLulAb6gO8XTI2ZSYE7ARk/36aoKG5CHU/FXVsiPMdHml1AK3p8p3bu6wzGeDn/BgDx4DeT/FuyvCBweUCH5dSxR/NmFxJVbYYuLSTeH3DWVBbQ0KD5aTrIRlWT4dMjsDFnUYWtK+RX8J7HTA5IaXJd1F+EvAQD6/XFeXvTGJz9sHzLkqhvr9G9v2DDpmckBJ5szSbLtRs2gGfprKI9nsW2MBXRgcGdzZFR+D/mamphM176omH5l8qlhS10FzMepBnAl/C6TAXDE6FPj9IY80kF0W4jKoSaB3ngQYH46+T2LSj8bzDAt73IeDTsn8a69ngCnKB9wEXoy1P8czowOBCwH44/UxKEKawbleh6PVlJ7GxtFB3gz2MkOpkIibSAPlB8gOovk0HTbdfLtqeXkf5giS16yGS2WUEuO+HAu/XU8AXoNlkpvFj6rjjweDqUYUB/k2KjaawtriB6PtFB1FcmaOb3p4R/a+IrlAngjalsEd8JnsE8A1KoS7L0/j89yQ2MRdz+ga072/E4c96FvgKVF8nom3Jr5ytuoLJUf//ANjG7t4fSvJFr1nnitKaHJGt/UbsBjpXUp9QuJXG5veBXwNLRHRlkytx7YEnmtBmR7+oQHs9cArtHEBKWmeKqO+j1aL4b4H3AJ9z+eq3oH5TQ6HQuCQ19WNc5MDzvkgQ/TLgQwr3bTT0T1MpzdlOKdn3S4FcOOHKOxcAuWDzq5h7a+27ZkBJ5xaTa1xgGGBRdotiW/RUuGe1w3bNlmo958sZUrOwArYPNc+HlBmdjGaYLieWJRRXhEWvz84UReVcWdWMTAHYiXkW14vkF38s3QdMGBrdeu4A9oi5dLeUvsyAS/NcmBgRSxs0OVCGSYZnBv1w3zCVJX4T+DfK/xfl1yShnSl9jsfzTlVxcXEB5aA71GEZumI1tLi+W6q0I8zcZKS9lCu0d/E37vveinni/i4WjK7YxTscAH7QsGtXaYejp8clFmXvxX0DlVRXMLnDVJkc1/n7zWwtNuwq0OxLTZqjiH2dxS30lToTHT3ajMnJgbUMSGbI0LoNknZITgAzqBBRV5SX49HGf2tF1IfvXQsatH9189AE9SXzSZbcz5XJl0wYfrL7nu98m8Ut9Blrh34YZuULiGurgX3k/FyRLnMH77xYRBfH/mVxGxlRV1uJDkyuvhSZj1V5+IjFLcWcTU20JKcOHW2kudvRoascdP4Gw89TpPhuBn/B/Z/b0KuSNqQ1Ul2KB5QQxnhog9Pll7l3ktqcLi534N2ME6TcB7pZ0h/MVDKP8Z87x6JN96p/uH+Bg77/MUAfcKt2KEFd5/nE7OhGyZXWCy3GescsGyZHiY/2k1NVHrpsaz3x/PxjRJZmck7gLKsmRUe+nSDatPE9pziYdmIwPU/GaHLL2T60Qy8840M86+MktTtdXD6jJOwjTUoPVk6vM0TUnqrSbrTjjUjhcW3VDotUeYri+CxHX5JP9TO5pbWdRNffRv/9GaprQuIvM45XdgYuzCopPzyvZG2jnD1L88PVixuEy1eGs0KbczKrNouM3IpM8NiV4uLiiMiOeG6JdW/kip2r7Jd+c5tmi7zm+6SrSHVIVJUuT/CAsHLP+cAjbSspfLpDu9pkC0bXnCFBoFdqQ2OnnABdTK6/CjqtQWeLT23LvqMhO97iGX2uxuF5Z3Ci1BEzsJK2P7IyVWg4AD6zYHQtsyykOXobK7skjFly6F6JLmwhzdXPKitrWX/r5D2R+oMHDnl1rj3Vt9Ohg6wM0Rs80m5qce1bh7TsYjO5GmbH6LgYQJsUV8wOj8cwga+A+VyOox+TnDbIq4HzpbQVC1TtBwm1FcJEQBMbiU6DOmwWVqzHhMlxUIwSin52u8ozxfNzzRf26ofLi5vklz6ypbLpvx55ZlSF7hNl8OqAaBXM7tTAkOXhWbVQD1LKNjCyG3BOFTWe+H8pkCFixT6pNSulDWe4yS33UWWuo/6t8bF/0h0y3Axe+tG0Un3C8PmHiqKyrAMWIDJDkZpmebufKaps+PC4l8aU676IC1YG5F96pL3FR9on20xYZXcNMJ9PwFwGC3MHXoZCrfWhbRvI572E53H12Gx1+FURzaySbNiSwL5PNzjK4tqOrDjSHD3Wlb20SyszxKuLDowjLsyu2FQaCV80ZvzkJYl+w0u7X3d0Tk7OuIL8vPoF+fmioCBf5PO4F/Oi5wUxvw3/5eM/Hs0muGraI5ew0uLaZXT4xfNLXNK2MrSfDdpHqaQLktDd4hrdG5xK6jSLMFqhbZxruT5NdONXnm42dIKOF7bmVyC8ncPw9zG/rey/HelMbExHlWLQ0yWDdwtWvp7r40l0g4SDiIk3lzURO8sy97PNNcqt+HxnRUGn2VPfKU5Gi05+Y8y6btfdyvApZTcYJ9qPiLoHJgpmCHNbKLOQ9BNq3vZmtK2YAF06rrMjIp2Rb7S4ZboLlbISdGk/Y/qneokeI3jedjyPORI/FT5k7TEBpw7DMy2ukdkzzPKOVORyPjsM241PZiy/weKWuRkx0hxXpzo7ecjYRftn7GmSV/HejrLs382eOrk4mQ371piXua/DuSIaIpNKwNUiq6/2g+jI2x10+gXA2+RgowQx2+L2a3HvAzb0qBK8a2MzmuByMrDP7kzi5PtMMo+gAJm8VW67P6H9+zjo+3OAd6eTviqTx44X1ot6k2O/bI6CnpdvyRMrt+3zxmicVzl7257sy+fO+LC6Ll561LDnufJCB9z1KfTloxH/BZvbGCbFAPrWJp3dBHgTkDGcNPIb7WnP2NAejHKvywyvRpp5QGZS4eq4lQvMQuEhySPen47Go5LY5APkxyUIfU9NwS6xwZPohyl0gzGb6MxhB5yGn7OAv0kTBtcK2FOaZ9rbmAs+yDJIc1zav8LJwyat3Jc2q2Fu1ffb9+R3WjZvWnVdNsCLQwdv7t3vURqe5yVDJfIJnuXXG2iVM419cwU6d4Ps3B3yK0aJy8qe9Y6IZmC1ck6l/a07aK+WEgajYRj4rRLH94APNkxKIUwQcEISmEu1VGHJoJv4TL4AtCcq3DcN9ahlcIzOuMfG5PI/RNDmh5xhT9tFNKdgC/lRCwWQEam0wxy0wxOK7UqzVAOJRwiL5LKxHzYKE0ZbHDvfUcqdT9dFc8yFMyM1RWXhC1cvmrkrCI085PFHV/Z/fDBtG2NECoCMPGCQ9RSFQXukRGWpgdKeULOFHWcjvR3wXQH9qT68fwnqyOzU3PciOwnt/T2edzO/1T6TzlI0/RQZ6lImQ+ymC3vb+GE2H8MgQWef2/ViF+U+Ar7ME6PqeoMTCj/tDosVW6Nqa7OCyoGrF81ZHqRWHtCvz1gpzaSKCssv1n0Jov2NlNoqfST7sZ/1lTGgvZLY3v8Ryc2HZ1UX2lFvERr8BGp0V9ZGl2RItfVkqaoow9yNUeGgSX7V9o3F2QMC+rLU4fekSs9IdaaHzwypljbDyZjpwY9ForeAXVy4lNgBbZXvJrHJaehfEJC+Hy2i24PuERq8AhfHzqemVPtHrUTX0SmlpT9F/c4Kc6r6fLtsfmUQ37Zvz3sY5jQslXqIzq0iGiI1NwG0GQHQRrhwB5FA0wTtSd0V4lrd1C8iJZvvk9TWdGKnylwSkL7nBOUmRDM1r3IF3PbxErTjVcD9zGi1jO73Til+DUYHaa5k7Y7coGdYeDIRElKCBzzVODq3/kFE92pVjfvkxOVS+ygL2muBHaR0R4anspBAB08ajY9H2RcS6UBNfzcRjU+tSVJbJ9XFRaE+K4C/E9FtC98T6hEbHOM01QxPI8ZWmyTiUeApaDfmbXwvbrtCbaWhj8kdGzh5QvsRJ4vm9StGjf9w1Y1Bb41XRo+bWFCQ39llZERmXWeRkNsLtpPSGDNecFWUDqXMPsuVOK6WMnCdm1eXOaTdXH7oSLulHAdlckwwaoMuC19wtdIBTa4Q1zOZyCMVaVDLMAugftMYLSIXW+IB8569qfi8bsJk5zVjnWVq+is8dikjSWYp1quR7HtGkDCFOFcA82Xf/yj7nur356pStvSNPN9Lm8mkodd4bAduhj1N0uMii9WCA80kpXJs0u90g5P8e2R0JwnrUKG4cMLfTxNHFpafOG3W16tSgNF1BVN7K1UZnQYNGrwBVdcTHUsYwEPrVexOBSYngSuE1bq7NWhIT6Da6spJs2Vh+fJZKfKSt1x/9a4Jb79LsbihG81RDxMNGlKf0TnecT2CqX9IQdXCVHrRP3btPFV3twYN6au6HuqmYEF29ULdfBo0aEgVRlfotFAoBEYXrlmqm0+DBg2pwujyXEp0Rbr5NGjQkApAG53jIGpGnWeFIikVqhIZvnfruA4uinYK9RDL9VDRoCG1Gd1ONwXvOmNz1d2p5YPNNDgtXZTTsYcaNBwEqmuZy7JNU+xd3da3TA8TDRpSn9Ftc1n2uFR5SaitfM9jXBbfpoeJBg2pz+jWuSybStuxkcm52SN1Y6hHaiUE0KBBQ3xG5zaM69wUek+3dV2lh4gGDQcHo1vhsuxZUAkLUuQ9L3BZboUeIho0pD5w1ZWuEzXC+V6XdEthXvjXgvyCYMb0E7zMZfGlSaojtxK8xGXxrVCvZxloMQ3LhTZlmPKGOebWo+wWB/Uk3XwXdfwOz/nKgtYSXF/jsM2Yuqo98FQR3SCIKZa4Qs58dky4yg2B5oPuHhs6zPvXXP7chPvnuOg/ui3VOt6vAY0lPvSrGUzR5hQXjA6NtgsdwnxWbV2UvynojE5Ek1c2cFk2WdviUTJ2u7/FDLH/dm/NnNBC35MpcL+Kf2As2KXrYrZmNy46r8qxYkaLKe+HKtb3Ihzul1K63ce5GPe/geOzeLevTe55SOzbyIVp3Lu4eD/u4la7FeVzYt9eGl761QyYn04767tQXQluA947YiC1Dvg79nZZ7kdMjnQIcyOz4Y5pi9GXjwEzg1hJ1KsFkKngP5ASq4oGwg8c96ZdgrJ685k0V10JTEfc1yWNfsArAzo5uB9mG5fFP63DqnNXqK0+qdfTxf5O4blSzWNGV6PLDRncw1JiuEfxuarq5lce+/EcEd2asHHMpY3AadL8sl2+2xFSnW0nfxNCUr1NNlQK+81+zhT7tjBktug5CjQ1eGB02+MMJBXojoHYHtLP9IAxObqTPOOBxJt1WP2HfWzPnqC10KSNmkqV8lGxb7Pqu/H/JJT5yIbuy7hnaBL6kRsFMXGqMSabaeMfAX6IOtSYlGPKc27x+KBIwsbY8QB1222nCqOeEw2q8xyU6SI0JEZ1RePSOD3eA50XpdE/SEDby4kuy+4ATj7YOx/9zoWMITjlZixGo/39AflYkRFPjGFyj1EKQr3fN2Ny8t1KgIxv/pVkdhV6uqc5o5MwygOdXwD/ESBp7jz5xXcLEzBJytNlEMgVUeO2kO1kNEldA5lw8xjptD+w2sG7VQG5E9xTerprRifksvpiD7RuxeS4IwBM7mgpnXqZqMPScCzMMJzT7aNBHfcjP543xHx8XKvK2iVDM7rYL6gXeAEDtFsdTg76VtG2dJgHMtMwKRak4ViIlZKq6rg+d4noIgKBiRXu09NVg1+MbpwQzhw349B7HQzn5jpgclxB/EJ4TzbwWJqOhfaGc8b41vXu9X8wnI9HfTbr6arBF0ZHe4aIGvG90nwFjGeQ3Bw7GUyOGzDTE/5Yj6TeC9rqcZLa7zdSgqqFd+q4Pq1waGH46209VTV4gXiMiG4VM0XUD8kL0C/vXAza28A8ViRoQtBPqr98llfjOZl8r4D0C9tNZWvGr9C233loP0q/N4roKmttdhe6RAxWKP4Ayt+kcB/7f57DqsWuls9PUrt3wDu52fTpRM1KUozRYVBG5KIC7VTZHunT0XMR6D2N41OgvcMnBkfbDf2NaFM8xqe2GJQohpxA9ZkmgpE290xAe8XGe4alxBS74ECD/Y1ohw0Kzz5coh3Uc/H+sTvT/ZSkdmd7tNZsIT0kOjK7ZZgcj+J0oA/PyJbq8J9B80URdTR1xVBQngOxu5S8TvKxHRiE/beDtI9VnWXXSOkrCKq7UTqvkCYVu7HBlXK7ZAZXgdaXetprRmcEqi8MoD7Pp2fR855xp70xKKmKMGaRLg0L6LhqoZrSzYD55M4HdhL7wnr8Aq7oXRMwvznGdKoY31f78Kyx0lwxycoBNw4MkWUTUcfdxg8lxkEjBW2AyQzsEg7YjR0y+Z4u6jtB1FH0hQaPjI5OmRhgV4lonGJzn5/bVmI/ydA4iH8AcqWvQqoQjaV6FUpwG/wpgMH7g3yUrNoYQ8DQ1kwbZIz6iOD6RBd0N5qFlvkAa2N+M5v17CS0+0437xTHNKAhYGBpwJdL+peLxO+ExUByhuowwLmdtJMcmQQmR7vhqHTqcLzvFBxeMPx1HSbq9QGrJlMqGSXsixTKXCvHkRFb6SmuwZbRyYnBFTNmJznYPMuZR69vmvZ7b8lMaoGxyscHiBmXSTXSyIyzbMrsBhYZUbjcylNDGjI6OYio6jAcp/ogeW+m/OHqYiQdO10ykqsNUhPtp+NlxpeggFHSZlhfDz1dNSSU0cnJwfjRrjEqRSrCGL6HykreQc7sloj9k5KeBnwiQFV8Xey/kDEYjPgkPWU1JJTRyclBSYgprFN1r9NBlEydZL84yIG2uvcMv3vKxYogMGJ+iO42/EV/vI9lsL8GDYljdHIAciOW04XHrLFJhlKqaqj7Q+mqrpr0JduCTsdGh9yRYCaHB6R+TNDwpOEvZg+eS4d2xfDCHN3LGghZLgfgegy0s6WEFPSsEgskk1upuztuX/6EvmQY2PvyLya7HIP/LrDxqztDMQSMQKdft5so0dmc7kZ3GiQ7Op73wfNHi+h+J1+D/na56xYz15wCvFhEV2I1aHAfdC8dbKnqMAB8uAiewyTrN0BEXUh0LjLrvvwA/TjU8NHqIBnM4xbFrpaoAlz9fM1l3Sh13oX6fSM/rLVhiVyg6C+Rvmx2QHPLJt3bWnV1O0m4JeCv+YUVwVnOp6f/SajbQM3klIHpxo2JV/8qN6UJCjN+Vo6zt8jXHBT9QTLs47VUryU6r4OQ0QxPYWIMl1LBvcLdRjtegIOfHv6PpWDizNjdorZ6oFUaQ2unYh+Wo/8ooRnjm+k/aYxIoM2smYs67Y7zn5HWGsU6rsLhCtSTzuRXScmTzI+2OzqXcxxykycyNMa0coewT2wWn+aYnDsBbi61Xp4vcVjWj+drsBs7CeE4w/f6ZXHS0PeuXYLfgVve0WWEyQK+0V2ankAfwHTa50NDABhdzAA8Wn59GcbzW+HPShi/2J+IaELGTx0Go2vQoEEzuoQyPWaPoN2Hm0rT+ZMLGAw94l4P8eyFu6RKQJWFmxQvA04HY9NGZQ0aNCjD/wswAEMGXsDxxofgAAAAAElFTkSuQmCC"
                        />
                        <span className="d-flex justify-content-center pt-3 dispatcherplatf">{formatMessage(messages.dispatcher_platform)}</span>
                    </div>
                    <hr className="d-none d-md-block" />
                    <div className="col-md-4">
                        {errMsg && (
                            <RedText>
                                {/* {formatMessage(signedData.code)} */}
                                {errMsg}
                            </RedText>
                        )}
                        <Form className="login-form" onSubmit={e => this.submitForm(e)} noValidate>
                            <FormGroup>
                                <Input
                                    type="email"
                                    className="form-control"
                                    id="email"
                                    name="email"
                                    aria-describedby="emailHelp"
                                    placeholder={formatMessage(messages.email_address)}
                                    onChange={e => {
                                        this.handleChange(e);
                                    }}
                                />
                            </FormGroup>
                            <FormGroup>
                                <div className="password-wrapper">
                                    {this.state.isPasswordVisible ? (
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="password"
                                            name="password"
                                            placeholder={formatMessage(messages.password)}
                                            onChange={e => {
                                                this.setState({ password: e.target.value });
                                                this.handleChange(e);
                                            }}
                                            value={this.state.password}
                                        />
                                    ) : (
                                        <input
                                            type="password"
                                            className="form-control"
                                            id="password"
                                            name="password"
                                            placeholder={formatMessage(messages.password)}
                                            onChange={e => {
                                                this.setState({ password: e.target.value });
                                                this.handleChange(e);
                                            }}
                                            value={this.state.password}
                                        />
                                    )}
                                    <div role="presentation" className="show-pass" onClick={() => this.setState(({ isPasswordVisible }) => ({ isPasswordVisible: !isPasswordVisible }))}>
                                        {this.state.isPasswordVisible ? <i className="fas fa-eye" /> : <i className="fas fa-eye-slash" />}
                                    </div>
                                </div>
                            </FormGroup>
                            <Button type="submit" className="btn btn-normal-enable">
                                {formatMessage(messages.login)}
                            </Button>
                            <div className="d-flex mt-4">
                                <div className="remember">
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                            {formatMessage(messages.remember_me)}
                                        </Label>
                                    </FormGroup>
                                </div>
                                <div className="forgot-password">{formatMessage(messages.forgot_password)}</div>
                            </div>
                        </Form>
                    </div>
                    <div className="col-md-2" />
                </div>
            </Fragment>
        );
    }
}

Login.propTypes = {
    siginCognito: PropTypes.func,
    intl: PropTypes.any,
    login: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    login: makeSelectLogin(),
});

export function mapDispatchToProps(dispatch) {
    return {
        // dispatch,
        siginCognito: (email, password) => dispatch(siginAction(email, password)),
    };
}

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(injectIntl(Login));

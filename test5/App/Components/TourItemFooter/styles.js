import { StyleSheet } from 'react-native';
// Colors, Metrics...
import { Colors, Fonts } from '../../Themes';

export default StyleSheet.create({
  tourItemFooter: {
    width: '100%',
    paddingTop: 10,
    marginBottom: 40,
    flexDirection: 'row',
    zIndex: 999,
    position: 'relative',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 10,
    backgroundColor: Colors.white, // Set background color to make the icon not transparent
  },
  finishBtn: {
    flex: 1,
    backgroundColor: Colors.main,
    borderRadius: 10,
  },
  finishBtnText: {
    fontSize: Fonts.size.small,
    color: Colors.white,
    textAlign: 'center',
    fontWeight: 'bold',
  },
});

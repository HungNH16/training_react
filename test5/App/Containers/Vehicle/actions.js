/*
 *
 * Vehicles Management actions
 *
 */
import {
    ADD_VEHICLE,
    ADD_VEHICLE_SUCCESS,
    ADD_VEHICLE_FAILED,
    UPDATE_INPUT,
    UPDATE_ERRORS,
    RESET_FORM,
    UPDATE_SUCCESS_FLAG,
    LOAD_VEHICLE_REQUEST_ACTION,
    LOAD_VEHICLE_SUCCESS_ACTION,
    LOAD_VEHICLE_FAILURE_ACTION,
    LOAD_MAKE_MODEL_SUCCESS_ACTION,
    RESET_STATE,
} from './constants';

export function addNewVehicle(data) {
    return {
        type: ADD_VEHICLE,
        data,
    };
}

export function addNewVehicleSuccess(response) {
    return {
        type: ADD_VEHICLE_SUCCESS,
        response,
    };
}

export function addNewVehicleFailed(errors, invalidFields) {
    return {
        type: ADD_VEHICLE_FAILED,
        errors,
        invalidFields,
    };
}

export function updateInput(name, value) {
    return {
        type: UPDATE_INPUT,
        name,
        value,
    };
}

export function updateErrors(errors, invalidFields) {
    return {
        type: UPDATE_ERRORS,
        errors,
        invalidFields,
    };
}

export function resetForm() {
    return {
        type: RESET_FORM,
    };
}

export function updateSuccessFlag(bool) {
    return {
        type: UPDATE_SUCCESS_FLAG,
        bool,
    };
}

export function loadVehicleAction(params) {
    return {
        type: LOAD_VEHICLE_REQUEST_ACTION,
        params,
    };
}

export function loadVehicleSuccessAction(data) {
    return {
        type: LOAD_VEHICLE_SUCCESS_ACTION,
        data,
    };
}

export function loadVehicleFailureAction(data) {
    return {
        type: LOAD_VEHICLE_FAILURE_ACTION,
        data,
    };
}

export function resetState() {
    return {
        type: RESET_STATE,
    };
}

export function loadMakeModelSuccessAction(data) {
    const options = [];
    data.data.map(item=>{
        const itemCustom = {...item}
        itemCustom.value = item.id;
        itemCustom.label = `${item.make} ${item.model}`;
        return options.push(itemCustom);
    })
    return {
        type: LOAD_MAKE_MODEL_SUCCESS_ACTION,
        data: options,
    };
}

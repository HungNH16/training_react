import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the driver state domain
 */

const selectDriverDomain = state => state.get('driver', initialState);

/**
 * Other specific selectors
 */

const selectDrivers = () => createSelector(selectDriverDomain, substate => substate.toJS());

const selectFormValues = createSelector(selectDriverDomain, obj => obj.toJS().formValues);

const selectErrors = createSelector(selectDriverDomain, obj => obj.toJS().errors);

const selectInvalidFields = createSelector(selectDriverDomain, obj => obj.toJS().invalidFields);

const selectSuccessFlag = createSelector(selectDriverDomain, obj => obj.toJS().addSuccessFlag);

export { selectDriverDomain, selectDrivers, selectFormValues, selectErrors, selectInvalidFields, selectSuccessFlag };

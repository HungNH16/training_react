import { fromJS } from 'immutable';
import { selectDriverDomain, selectDrivers, selectFormValues, selectErrors, selectInvalidFields, selectSuccessFlag } from '../selectors';
import { initialState } from '../reducer';

describe('selectDriverDomain', () => {
    it('Expect to have selectDriverDomain ', () => {
        const mockedState = fromJS({
            vehicle: initialState,
        });
        expect(selectDriverDomain(mockedState)).toEqual(initialState);
    });
});

describe('selectDrivers', () => {
    it('Expect to have drivers', () => {
        const makeSelector = selectDrivers();
        expect(makeSelector(initialState)).toEqual(initialState.toJS());
    });
});

describe('selectFormValues', () => {
    it('Expect to have form values', () => {
        expect(selectFormValues(initialState)).toEqual(initialState.toJS().formValues);
    });
});

describe('selectErrors', () => {
    it('Expect to have array of error messages', () => {
        expect(selectErrors(initialState)).toEqual(initialState.toJS().errors);
    });
});

describe('selectInvalidFields', () => {
    it('Expect to have array of invalid field names', () => {
        expect(selectInvalidFields(initialState)).toEqual(initialState.toJS().invalidFields);
    });
});

describe('selectSuccessFlag', () => {
    it('Expect to have success pop-up flag', () => {
        expect(selectSuccessFlag(initialState)).toEqual(initialState.toJS().addSuccessFlag);
    });
});

/**
 *
 * Vehicle Assignment Reducer
 *
 * This contains Redux state for Vehicle Assignment screen
 * and how actions handle state
 */
// ===== IMPORTS =====
// Utilities, Constants & Styles
import { fromJS } from 'immutable';
import { get as _get, isEmpty as _isEmpty } from 'lodash';
import { alphabeticalSort } from '../../utils';
import {
    ADD_ROW,
    DELETE_ROW,
    LOAD_DATA,
    LOAD_DATA_SUCCESS,
    LOAD_DRIVER,
    LOAD_DRIVER_SUCCESS,
    LOAD_VEHICLE,
    LOAD_VEHICLE_SUCCESS,
    RESET_STATE,
    REVERT_DATA,
    SUBMIT_DATA,
    SUBMIT_DATA_SUCCESS,
    UPDATE_DRIVER,
    UPDATE_ERRORS,
    UPDATE_STATUS,
    UPDATE_SUCCESS_FLAG,
    UPDATE_VEHICLE,
    VEHICLE_ASSIGNMENT_COLUMN_MAP,
} from './constants';
import { LIST } from '../../constants';
// ===================

// ===== REDUCERS =====
export const initialForm = {
    [VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME]: '',
    [VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS]: '',
    [VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE]: '',
    [VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS]: '',
};

export const initialState = fromJS({
    data: [],
    originalValues: [initialForm],
    rowValues: [initialForm],
    drivers: [],
    vehicles: [],
    errors: [],
    invalidFields: [],
    successFlag: false,
});

export const convertToEditValues = rawData =>
    !_isEmpty(rawData)
        ? rawData.map(row => ({
            [VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME]: {
                label: row.driverName,
                value: row.driverID,
                status: row.driverStatus,
            },
            [VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS]: LIST.VA_DRIVER_STATUSES.find(opt => opt.value === row.driverStatus),
            [VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE]: {
                label: `${_get(row, 'licensePlate', '')}${row.makeModel ? ` - ${_get(row, 'makeModel.make', '')} ${_get(row, 'makeModel.model', '')}` : ''}`,
                value: row.licensePlate,
                status: row.vehicleStatus,
            },
            [VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS]: LIST.VA_VEHICLE_STATUSES.find(opt => opt.value === row.vehicleStatus),
        }))
        : [initialForm];

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_ROW:
            return state.update('rowValues', rows => rows.concat(initialForm));
        case DELETE_ROW:
            return state.update('rowValues', rows => rows.filter((_, index) => index !== action.id));
        case LOAD_DATA:
            return state
                .set('data', [])
                .set('errors', [])
                .set('invalidFields', []);
        case LOAD_DATA_SUCCESS: {
            const originalValues = convertToEditValues(_get(action, 'response.data', []));
            return state
                .set('data', _get(action, 'response.data', []))
                .set('rowValues', originalValues)
                .set('originalValues', originalValues);
        }
        case LOAD_DRIVER:
            return state.set('drivers', []);
        case LOAD_DRIVER_SUCCESS:
            return state.set(
                'drivers',
                alphabeticalSort(
                    _get(action, 'response.data', []).map(row => ({
                        label: row.driverName,
                        value: row.driverID,
                        status: +row.status,
                    })),
                    ['label'],
                ),
            );
        case LOAD_VEHICLE:
            return state.set('vehicles', []);
        case LOAD_VEHICLE_SUCCESS:
            return state.set(
                'vehicles',
                alphabeticalSort(
                    _get(action, 'response.data', []).map(row => ({
                        label: `${_get(row, 'licensePlate', '')}${row.makeModel ? ` - ${_get(row, 'makeModel.make', '')} ${_get(row, 'makeModel.model', '')}` : ''}`,
                        value: row.licensePlate,
                        status: +row.status,
                    })),
                    ['label'],
                ),
            );
        case RESET_STATE:
            return initialState;
        case REVERT_DATA:
            return state
                .set('rowValues', convertToEditValues(state.get('data')))
                .set('errors', [])
                .set('invalidFields', []);
        case SUBMIT_DATA:
            return state
                .set('errors', [])
                .set('invalidFields', [])
                .set('successFlag', false);
        case SUBMIT_DATA_SUCCESS:
            return state.set('successFlag', !!_get(action, 'response.data.categoryID'));
        case UPDATE_DRIVER:
            return state
                .update('rowValues', rows =>
                    rows.map((row, rowIdx) => {
                        if (rowIdx === action.rowId) {
                            return {
                                ...row,
                                driverName: action.value,
                                driverStatus: LIST.VA_DRIVER_STATUSES.filter(opt => opt.value !== 5)
                                    .map(opt => opt.value)
                                    .includes(_get(action, 'value.status'))
                                    ? LIST.VA_DRIVER_STATUSES[3]
                                    : LIST.VA_DRIVER_STATUSES[4],
                            };
                        }
                        return row;
                    }),
                )
                .update('invalidFields', fields =>
                    fields.filter(
                        field => field !== `data_${action.rowId}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME}` && field !== `data_${action.rowId}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS}`,
                    ),
                );
        case UPDATE_ERRORS:
            return state.set('errors', action.errors).set('invalidFields', action.invalidFields);
        case UPDATE_STATUS:
            return state.update('rowValues', rows =>
                rows.map(
                    (row, rowIdx) =>
                        rowIdx === action.rowId
                            ? {
                                ...row,
                                [action.name]: action.value,
                            }
                            : row,
                ),
            );
        case UPDATE_SUCCESS_FLAG:
            return state.set('successFlag', action.bool);
        case UPDATE_VEHICLE:
            return state
                .update('rowValues', rows =>
                    rows.map((row, rowIdx) => {
                        if (rowIdx === action.rowId) {
                            return {
                                ...row,
                                vehicle: action.value,
                                vehicleStatus: LIST.VA_VEHICLE_STATUSES.filter(opt => opt.value !== 5)
                                    .map(opt => opt.value)
                                    .includes(_get(action, 'value.status'))
                                    ? LIST.VA_VEHICLE_STATUSES[3]
                                    : LIST.VA_VEHICLE_STATUSES[4],
                            };
                        }
                        return row;
                    }),
                )
                .update('invalidFields', fields =>
                    fields.filter(
                        field => field !== `data_${action.rowId}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE}` && field !== `data_${action.rowId}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS}`,
                    ),
                );
        default:
            return state;
    }
};
// ====================

import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------------Types and Action Creators-----------------*/

const { Types, Creators } = createActions({
  setRetailer: ['payload'],
  setIsFetchingRetailer: ['payload'],
  loadRetailer: ['payload'],
});

export const RetailerTypes = Types;
export default Creators;

/* ------------------Initial State-----------------*/

export const INITIAL_STATE = Immutable({
  Retailer: [],
  isFetchingRetailer: true,
});

/* ------------------Reducers-----------------*/

// eslint-disable-next-line no-undef
const setRetailer = (state, { payload: Retailer }) => state.merge({ Retailer });
const setFetchingRetailer = (state, { payload: isFetchingRetailer }) => state.merge({ isFetchingRetailer });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_RETAILER]: setRetailer,
  [Types.SET_IS_FETCHING_RETAILER]: setFetchingRetailer,
});

import React from "react";
import { Input, FormGroup, Button,Label } from 'reactstrap';
import './style.scss';

export class Terms extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isUserAgree: false,
        };
        this.onCheckboxPressed = this.onCheckboxPressed.bind(this);
        this.onCancelButtonPressed = this.onCancelButtonPressed.bind(this);
        this.onContinueButtonPressed = this.onContinueButtonPressed.bind(this);
    }

    onCheckboxPressed() {
        const { isUserAgree } = this.state;

        this.setState({
            isUserAgree: !isUserAgree,
        });
    }

    onCancelButtonPressed() {}

    onContinueButtonPressed() {}

    render() {

        const { isUserAgree } = this.state; 
        return (
            <div>
                <div>
                    <Label>
                        <Input type="text" />
                            Terms of Conditions
                    </Label>
                </div>
                <div>
                    <div styles={{ height: '300px', overflowY: 'scoll' }}>
                        <Label>
                            <Input type="text" />
                            Term & Conditions
                        </Label>
                        <p>
                            Please read these terms and conditions of use carefully before using this mobile application (the “<b>App</b>”).
                            By accessing or using this App or any functionality thereof on any computer, mobile phone, tablet, console
                            or other device (together “<b>Devices</b>” and each a “<b>Device</b>”), you confirm that you have read, understood and agreed
                            to be bound by these Terms and Conditions of Use and any other applicable law.
                            {'\n \n \n'}
                            If you do not agree to these Terms and Conditions of Use, please do not access or use the App or any functionality thereof.
                            1. Owner and Operator Information 1.1 The App is owned and operated by Connected Freight Pte Ltd,
                            a company whose registered office is at 9 North Buona Vista Drive, The 2.3
                        </p> 
                    </div>
                </div>
                <div>
                    <div className="remember">
                        <FormGroup check>
                            <Label check>
                                <Input 
                                    id="checkboxLabel"
                                    type="checkbox" 
                                    className="form-check-input" 
                                    isChecked={isUserAgree}
                                    onPress={this.onCheckboxPressed}
                                />
                                I have read and agree to the Terms of Use
                            </Label>
                        </FormGroup>
                    </div>
                </div>
                <div>
                    <Button
                        className="termOfUseButton"
                        value="CONTINUE"
                        disable={!isUserAgree}
                        onPress={onContinueButtonPressed}
                    />
                    <Button
                        className="termOfUseButton"
                        value="CANCEL"
                        onPress={onCancelButtonPressed}
                    />
                </div>
            </div>
        );
    }
}
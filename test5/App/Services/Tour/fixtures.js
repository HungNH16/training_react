// GET /scheduled-tour-plans/{tourID}
export const fetchTour = () => ([
  {
    type: 'pick-up',
    location: {
      name: 'DairyFarm Nature Park 1, 1123, Quezon, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '10:00 - 11:30',
    name: 'Company ABC',
    passed: true,
  },
  {
    type: 'pick-up',
    location: {
      name: '1040 Gen Luna Cor Apacible St Parco, 1007, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '11:00 - 11:30',
    name: 'Company DEF',
    passed: false,
  },
  {
    type: 'drop-off',
    location: {
      name: '1015 Edsa Cor Bansalangin Project 7, 1105, Quezon, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '12:00 - 12:30',
    name: 'Company GHI',
    passed: false,
  },
  {
    type: 'drop-off',
    location: {
      name: '1031 Kaligaya Nouveau Avenue, 1343, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '14:00 - 14:30',
    name: 'Company MNO',
    passed: false,
  },
  {
    type: 'drop-off',
    location: {
      name: '1331 Sunshine Bay Street, 1423, Quezon Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '15:00 - 15:30',
    name: 'Company PQR',
    passed: false,
  },
]);

// import React from 'react';
// import { mount } from 'enzyme';
// import { enzymeFind } from 'styled-components/test-utils';

import { mapDispatchToProps } from '../index';
const dispatch = jest.fn();

describe('mapDispatchToProps', () => {
    describe('dispatch', () => {
        it('should be injected', () => {
            const result = mapDispatchToProps(dispatch);
            expect(result.siginCognito).toBeDefined();
        });
    });
});
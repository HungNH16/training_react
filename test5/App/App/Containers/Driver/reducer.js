/*
 *
 * Drivers Management reducer
 *
 */

import { fromJS } from 'immutable';
import { get as _get } from 'lodash';
import queryString from 'query-string';
import {
    LOAD_DRIVER_REQUEST_ACTION,
    LOAD_DRIVER_SUCCESS_ACTION,
    LOAD_DRIVER_FAILURE_ACTION,
    ADD_DRIVER,
    ADD_DRIVER_SUCCESS,
    UPDATE_INPUT,
    UPDATE_ERRORS,
    RESET_FORM,
    UPDATE_SUCCESS_FLAG,
    ADD_DRIVER_FAILED,
    RESET_STATE,
} from './constants';

export const initialForm = {
    name: '',
    dialCode: '65',
    phone: '',
    status: '',
    tags: '',
    workingHours: [
        {
            fromDay: 1,
            toDay: 1,
            fromTime: '00:00',
            toTime: '00:00',
        },
    ],
};

export const initialState = fromJS({
    drivers: [],
    pagination: {
        perPage: 10,
        total: 0,
        currentPage: 0,
    },
    addSuccessFlag: false,
    formValues: fromJS(initialForm),
    invalidFields: [],
    errors: [],
});

export default (state = initialState, action) => {
    switch (action.type) {
        case LOAD_DRIVER_REQUEST_ACTION: {
            const parsed = queryString.parse(window.location.search);
            const currentPage = _get(parsed, 'page', 1);
            return state.setIn(['pagination', 'currentPage'], currentPage);
        }
        case LOAD_DRIVER_SUCCESS_ACTION: {
            const drivers = _get(action, 'data.data', []);
            let pagination = _get(action, 'data.meta.pagination', {});

            if (drivers.length === 0) {
                pagination = {
                    perPage: 10,
                    total: 0,
                    currentPage: 0,
                };
            }
            return state.set('drivers', drivers).set('pagination', fromJS(pagination));
        }
        case LOAD_DRIVER_FAILURE_ACTION:
            return state.set('errors', action.data);
        case ADD_DRIVER:
            return state.set('addSuccessFlag', false);
        case ADD_DRIVER_SUCCESS:
            // if (!_get(action, 'response.data.driverId')) {
            //     return state.set('errors', ['Error ....']);
            // }
            return state.set('addSuccessFlag', true);
        case ADD_DRIVER_FAILED:
            return state.set('errors', action.errors).set('invalidFields', action.invalidFields);
        case UPDATE_INPUT: {
            const workingHoursField = ['fromDay', 'toDay', 'fromTime', 'toTime']
            let fieldName = action.name;
            fieldName = fieldName.replace(new RegExp("_[0-9]{1,}", "g"), "");

            if (workingHoursField.includes(fieldName)) {
                const workingHourIndex = action.name.replace(`${fieldName}_`, '');
                let workingHours = state.getIn(['formValues', 'workingHours']).toJS();

                if (action.value === 'removeWHRange') {
                    // update all workingHours
                    const whTemp = [];
                    workingHours.map((wh, index) => {
                        if (index === workingHourIndex) {
                            return false;
                        }
                        whTemp.push(wh);
                        return whTemp;
                    })
                    workingHours = whTemp;
                } else if (!workingHours[workingHourIndex]) {
                    workingHours[workingHourIndex] = {};
                    workingHours[workingHourIndex][fieldName] = action.value;
                } else {
                    workingHours[workingHourIndex][fieldName] = action.value;
                }

                return state.setIn(['formValues', 'workingHours'], fromJS(workingHours));
            }
            return state.setIn(['formValues', action.name], action.value).set('invalidFields', state.get('invalidFields').filter(field => field !== action.name));
        }
        case UPDATE_ERRORS:
            return state.set('errors', action.errors).set('invalidFields', action.invalidFields);
        case RESET_FORM:
            return state
                .set('formValues', fromJS(initialForm))
                .set('errors', [])
                .set('invalidFields', []);
        case UPDATE_SUCCESS_FLAG:
            return state.set('addSuccessFlag', action.bool);
        case RESET_STATE:
            return initialState;
        default:
            return state;
    }
};

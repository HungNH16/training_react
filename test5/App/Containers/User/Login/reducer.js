/*
 *
 * Login reducer
 *
 */

import { fromJS } from 'immutable';
import { DEFAULT_ACTION, SIGIN_SUCCESS } from './constants';

export const initialState = fromJS({});

function loginReducer(state = initialState, action) {
    switch (action.type) {
        case DEFAULT_ACTION:
            return state;
        case SIGIN_SUCCESS:
            return state.set('signedData', action.data);
        default:
            return state;
    }
}

export default loginReducer;

import { takeLatest, call, put } from 'redux-saga/effects';
import { siginedAction } from './actions';
import { SIGIN_ACTION } from './constants';
import { authenticateUser } from '../../../../services/auth';

// import request from 'utils/request';

export function* loginCognito({ email, password }) {
    const data = yield call(authenticateUser, email, password);
    yield put(siginedAction(data));
}

// Individual exports for testing
export default function* loginSaga() {
    yield takeLatest(SIGIN_ACTION, loginCognito);
}

/**
 *
 * Asynchronously loads the component for Vehicle
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));

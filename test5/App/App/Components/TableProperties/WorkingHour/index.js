import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Input, Container } from 'reactstrap';
import { Checkbox } from 'cf-web-ui-kit-components';
import { LIST } from '../../../constants';

const WorkingHours = (props) => {
    const { index, onDeleteRow, handleChangeInput, invalidFields, workingHoursSelected, added } = props;
    let fromDaySelected;
    let toDaySelected;
    if (added && workingHoursSelected.length && workingHoursSelected[index]) {
        // remove before option
        fromDaySelected = workingHoursSelected[index].fromDay;
        toDaySelected = workingHoursSelected[index].toDay;

    } else {
        fromDaySelected = index;
        toDaySelected = index;
    }

    const renderDayList = (indexSelected) => LIST.WEEK_DAYS.filter(item => item.value >= indexSelected).map(time => <option value={time.value}>{time.label}</option>);
    const renderTimeList = () => LIST.TIME_INTERVALS.map(time => <option value={time.value}>{time.label}</option>);

    return (
        <Container className="wh-row">
            <Row id="working-hour" className="align-items-center">
                <Col className="wh-day">
                    <Input type="select" name={`fromDay_${index}`} onChange={(e) => handleChangeInput(`fromDay_${index}`, parseInt(e.target.value, 10))} invalid={invalidFields.includes(`workingHours.${index}.fromDay`)} value={workingHoursSelected[index] ? workingHoursSelected[index].fromDay : ''}>
                        {renderDayList(fromDaySelected)}
                    </Input>
                </Col>
                <Col className="col-md-auto">to</Col>
                <Col className="wh-day">
                    <Input type="select" name={`toDay_${index}`} onChange={(e) => handleChangeInput(`toDay_${index}`, parseInt(e.target.value, 10))} invalid={invalidFields.includes(`workingHours.${index}.toDay`)} value={workingHoursSelected[index] ? workingHoursSelected[index].toDay : ''}>
                        {renderDayList(toDaySelected)}
                    </Input>
                </Col>
                <Col className="pl-3">
                    <Input type="select" name={`fromTime_${index}`} onChange={(e) => handleChangeInput(`fromTime_${index}`, e.target.value)} invalid={invalidFields.includes(`workingHours.${index}.fromTime`)} value={workingHoursSelected[index] ? workingHoursSelected[index].fromTime : ''}>
                        {renderTimeList()}
                    </Input>
                </Col>
                <Col className="col-md-auto">to</Col>
                <Col>
                    <Input type="select" name={`toTime_${index}`} onChange={(e) => handleChangeInput(`toTime_${index}`, e.target.value)} invalid={invalidFields.includes(`workingHours.${index}.toTime`)} value={workingHoursSelected[index] ? workingHoursSelected[index].toTime : ''}>
                        {renderTimeList()}
                    </Input>
                </Col>
                <Col lg="1" md="1" sm="1" xs="1" className="pl-1">
                    {index > 0 ?
                        (<div role="presentation" className="btn-icon btn-icon--small" onClick={() => onDeleteRow(index)}>
                            <div className="icon">
                                <i className="far fa-times" />
                            </div>
                        </div>)
                        : ''}
                </Col>
            </Row>
        </Container>
    )
}

WorkingHours.propTypes = {
    index: PropTypes.number,
    onDeleteRow: PropTypes.func,
    handleChangeInput: PropTypes.func,
    invalidFields: PropTypes.array,
    workingHoursSelected: PropTypes.array,
    added: PropTypes.bool,
};

class WorkingHourBlock extends Component {
    constructor(props) {
        super(props);

        const { handleChangeInput } = props;
        this.state = {
            weekDays: [{
                component: WorkingHours,
                getProps: {
                    index: 0,
                    handleChangeInput,
                    invalidFields: [],
                },
            }],
        };
        this.addOperatingHours = this.addOperatingHours.bind(this);
        this.onDeleteRow = this.onDeleteRow.bind(this);
        this.handleChangeInput = handleChangeInput;
    }

    addOperatingHours(event) {
        event.preventDefault();
        const { weekDays } = this.state;
        const { value } = this.props;
        const lastWDay = value[weekDays.length - 1]
        const index = weekDays.length;
        weekDays.push({
            added: true,
            component: WorkingHours,
            getProps: {
                index,
                handleChangeInput: this.handleChangeInput,
                onDeleteRow: this.onDeleteRow,
                invalidFields: [],
            },
        })

        const fromDay = lastWDay.toDay + 1;
        const toDay = lastWDay.toDay + 1;
        this.setState({
            weekDays,
        });
        this.handleChangeInput(`fromDay_${index}`, fromDay > 7 ? 1 : fromDay);
        this.handleChangeInput(`toDay_${index}`, toDay > 7 ? 1 : toDay);
        this.handleChangeInput(`fromTime_${index}`, '00:00');
        this.handleChangeInput(`toTime_${index}`, '00:00');
    }

    onDeleteRow(index) {
        const { weekDays } = this.state;
        let i = 0;
        const weeDaysTmp = [];
        weekDays
            .filter((item, itemIndex) => itemIndex !== index)
            .map((wd) => {
                weeDaysTmp.push({
                    component: wd.component,
                    getProps: {
                        index: i,
                        handleChangeInput: this.handleChangeInput,
                        onDeleteRow: this.onDeleteRow,
                        invalidFields: [],
                    },
                });
                i += 1;
                return wd;
            })

        this.setState({
            weekDays: weeDaysTmp,
        });

        this.handleChangeInput(`fromDay_${index}`, 'removeWHRange');
    }

    render() {
        const { weekDays } = this.state;
        const { invalidFields, value } = this.props;

        return (
            <div>
                {weekDays.map(({ component: WHComponent, getProps, added }) => <WHComponent {...getProps} invalidFields={invalidFields} workingHoursSelected={value} added={added} />)}
                <Row className="add-operating-hour-btn align-items-center">
                    <Col>
                        <div role="presentation" className="btn-icon btn-icon--small" onClick={this.addOperatingHours}>
                            <div className="icon">
                                <i className="fas fa-plus" />
                            </div>
                            <span className="ml-2">Add Operating Hours</span>
                        </div>
                    </Col>
                    <Col>
                        <Checkbox value="Include eve of PH & PH" />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default WorkingHourBlock;
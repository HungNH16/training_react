import React, { Component } from 'react';
import { CheckBox, View } from 'react-native';

class Boxes extends Component {
    constructor() {
        super();
        this.state = {
          data: [
            {
              name: 'Bulk Box 1',
              text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
            },
            {
              name: 'Bulk Box 2',
              text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
            },
            {
              name: 'Bulk Box 3',
              text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
            },
            {
              name: 'Bulk Box 4',
              text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
            },
            {
              name: 'Bulk Box 5',
              text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
            },
            {
              name: 'Bulk Box 6',
              text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
            },
            {
              name: 'Bulk Box 7',
              text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
            },
            {
              name: 'Bulk Box',
              text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
            },
          ],
          checked: []
        }
      }
      
      componentWillMount() {
        let { data, checked } = this.state;
        let intialCheck = data.map(x => false);
        this.setState({ checked: intialCheck })
      }
      
      handleChange = (index) => {
        let checked = [...this.state.checked];
        checked[index] = !checked[index];
        this.setState({ checked });
      }
      
      render() {
        let { data, checked } = this.state;
        return (
          <View
            data={data}
            renderItem={({ item, index }) =>
            <CheckBox
            center
            title={item.name}
            text = {item.text}
            onPress={() => this.handleChange(index)}
            checked={checked[index]} />
            }
          />
        );
      }
}

export default Boxes;
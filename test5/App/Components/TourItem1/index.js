/* eslint-disable indent */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-indent */
/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import FAI from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';
// Styles
import styles from './styles';
// Colors, Fonts
import { Colors, Fonts } from '../../Themes';
// Enum
export const typeEnum = { // Make an enum to make sure consistent use of drop-of and pick-up
  DROPOFF: 'drop-off',
  PICKUP: 'pick-up',
};

class TourItem1 extends PureComponent {
  render() {
    const {
      type,
      name,
      location,
      time,
      passed,
    } = this.props;
    let timeLineLineStyle = [styles.timeLineLine];
    let timeLineIconColor = Colors.blue;
    let detailTypeLabelStyle = [styles.detailTypeLabel];
    let detailContainerStyle = [styles.detailContainer];
    let clockIconColor = Colors.blue;
    let detailTimeStyle = [styles.detailTime];
    let navigateBtnStyle = [styles.navigateBtn];
    let detailReportIssueBtnStyle = [styles.detailReportIssueBtn];

    if (passed) {
      timeLineLineStyle = timeLineLineStyle.concat(styles.__passedTimeLineLine);
      timeLineIconColor = Colors.shuttleGrey;
      detailTypeLabelStyle = detailTypeLabelStyle.concat(styles.__passedDetailTypeLabel);
      detailContainerStyle = detailContainerStyle.concat(styles.__passedDetailContainer);
      clockIconColor = Colors.shuttleGrey;
      detailTimeStyle = detailTimeStyle.concat(styles.__passedDetailTime);
      navigateBtnStyle = navigateBtnStyle.concat(styles.__passedNavigateBtn);
      detailReportIssueBtnStyle = detailReportIssueBtnStyle.concat(styles.__passedDetailReportIssueBtn);
    } else if (type === typeEnum.DROPOFF) {
      timeLineLineStyle = timeLineLineStyle.concat(styles.__dropOffTimeLineLine);
      timeLineIconColor = Colors.darkBlue;
      detailTypeLabelStyle = detailTypeLabelStyle.concat(styles.__dropOffDetailTypeLabel);
      detailContainerStyle = detailContainerStyle.concat(styles.__dropOffDetailContainer);
      clockIconColor = Colors.darkBlue;
      detailTimeStyle = detailTimeStyle.concat(styles.__dropOffDetailTime);
      navigateBtnStyle = navigateBtnStyle.concat(styles.__dropOffNavigateBtn);
    }
    return (
      <View style={styles.tourItem}>
              <View style={styles.timeLineTree}>
                  <View style={styles.timeLineTreeContainer}>
                      <View style={styles.timeLineIcon}>
                          <FAI name="dot-circle" size={20} color={timeLineIconColor} />
                        </View>
                      <View style={timeLineLineStyle} />
                    </View>
                </View>
              <View style={styles.detail}>
                  <View style={detailContainerStyle}>
                      <View style={styles.leftDetailContainer}>
                          <View style={styles.detailRow}>
                              <View style={detailTypeLabelStyle}>
                                  <Text style={styles.detailTypeText}>{type}</Text>
                                </View>
                              <FAI name="clock" size={Fonts.size.tiny} color={clockIconColor} />
                              <Text style={detailTimeStyle}>
                                  {time}
                                </Text>
                            </View>
                          <Text style={styles.detailCompanyName}>{name}</Text>
                          <View style={styles.detailLocationRow}>
                              <FAI name="map-marker-alt" size={Fonts.size.small} color={Colors.shuttleGrey} />
                              {/* <Text style={styles.detailLocationText}>
                                  {location.name}
                                </Text> */}
                            </View>
                        </View>
                    </View>
                </View>
            </View>
    );
  }
}
// eslint-disable-next-line no-undef
TourItem1.propTypes = {
    type: PropTypes.string.isRequired,
    location: PropTypes.objectOf({
      name: PropTypes.string,
      long: PropTypes.number,
      lat: PropTypes.number,
    }).isRequired,
    passed: PropTypes.bool.isRequired,
    time: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  };
// eslint-disable-next-line eol-last
export default TourItem1;
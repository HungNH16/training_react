// GET /scheduled-tour-plans/{tourID}
export const fetchTour1 = () => ([
  {
    type: 'pick-up',
    location: {
      name: 'DairyFarm Nature Park 1, 1123, Quezon, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '10:00 - 11:30',
    name: 'Company ABC',
    passed: true,
  },
  {
    type: 'pick-up',
    location: {
      name: 'DairyFarm Nature Park 1, 1123, Quezon, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '10:00 - 11:30',
    name: 'Company ABC',
    passed: true,
  },
  {
    type: 'pick-up',
    location: {
      name: '1040 Gen Luna Cor Apacible St Parco, 1007, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '11:00 - 11:30',
    name: 'Company DEF',
    passed: false,
  },
  {
    type: 'drop-off',
    location: {
      name: '1015 Edsa Cor Bansalangin Project 7, 1105, Quezon, Manila, The Philippines',
      long: 11,
      lat: 11,
    },
    time: '12:00 - 12:30',
    name: 'Company GHI',
    passed: false,
  },
]);

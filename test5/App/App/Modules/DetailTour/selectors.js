export const getTourState1 = state => (state.tour1);
export const getTour1 = state => getTourState1(state).tour1;
// eslint-disable-next-line eol-last
export const getIsFetchingTour1 = state => getTourState1(state).isFetchingTour1;
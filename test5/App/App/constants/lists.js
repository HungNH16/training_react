// Dial code options for telephone input field
const COUNTRY_LOCALES = ['sg', 'ph', 'vn'];

// Drivers management - Status options
const DRIVER_STATUSES = [
    {
        label: 'On tour',
        value: '1',
    },
    {
        label: 'Standby',
        value: '2',
    },
    {
        label: 'No tour',
        value: '3',
    },
    {
        label: 'Active',
        value: '4',
    },
    {
        label: 'Unavailable',
        value: '5',
    },
];

// Vehicles management - Status options
const VEHICLE_STATUSES = [
    {
        label: 'On tour',
        value: '1',
    },
    {
        label: 'Standby',
        value: '2',
    },
    {
        label: 'No tour',
        value: '3',
    },
    {
        label: 'Active',
        value: '4',
    },
    {
        label: 'Unavailable',
        value: '5',
    },
];

// Vehicles management - Characteristics options
const VEHICLE_CHARACTERISTICS = [
    {
        label: 'None',
        value: '',
    },
    {
        label: 'Cold Storage',
        value: 'Cold Storage',
    },
    // {
    //     label: 'Zone Restriction - North',
    //     value: 'Zone Restriction - North',
    // },
    // {
    //     label: 'Zone Restriction - South',
    //     value: 'Zone Restriction - South',
    // },
    // {
    //     label: 'Zone Restriction - East',
    //     value: 'Zone Restriction - East',
    // },
    // {
    //     label: 'Zone Restriction - West',
    //     value: 'Zone Restriction - West',
    // },
];

// Week Days
const WEEK_DAYS = [
    {
        label: 'Mon',
        value: 1,
    },
    {
        label: 'Tue',
        value: 2,
    },
    {
        label: 'Wed',
        value: 3,
    },
    {
        label: 'Thu',
        value: 4,
    },
    {
        label: 'Fri',
        value: 5,
    },
    {
        label: 'Sat',
        value: 6,
    },
    {
        label: 'Sun',
        value: 7,
    },
];

// Time Options in a Day (Interval = 15 mins)
const generateTimeString = (hour, min) => `${`0${hour}`.slice(-2)}:${`0${min}`.slice(-2)}`;
const generateTimeIntervals = () => {
    const result = [];
    let hour = 0;
    let min = 0;
    while (hour < 24) {
        for (let i = 0; i < 4; i += 1) {
            const timeStr = generateTimeString(hour, min);
            result.push({ label: timeStr, value: timeStr });
            min += 15;
        }
        min = 0;
        hour += 1;
    }

    return result;
};
const TIME_INTERVALS = generateTimeIntervals();

// Vehicle Assignment - Driver Statuses
const VA_DRIVER_STATUSES = [
    {
        label: 'On tour',
        value: 1,
    },
    {
        label: 'Standby',
        value: 2,
    },
    {
        label: 'No tour',
        value: 3,
    },
    {
        label: 'Active',
        value: 4,
    },
    {
        label: 'Unavailable',
        value: 5,
    },
];

// Vehicle Assignment - Vehicle Statuses
const VA_VEHICLE_STATUSES = [
    {
        label: 'On tour',
        value: 1,
    },
    {
        label: 'Standby',
        value: 2,
    },
    {
        label: 'No tour',
        value: 3,
    },
    {
        label: 'Active',
        value: 4,
    },
    {
        label: 'Unavailable',
        value: 5,
    },
];

export default {
    COUNTRY_LOCALES,
    VEHICLE_STATUSES,
    VEHICLE_CHARACTERISTICS,
    DRIVER_STATUSES,
    WEEK_DAYS,
    TIME_INTERVALS,
    VA_DRIVER_STATUSES,
    VA_VEHICLE_STATUSES,
};

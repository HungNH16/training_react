// import React, { Component } from 'react';
// import { View, TouchableOpacity, Text } from 'react-native';
// import styles from '../CheckBoxes/styles';
// import FAI from 'react-native-vector-icons/FontAwesome5';
// import { Colors, Fonts } from '../../Themes';
// class CheckBoxes extends Component {
//     constructor(props){
//         super(props);
//         this.state = {
//             Check: props.Check
//         };
//     }

//     onChange = (index) => {
//         const newChecked = this.state.Check.map((item, i) => {
//           if (i === index) {
//             return !item
//           }
//           return item
//         })
//         this.setState({
//           Check: newChecked,
//         })
//       }
//     render() {
//         const { Check } = this.state;
//         const { name, text } = this.props;
//         return (
//             <View style={styles.container}>
//                 <Text style={styles.checkboxLabel}>{name}</Text>
//                 <View style={styles.checkboxBorder}>
//                     <Text style={styles.checkboxText}>{text}</Text>
//                     <TouchableOpacity
//                     key={index}
//                     checked={Check[index]}
//                     onPress={() => this.onChange(index)}>
//                         <FAI name="check" color={Colors.main} size={10} />
//                     </TouchableOpacity>
//                 </View>
//             </View>
//         );
//     }
// }

// CheckBoxes.defaultProps = {
//     name: '',
//     Check: false,
//   };
// export default CheckBoxes;
import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import FAI from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
// Styles
import styles from './styles';
// Theme config
import { Colors } from '../../Themes';

class CheckBoxes extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isChecked: props.isChecked,
        };

        this.onCheckboxToggle = this.onCheckboxToggle.bind(this);
    }

    onCheckboxToggle() {
        const { onPress, name } = this.props;
        const { isChecked } = this.state;

        this.setState({ isChecked: !isChecked });
        onPress({ name, isChecked: !isChecked });
    }

    render() {
        const { isChecked } = this.state;
        const { name, text } = this.props;

        return (
            <View style={styles.container}>
                <View><Text style={styles.name}>{name}</Text></View>
                <View>
                    <Text>{text}</Text>
                    <TouchableOpacity onPress={this.onCheckboxToggle} style={styles.container}>
                        <View style={styles.checkboxBorder}>
                            {isChecked ? <FAI name="check" color={Colors.main} size={10} /> : null}
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

CheckBoxes.defaultProps = {
    name: '',
    isChecked: false,
    onPress: () => { },
    label: '',
};

CheckBoxes.propTypes = {
    name: PropTypes.string,
    isChecked: PropTypes.bool,
    onPress: PropTypes.func,
    label: PropTypes.string,
};

export default CheckBoxes;

import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import FAI from 'react-native-vector-icons/FontAwesome5';
// Styles
import styles from './styles';
// Colors
import { Colors } from '../../Themes';

const TourItemFooter = () => (
  <View style={styles.tourItemFooter}>
    <View style={styles.icon}>
      <FAI name="dot-circle" size={20} color={Colors.main} />
    </View>
    <TouchableOpacity style={styles.finishBtn}>
      <Text style={styles.finishBtnText}>FINISH</Text>
    </TouchableOpacity>
  </View>
);

export default TourItemFooter;

import React, { Component } from 'react';
import TaskForm from './components/TaskForm';
import Control from './components/Control';
import TaskList from './components/TaskList';
import './App.css';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';
import * as actions from './actions/index';
class App extends Component {
    onToggleForm = () => {
        var { itemEdit } = this.props;
        if (itemEdit && itemEdit.id !== '') {
            this.props.onOpenForm();
        } else {
            this.props.onToggleForm();
        }
        this.props.onClearTask({
            id: '',
            name: '',
            status: false
        });
    }
    render() {
        var { isDisplayForm } = this.props;
        return (
            <div className="container">
                <div className="text-center">
                    <h1>Quản Lý Công Việc</h1><hr />
                </div>
                <div className="row">
                    <div className={isDisplayForm ? 'col-xs-4 col-sm-4 col-md-4 col-lg-4' : ''}>
                        <TaskForm />
                    </div>
                    <div className={isDisplayForm ? 'col-xs-8 col-sm-8 col-md-8 col-lg-8' : 'col-xs-12 col-sm-12 col-md-12 col-lg-12'}>
                        <Button
                            type="button"
                            color="primary"
                            className="btn btn-primary"
                            onClick={this.onToggleForm}
                        >
                            <span ><i className="fa fa-plus mr-5"></i></span>Thêm Công Việc
                        </Button>
                        {/*Search-sort*/}
                        <Control/>
                        {/* List */}
                        <TaskList />
                    </div>
                </div>
            </div >
        );
    }
}
const mapStateToProps = state => {
    return {
        isDisplayForm: state.isDisplayForm.form,
        itemEdit: state.itemEdit
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onToggleForm: () => {
            dispatch(actions.toggleForm())
        },
        onClearTask: (tasks) => {
            dispatch(actions.editTask(tasks))
        },
        onOpenForm: () => {
            dispatch(actions.openForm())
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);

/*
 * Driver Messages
 *
 * This contains all the text for the Driver container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Driver';

export default defineMessages({
    pageTitle: {
        id: `${scope}.pageTitle`,
        defaultMessage: 'Driver Management',
    },
    tableTitle: {
        id: `${scope}.tableTitle`,
        defaultMessage: 'Driver Management',
    },
    driverId: {
        id: `${scope}.driverId`,
        defaultMessage: 'Driver ID',
    },
    driverName: {
        id: `${scope}.driverName`,
        defaultMessage: 'Driver Name',
    },
    '3plName': {
        id: `${scope}.3plName`,
        defaultMessage: '3PL Name',
    },
    mobileNumber: {
        id: `${scope}.mobileNumber`,
        defaultMessage: 'Mobile Number',
    },
    tags: {
        id: `${scope}.tags`,
        defaultMessage: 'Tag(s)',
    },
    status: {
        id: `${scope}.status`,
        defaultMessage: 'Status',
    },
    action: {
        id: `${scope}.action`,
        defaultMessage: 'Action',
    },
    enterKeywords: {
        id: `${scope}.enterKeywords`,
        defaultMessage: 'Enter Keywords',
    },
});


import React, { Fragment } from "react";
import { Link, Redirect, withRouter } from "react-router-dom";
import { Row, Col } from 'reactstrap';
import AddNewButton from "../../components/TableProperties/AddNewButton";

export class Terms extends React.Component {
    render() {
        return(
            <Fragment>
                <Row className="mt-2 text-md-right text-sm-left">
                    <Col>
                        <div>
                            <AddNewButton
                                title={formatMessage(MSG.TERMS_BUTTON_TITLE)}
                            />
                        </div>
                    </Col>
                </Row>
            </Fragment>
        )
    }
}
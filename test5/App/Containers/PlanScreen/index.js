/* eslint-disable import/no-duplicates */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
import styles from '../../Components/TourItem/styles';
// Components
import TabList from '../../Components/TabList';
import TourItem1 from '../../Components/TourItem1';
import Boxes from '../../Components/Boxes';
// Actions
import TourActions from '../../Modules/DetailTour/reducer';
// Selectors
import { getTour1 } from '../../Modules/DetailTour/selectors';
/* eslint-disable */
class PlanScreen extends Component {
    componentDidMount() {
        // eslint-disable-next-line react/prop-types
        const { fetchTourPlan1 } = this.props;
        fetchTourPlan1('fakeID');
    }
    render() {

        const { tour1 } = this.props;

        return (
            <TabList tabs={{
                back: {
                    label: 'BACK',
                    render: () => (
                        <FlatList
                            style={styles.container}
                            data={tour1}
                            keyExtractor={(item, index) => `${index}`}
                            renderItem={({ item }) => (
                            <TourItem1 {...item} />
                            )}
                        />
                    ),
                },
            }
            } />
        )
    }
}

PlanScreen.propTypes = {
    tour1: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
    tour1: getTour1(state),
});

const mapDispatchToProps = dispatch => ({
    fetchTourPlan1: id => dispatch(TourActions.loadTour1(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(PlanScreen);
const BASE_URL = 'http://3.208.187.143:3080/api';

export default {
    BASE_URL,
    FLEET_DRIVERS: `${BASE_URL}/drivers`,
    FLEET_VEHICLES: `${BASE_URL}/vehicles`,
    FLEET_MAKE_MODELS: `${BASE_URL}/make-models`,
    FLEET_VEHICLE_ASSIGNMENTS_LIST: `${BASE_URL}/vehicle-assignments/3pl`,
    FLEET_VEHICLE_ASSIGNMENTS_SUBMIT: `${BASE_URL}/vehicle-assignments`,
    TERMS_OF_USE: `${BASE_URL}/terms`,
};

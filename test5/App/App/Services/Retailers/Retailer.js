export const fetchRetailer = () => ([
    {
        type: 'Pick-up for: ',
        name: 'Retailer Name 1',
        text: '1015 Edsa Cor Bansalangin Project 7, 1105, Quezon, Manila, The Philippines',
        packages: 8,
    },
    {
        type: 'Drop-off Point 2',
        name: 'Retailer Name 2',
        text: '1046 Sunset Bay Avennue 1, 1105, Quezon, Manila, The Philippines',
        packages: 6,
    },
    {
        type: 'Drop-off Point 3',
        name: 'Retailer Name 3',
        text: '121 Coral Field Street 18, 1105, Quezon, Manila, The Philippines',
        packages: 8,
    },
])
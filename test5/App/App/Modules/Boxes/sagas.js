import { put, takeLatest, call } from 'redux-saga/effects';
// Actions, reducers
import BoxesActions, { BoxesTypes } from './reducer';
// Services
import BoxesApi from '../../Services/Boxes/index';
import Logger from '../../Services/Logger';

function* loadBoxes() {
  try {
    yield put(BoxesActions.setIsFetchingBoxes(true));
    const Boxes = yield call(BoxesApi.fetchBoxes);
    console.log(`123456789`, Boxes)
    yield put(BoxesActions.setBoxes(Boxes));
  } catch (error) {
    Logger.log(error);
  } finally {
    yield put(BoxesActions.setIsFetchingBoxes(false));
  }
}

export default [
  takeLatest(BoxesTypes.LOAD_BOXES, loadBoxes),
];

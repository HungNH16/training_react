/**
 * Asynchronously loads the component for Vehicle Assignment
 */
import loadable from 'loadable-components';

export default loadable(() => import('./index'));

import React, { Component } from 'react';
import Login from "./Login";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
class App extends Component {
  render() {
    return (
      <Route>
        <div>
          <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
          </ul>
          <Route path="/login" exact component={Login} />
        </div>
      </Route>
    );
  }
}

export default App;
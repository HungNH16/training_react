/**
 *
 * Vehicle Assignment Configurations
 *
 * This defines column configurations for Vehicle Assignment table
 */
// ===== IMPORTS =====
// Modules
import React from 'react';
import { get as _get } from 'lodash';
import { IconAction } from 'cf-web-ui-kit-components';
// Custom Components
import SelectInput from '../EditMode/SelectInput';
// Utilities, Constants & Styles
import { VEHICLE_ASSIGNMENT_COLUMN_MAP } from '../../../containers/VehicleAssignment/constants';
import { MSG, LIST } from '../../../constants';
// ===================

// ===== CONFIGURATION =====
export default ({ formatMessage, actions, filterDriverOptions, filterVehicleOptions, markFieldInvalid, changeDriver, changeVehicle, changeStatus }) => [
    {
        headerClassName: 'd-none',
        columns: [
            {
                Header: formatMessage(MSG.VEHICLE_ASSIGNMENT_TABLE_HEADER_DRIVER_NAME),
                accessor: VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME,
                viewCell: ({ value }) => _get(value, 'value', value),
                // eslint-disable-next-line react/prop-types
                editCell: ({ value, index }) => (
                    <SelectInput
                        rowId={index}
                        name={VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME}
                        value={value}
                        getOptions={filterDriverOptions}
                        onChange={changeDriver}
                        isInvalid={markFieldInvalid(`data_${index}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME}`)}
                    />
                ),
                width: 350,
                sortable: false,
                customConfig: {
                    edit: {
                        className: 'cf-table__cell--visible',
                        resizable: false,
                    },
                },
            },
        ],
    },
    {
        headerClassName: 'd-none',
        columns: [
            {
                Header: formatMessage(MSG.VEHICLE_ASSIGNMENT_TABLE_HEADER_DRIVER_STATUS),
                accessor: VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS,
                viewCell: ({ value }) => _get(LIST.VA_DRIVER_STATUSES.find(opt => opt.value === value), 'label', ''),
                // eslint-disable-next-line react/prop-types
                editCell: ({ value, index }) => (
                    <SelectInput
                        rowId={index}
                        name={VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS}
                        value={value}
                        options={LIST.VA_DRIVER_STATUSES.slice(3)}
                        onChange={changeStatus}
                        isInvalid={markFieldInvalid(`data_${index}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS}`)}
                    />
                ),
                sortable: false,
                customConfig: {
                    edit: {
                        className: 'cf-table__cell--visible',
                        resizable: false,
                    },
                },
            },
        ],
    },
    {
        headerClassName: 'd-none',
        columns: [
            {
                Header: formatMessage(MSG.VEHICLE_ASSIGNMENT_TABLE_HEADER_VEHICLE),
                accessor: VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE,
                viewCell: ({ original }) => `${_get(original, 'licensePlate', '')}${original.makeModel ? ` - ${_get(original, 'makeModel.make', '')} ${_get(original, 'makeModel.model', '')}` : ''}`,
                // eslint-disable-next-line react/prop-types
                editCell: ({ value, index }) => (
                    <SelectInput
                        rowId={index}
                        name={VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE}
                        value={value}
                        getOptions={filterVehicleOptions}
                        onChange={changeVehicle}
                        isInvalid={markFieldInvalid(`data_${index}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE}`)}
                    />
                ),
                width: 350,
                sortable: false,
                customConfig: {
                    edit: {
                        className: 'cf-table__cell--visible',
                        resizable: false,
                    },
                },
            },
        ],
    },
    {
        headerClassName: 'd-none',
        columns: [
            {
                Header: formatMessage(MSG.VEHICLE_ASSIGNMENT_TABLE_HEADER_VEHICLE_STATUS),
                accessor: VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS,
                viewCell: ({ value }) => _get(LIST.VA_VEHICLE_STATUSES.find(opt => opt.value === value), 'label'),
                // eslint-disable-next-line react/prop-types
                editCell: ({ value, index }) => (
                    <SelectInput
                        rowId={index}
                        name={VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS}
                        value={value}
                        options={LIST.VA_VEHICLE_STATUSES.slice(3)}
                        onChange={changeStatus}
                        isInvalid={markFieldInvalid(`data_${index}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS}`)}
                    />
                ),
                sortable: false,
                customConfig: {
                    edit: {
                        className: 'cf-table__cell--visible',
                    },
                },
            },
        ],
    },
    {
        headerClassName: 'd-none',
        columns: [
            {
                Header: formatMessage(MSG.COMMON_TABLE_COLUMN_HEADER_ACTION),
                accessor: VEHICLE_ASSIGNMENT_COLUMN_MAP.ACTION,
                editCell: tableProps => (
                    <div className="d-flex">
                        {actions.map((item, index) => (
                            <div className="mr-2">
                                <IconAction {...item} key={`action_${index + 1}`} action={() => item.action(tableProps)} />
                            </div>
                        ))}
                    </div>
                ),
                width: 70,
                sortable: false,
                customConfig: {
                    edit: {
                        resizable: false,
                    },
                },
            },
        ],
    },
];
// =========================

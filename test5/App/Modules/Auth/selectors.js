export const getAuthState = state => state.auth;
export const getError = state => getAuthState(state).error;

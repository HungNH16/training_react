import { StyleSheet } from 'react-native';
import { Metrics } from '../../Themes';

export default StyleSheet.create({
  applicationView: {
    width: Metrics.screenWidth,
    height: Metrics.screenHeight,
    position: 'relative',
  },
  loadingIndicator: {
    opacity: 0.7,
    top: 0,
    // flex: 1,
    position: 'absolute',
    width: Metrics.screenWidth,
    height: Metrics.screenHeight,
    zIndex: 99999, // Super large zindex to make it stay on top
  },
});

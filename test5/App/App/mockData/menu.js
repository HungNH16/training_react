import { ROUTE } from '../constants';

export default {
    '3pl_full': [
        {
            exact: true,
            url: ROUTE.HOMEPAGE,
            label: {
                iconClass: 'far fa-home-alt',
                title: 'Home',
            },
        },
        {
            exact: true,
            url: ROUTE.TOURS,
            label: {
                iconClass: 'far fa-route',
                title: 'Tour Generation',
            },
        },
        {
            url: ROUTE.FLEET_DRIVERS,
            label: {
                iconClass: 'far fa-steering-wheel',
                title: 'Fleet Management',
            },
            subMenu: [
                {
                    exact: true,
                    url: ROUTE.FLEET_DRIVERS,
                    label: 'Drivers',
                },
                {
                    exact: true,
                    url: ROUTE.FLEET_VEHICLES,
                    label: 'Vehicles',
                },
                {
                    exact: true,
                    url: ROUTE.FLEET_VEHICLE_ASSIGNMENT,
                    label: 'Vehicle Assignment',
                },
            ],
        },
        {
            exact: true,
            url: ROUTE.REPORTS,
            label: {
                iconClass: 'far fa-chart-line',
                title: 'Reports',
            },
        },
        {
            exact: true,
            url: ROUTE.SETTINGS,
            label: {
                iconClass: 'far fa-cog',
                title: 'Settings',
            },
        },
        {
            exact: true,
            url: ROUTE.ABOUT,
            label: {
                iconClass: 'far fa-info',
                title: 'About',
            },
        },
    ],
    '3pl_lite': [
        {
            exact: true,
            url: ROUTE.HOMEPAGE,
            label: {
                iconClass: 'far fa-home-alt',
                title: 'Home',
            },
        },
        {
            url: ROUTE.FLEET_DRIVERS,
            label: {
                iconClass: 'far fa-steering-wheel',
                title: 'Fleet Management',
            },
            subMenu: [
                {
                    exact: true,
                    url: ROUTE.FLEET_DRIVERS,
                    label: 'Drivers',
                },
                {
                    exact: true,
                    url: ROUTE.FLEET_VEHICLES,
                    label: 'Vehicles',
                },
                {
                    exact: true,
                    url: ROUTE.FLEET_VEHICLE_ASSIGNMENT,
                    label: 'Vehicle Assignment',
                },
            ],
        },
    ],
};

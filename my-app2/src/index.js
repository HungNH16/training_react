import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Product from './Products';

ReactDOM.render(
<Product />,
 document.getElementById('root'));

import { isNumber as _isNumber, uniq as _uniq, flatten as _flatten, get as _get } from 'lodash';

export const compareUrl = (mainUrl, subUrl) => {
    const mainDomains = mainUrl.split('/');
    const subDomains = subUrl.split('/');

    return mainUrl === subUrl || mainDomains[1] === subDomains[1];
};

export const formatPhone = (dialCode, phone) => (dialCode && phone ? `+(${dialCode}) ${phone}` : '');

export const roundDecimals = floatNumber => (_isNumber(Number(floatNumber) || '') ? parseFloat(floatNumber).toFixed(2) : floatNumber);

export const alphabeticalSort = (array, keys) =>
    array.sort((a, b) => {
        if (a[keys.join('.')].toUpperCase() > b[keys.join('.')].toUpperCase()) return 1;
        if (a[keys.join('.')].toUpperCase() < b[keys.join('.')].toUpperCase()) return -1;
        return 0;
    });

export const convertResponseAlerts = error => {
    const errorList = _get(error, 'response.data.alerts', []);
    const errors = _uniq(errorList.map(err => err.message));
    const invalidFields = _flatten(errorList.map(err => err.meta.field.replace(/\./g, '_')));

    return {
        errors,
        invalidFields,
    };
};

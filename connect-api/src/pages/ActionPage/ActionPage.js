import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {add_products_request, edit_products_request, update_products_request} from '../../actions/index';
import { connect } from 'react-redux';
class ActionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            txtName: '',
            txtPrice: '',
            chkbStatus: ''
        }
    }

    componentDidMount() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            this.props.onItemEdit(id);
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.ItemEdit){
           var {ItemEdit} = nextProps;
           this.setState({
                id: ItemEdit.id,
                txtName: ItemEdit.name,
                txtPrice: ItemEdit.price,
                chkbStatus: ItemEdit.status

           });
        }
    }

    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.type === "checkbox" ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }
    
    onSave = (event) => {
        event.preventDefault();
        var { id, txtName, txtPrice, chkbStatus } = this.state;
        var { history } = this.props;
        var product = {
            id : id,
            name : txtName,
            price : txtPrice,
            status : chkbStatus
        }
        if (id) {
          this.props.onUpdateProduct(product);
        } else {
            this.props.onAddProduct(product);
        }
        history.goBack();
    }
    render() {
        var { txtName, txtPrice, chkbStatus } = this.state;
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form onSubmit={this.onSave}>
                    <div className="form-group">
                        <label >Tên Sản Phẩm: </label>
                        <input
                            type="text"
                            className="form-control"
                            name="txtName"
                            value={txtName}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="form-group">
                        <label>Gía: </label>
                        <input
                            type="text"
                            className="form-control"
                            name="txtPrice"
                            value={txtPrice}
                            onChange={this.onChange}
                        />
                    </div>
                    <div className="form-group">
                        <label>Trạng Thái: </label>
                    </div>
                    <div className="checkbox">
                        <label>
                            <input
                                type="checkbox"
                                name="chkbStatus"
                                value={chkbStatus}
                                onChange={this.onChange}
                                checked={chkbStatus} />
                            Còn Hàng
                            </label>
                    </div>
                    <Link to="/product-list" className="btn btn-danger" id="md-5">Trở Lại</Link>
                    <button type="submit" className="btn btn-primary">Lưu Lại</button>
                </form>

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ItemEdit: state.ItemEdit
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onAddProduct: (product) => {
            dispatch(add_products_request(product));
        },
        onItemEdit: (id) => {
            dispatch(edit_products_request(id));
        },
        onUpdateProduct: (product) => {
            dispatch(update_products_request(product));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionPage);

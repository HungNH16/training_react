import { StyleSheet } from 'react-native';
// Colors
import { Colors, Fonts } from '../../Themes';

const height = 135;

const textRowStyle = {
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 10,
  },
  text: {
    fontSize: Fonts.size.small,
    lineHeight: Fonts.size.small + 2,
    paddingLeft: 5,
  },
};

export default StyleSheet.create({
  tourItem1:{
    paddingLeft:15,
    paddingRight:15,
    paddingTop:15,
  },
  tourItem: {
    width: '100%',
    height,
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  // Timeline on the left
  timeLineIcon: {
    width: 20,
    height: 20,
  },
  timeLineLine: {
    width: 5,
    height: height - 20, // timeLineContainer.height - timeLineIcon.height,
    backgroundColor: Colors.blue,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  detail: {
    paddingBottom: 5,
    flex: 1,
    height,
    backgroundColor: Colors.midNight,
  },
  detail2: {
    paddingRight:15,
    paddingLeft:15,
  },
  // Detail card on the right
  detailContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: Colors.shuttleGrey,
  },
  leftDetailContainer: {
    flex: 1,
    paddingTop: 20,
  },
  detail1: {
    flex: 1,
    paddingTop:50,
  },
  detailRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  Arrived: {
    paddingTop:70,
  },
  detailTypeLabel: {
    height: 17,
    backgroundColor: Colors.blue,
    paddingHorizontal: 10,
    borderRadius: 8,
    marginRight: 5,
  },
  detailTypeText: {
    fontSize: Fonts.size.tiny,
    color: Colors.white,
    textAlign: 'center',
    lineHeight: 17,
    fontWeight: 'bold',
  },
  detailTime: {
    fontSize: Fonts.size.tiny,
    lineHeight: 17,
    marginLeft: 5,
    color: Colors.blue,
    fontWeight: 'bold',
  },
  detailCompanyName: {
    fontWeight: 'bold',
    marginVertical: 5,
    color: Colors.white,
    paddingTop:4,
  },
  detailLocationRow: {
    ...textRowStyle.row,
  },
  detailLocationText: {
    ...textRowStyle.text,
    color: Colors.white,
    width: '100%',
    maxHeight: 40, // Maximum line is 3(Fonts.size.small * 3)
    overflow: 'hidden',
    paddingRight:30,
  },
  detailIDRow: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    flex: 1,
  },
  ArrivedBtn: {
    backgroundColor: Colors.main,
    height:30,
  },
  ArrivedBtnText: {
    textAlign:'center',
    color: Colors.white,
    fontWeight: 'bold',
    paddingTop:5,
  },
  Report: {
    paddingBottom: 10,
  },
  ReportBtn:{
    backgroundColor: Colors.shuttleGrey,
    position: 'relative',
    height:30,
    paddingTop:5
  },
  ReportBtnText: {
    color: Colors.white,
    textAlign: 'center',
    lineHeight: 20,
    fontWeight: 'bold',
  },
  Mark: {
    paddingTop: 0,
  },
  MarkBtn:{
    backgroundColor: Colors.shuttleGrey,
    position: 'relative',
    height:30,
    paddingTop:5,
  },
  MarkBtnText: {
    color: Colors.white,
    textAlign: 'center',
    lineHeight: 20,
    fontWeight: 'bold',
  },
  __dropOffTimeLineLine: {
    backgroundColor: Colors.darkBlue,
  },
  __dropOffDetailTypeLabel: {
    backgroundColor: Colors.darkBlue,
  },
  __dropOffDetailContainer: {
    backgroundColor: Colors.lightBlue,
  },
  __dropOffDetailTime: {
    color: Colors.darkBlue,
  },
  __dropOffNavigateBtn: {
    backgroundColor: Colors.darkBlue,
  },
});

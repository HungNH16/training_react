import { StyleSheet } from 'react-native';
// Shared style
import { ApplicationStyles, Fonts, Colors } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  listHeader: {
    color: Colors.black,
    fontSize: Fonts.size.medium,
    fontWeight: 'bold',
    paddingVertical: 15,
  },
});

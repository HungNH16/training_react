import { StyleSheet } from 'react-native';
// Colors
import { Colors } from '../../Themes';

export default StyleSheet.create({
  phoneInput: {
    position: 'relative',
    width: '100%',
  },
  countryCode: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    width: 100,
    height: 40,
    paddingHorizontal: 10,
    left: 0,
    zIndex: 10,
  },
  contryCodeText: {
    color: Colors.black,
  },
  input: {
    position: 'absolute',
    left: 0,
    paddingLeft: 100,
  },
});

import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { injectIntl } from 'react-intl';
import { isEmpty as _isEmpty } from 'lodash';
import { MSG } from '../../../../constants';

const SelectInput = ({ name, value, multiple, options, getOptions, onChange, isInvalid, rowId, className, intl: { formatMessage } }) => {
    const onChangeSelect = newValue => onChange(rowId, name, newValue);
    const filteredOptions = getOptions ? getOptions(rowId) : options;
    return (
        <Select
            name={name}
            value={value}
            isMulti={multiple}
            options={filteredOptions}
            isLoading={_isEmpty(filteredOptions)}
            placeholder={formatMessage(MSG.COMMON_INPUT_SELECT_OPTION_DEFAULT)}
            onChange={onChangeSelect}
            className={`${className || ''}${isInvalid ? ' cf-select--is-invalid' : ''}`}
            classNamePrefix="cf-select"
        />
    );
};

SelectInput.propTypes = {
    /** Component's class */
    className: PropTypes.string,
    /** Injected React Intl object */
    intl: PropTypes.object,
    /** Condition flag to determine whether this field's value is invalid */
    isInvalid: PropTypes.bool,
    /** Input name */
    name: PropTypes.string,
    /** Condition flag to allow select mupltiple values */
    multiple: PropTypes.bool,
    /** Action to handle input change */
    onChange: PropTypes.func,
    /** Options to select */
    options: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        }),
    ),
    /** Custom method to generate select options */
    getOptions: PropTypes.func,
    /** Current row index of this input */
    rowId: PropTypes.number,
    /** Input value */
    value: PropTypes.oneOfType([
        PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string,
                value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
            }),
        ),
        PropTypes.shape({
            label: PropTypes.string,
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        }),
        PropTypes.string,
    ]),
};

export default injectIntl(SelectInput);

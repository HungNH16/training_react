import React from 'react';
import { injectIntl } from 'react-intl';
import { Container, Row, Col, CardImg, Label } from 'reactstrap';
import PropTypes from 'prop-types';
import { Popup } from 'cf-web-ui-kit-components';
import { MSG } from '../../../constants';
import successIcon from '../../../assets/images/icons/Success.png';
import '../../../scss/components/popups/success.scss';

const SuccessContent = ({ message }) => (
    <Container>
        <Row>
            <Col>
                <CardImg src={successIcon} />
            </Col>
        </Row>
        <Row>
            <Col>
                <Label>{message.successLabel}</Label>
            </Col>
        </Row>
        <Row>
            <Col>
                <span>{message.successMsg}</span>
            </Col>
        </Row>
    </Container>
);

const SuccessPopup = ({ isOpen, intl, handleExecution, successMsg }) => (
    <Popup
        isOpen={isOpen}
        popupClass="success-popup"
        button={{
            primary: {
                title: 'OK',
                className: 'success-btn',
            },
        }}
        component={SuccessContent}
        getContentProps={{
            message: {
                successLabel: intl.formatMessage({ ...MSG.COMMON_POPUP_MESSAGE_SUCCESS }),
                successMsg,
            },
        }}
        handleExecution={handleExecution}
    />
)

SuccessContent.propTypes = {
    message: PropTypes.objectOf(PropTypes.oneOf(PropTypes.string, PropTypes.element)),
}

SuccessPopup.propTypes = {
    intl: PropTypes.object,
    isOpen: PropTypes.bool,
    successMsg: PropTypes.oneOf(PropTypes.string, PropTypes.element),
    handleExecution: PropTypes.func,
}

export default injectIntl(SuccessPopup);

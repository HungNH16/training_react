import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setTour: ['payload'],
  setIsFetchingTour: ['payload'],
  fetchTour: ['payload'],
});

export const TourTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

const INITIAL_STATE = Immutable({
  tour: {},
  isFetchingTour: true,
});

/* ------------- Reducers ------------- */

const setTour = (state, { payload: tour }) => state.merge({ tour });
const setFetchingTourStatus = (state, { payload: isFetchingTour }) => state.merge({ isFetchingTour });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_TOUR]: setTour,
  [Types.SET_IS_FETCHING_TOUR]: setFetchingTourStatus,
});

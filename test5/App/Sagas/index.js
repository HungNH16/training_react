import { takeLatest, all } from 'redux-saga/effects';
import API from '../Services/Api';

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux';

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas';
import appSagas from '../Modules/App/sagas';
import authSagas from '../Modules/Auth/sagas';
import tourSagas from '../Modules/Tour/sagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
export const api = API.create();

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    ...appSagas,
    ...authSagas,
    ...tourSagas,
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),
  ]);
}

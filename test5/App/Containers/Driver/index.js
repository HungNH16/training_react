/**
 *
 * Drivers Management
 *
 * This screen manages available drivers of all 3PL accounts,
 * including their statistics & current working status.
 */
// ===== IMPORTS =====
// Modules
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Input, Row, Col, Container } from 'reactstrap';
import { injectIntl } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { isEmpty as _isEmpty, uniq as _uniq, get as _get } from 'lodash';
import { Table, IconAction, Pagination, Button, PhoneNumberField } from 'cf-web-ui-kit-components';
// Custom Components
import { injectSaga, injectReducer, validations, formatPhone } from '../../utils';
import { selectFormValues, selectErrors, selectInvalidFields, selectSuccessFlag, selectDrivers } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { loadDriverAction, addNewDriver, updateSuccessFlag, resetForm, updateInput, updateErrors, resetState } from './actions';
import AddNewButton from '../../components/TableProperties/AddNewButton';
// Constants & Styles
import { MSG, FORM, LIST, ROUTE, ENUM } from '../../constants';
import '../../scss/containers/common.scss';
// ===================

// ===== MAIN COMPONENT =====
export class DriversManagement extends Component {
    constructor(props) {
        super(props);

        this.handleSearch = this.handleSearch.bind(this);
        this.handlePaging = this.handlePaging.bind(this);
        this.updateFieldMap = this.updateFieldMap.bind(this);
        this.validateAddForm = this.validateAddForm.bind(this);
        this.getAddRequest = this.getAddRequest.bind(this);
        this.handleSubmitAdd = this.handleSubmitAdd.bind(this);
    }

    componentDidMount() {
        // Create add-new form configuration
        this.updateFieldMap();

        const {
            driver: {
                pagination: { currentPage },
            },
        } = this.props;
        this.props.searchAction({ q: '', page: currentPage });
    }

    componentWillUnmount() {
        this.props.onResetState();
    }

    handleSearch = () => {
        const {
            driver: {
                pagination: { currentPage },
            },
        } = this.props;
        this.props.searchAction({ page: currentPage });
    };

    handlePaging = pageNumber => {
        const {
            history: { push },
        } = this.props;
        push(`${ROUTE.FLEET_DRIVERS}?page=${pageNumber}`);
        this.props.searchAction({ page: pageNumber });
    };

    updateFieldMap() {
        const { intl, onUpdateDialCode, onUpdatePhone } = this.props;
        this.updatedFieldMap = FORM.DRIVER_FORM.map(field => {
            const newField = { ...field };

            if (newField.type === 'tel') {
                newField.customInput = PhoneNumberField;
                newField.getInputProps = {
                    onSelectFlag: onUpdateDialCode,
                    onPhoneNumberChange: onUpdatePhone,
                    separateDialCode: true,
                    countries: LIST.COUNTRY_LOCALES,
                    defaultCountry: 'sg',
                    placeholder: intl.formatMessage({
                        ...MSG.COMMON_INPUT_TELEPHONE_PLACEHOLDER,
                    }),
                    style: { width: '100%' },
                };
            } else if (newField.name === 'status') {
                newField.options = newField.options.filter(opt => ['4', '5'].includes(opt.value));
            }
            return newField;
        });
    }

    validateAddForm() {
        const { isRequired, isMinLength, isMaxlength, isTagValid, isPhoneValid } = validations;
        const {
            formValues: { name, dialCode, phone, status, tags },
        } = this.props;
        return {
            ...isRequired([{ name: 'name', value: name }, { name: 'dialCode', value: dialCode }, { name: 'phone', value: phone }, { name: 'status', value: status }]),
            ...isMaxlength({ name: 'name', label: 'Driver Name', value: name, max: 255 }),
            ...isMinLength({ name: 'phone', label: 'Mobile No.', value: phone, min: 7 }),
            ...isMaxlength({ name: 'phone', label: 'Mobile No.', value: phone, max: 20 }),
            ...isPhoneValid({ name: 'phone', value: formatPhone(dialCode, phone) }),
            ...isTagValid({ name: 'tags', value: tags }),
        };
    }

    getAddRequest() {
        const {
            formValues: { name, dialCode, phone, status, tags, workingHours },
        } = this.props;
        return {
            driverName: name,
            contactNumber: {
                countryCode: `+${dialCode}`,
                number: phone,
            },
            status: status.value,
            tags: tags.split(/,[ ]*/),
            workingHours,
        };
    }

    handleSubmitAdd() {
        const { onUpdateErrors, onAddNewDriver } = this.props;
        const errors = this.validateAddForm();

        if (!_isEmpty(errors)) {
            onUpdateErrors(_uniq(Object.values(errors)), Object.keys(errors));
        } else {
            onUpdateErrors([], []);
            onAddNewDriver(this.getAddRequest());
        }
    }

    render() {
        const {
            driver: { drivers, pagination },
            intl: { formatMessage },
            formValues,
            errors,
            invalidFields,
            addSuccessFlag,
            onUpdateInput,
            onResetForm,
            onUpdateSuccessFlag,
        } = this.props;
        const columns = [
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.driverId),
                        accessor: 'driverID',
                        width: 130,
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.driverName),
                        accessor: 'driverName',
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.mobileNumber),
                        accessor: 'mobileNumber',
                        Cell: original => [original.row.mobileNumber.countryCode, ' ', original.row.mobileNumber.number].join(''),
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.status),
                        accessor: 'status',
                        Cell: props => (LIST.DRIVER_STATUSES.find(opt => parseInt(opt.value, 10) === parseInt(props.value, 10)) || {}).label,
                        width: 120,
                        sortable: true,
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: 'Working Hours',
                        accessor: 'workingHours',

                        Cell: ({ original }) => {
                            const result = [];
                            const workingHours = _get(original, 'workingHours', []);

                            if (workingHours.length) {
                                workingHours.map((dayRange, index) => {
                                    result.push(`${ENUM.WEEKDAY_MAP[dayRange.fromDay].SHORT} - ${ENUM.WEEKDAY_MAP[dayRange.toDay].SHORT}`);
                                    result.push(index < workingHours.length - 1 ? `(${dayRange.fromTime} - ${dayRange.toTime}),` : `(${dayRange.fromTime} - ${dayRange.toTime})`);
                                    return dayRange;
                                });
                                return result.map(item => (
                                    <Fragment>
                                        {item}
                                        <br />
                                    </Fragment>
                                ));
                            }
                            return '';
                        },
                        width: 140,
                        sortable: true,
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: 'Day Off',
                        accessor: 'dayOff',

                        Cell: ({ original }) => {
                            let result = '';
                            const dayOffs = _get(original, 'dayOff', []);

                            if (dayOffs.length) {
                                result = dayOffs.map(dayNo => ENUM.WEEKDAY_MAP[dayNo].SHORT).join(', ');
                            }

                            return result;
                        },
                        width: 120,
                        sortable: true,
                        style: { whiteSpace: 'unset' },
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.tags),
                        accessor: 'tags',
                        Cell: original => {
                            const tags = [];
                            original.row.tags.map(item => (item ? tags.push(`#${item}`.replace('##', '#')) : ''));
                            return tags.join(', ');
                        },
                    },
                ],
            },
            {
                headerClassName: 'd-none',
                columns: [
                    {
                        Header: formatMessage(messages.action),
                        Cell: props => (
                            <div className="d-flex">
                                {icons.map(item => (
                                    <div className="mr-2">
                                        <IconAction
                                            icon={item.icon}
                                            action={() => {
                                                const {
                                                    original: { driverID },
                                                } = props;
                                                item.action(driverID);
                                            }}
                                            key={item.name + props.original.driverID}
                                        />
                                    </div>
                                ))}
                            </div>
                        ),
                        accessor: 'action',
                        className: 'd-none d-md-block',
                        headerClassName: 'd-none d-md-block',
                        sortable: false,
                        width: 150,
                    },
                ],
            },
        ];
        const deleteItem = id => id;
        const editItem = id => id;

        const icons = [
            {
                name: 'edit',
                icon: <i className="fas fa-pencil-alt" />,
                action: editItem,
            },
            {
                name: 'delete',
                icon: <i className="fas fa-times" />,
                action: deleteItem,
            },
        ];

        const initialState = {
            data: drivers,
            currentPage: pagination.currentPage,
        };

        return (
            <Fragment>
                <Helmet>
                    <title>{formatMessage(MSG.DRIVER_PAGE_TITLE)}</title>
                </Helmet>
                <Row className="row d-flex justify-content-center align-items-center">
                    <Col>
                        <Container>
                            <Row>
                                <Col>
                                    <h3>
                                        <b> {formatMessage(MSG.DRIVER_TABLE_HEADER)} </b>
                                    </h3>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Input type="text" className="form-control" name="searchbox" placeholder={formatMessage(MSG.COMMON_INPUT_SEARCH_PLACEHOLDER)} />
                                </Col>
                                <Col>
                                    <Button onClick={() => this.handleSearch()}>{formatMessage(MSG.COMMON_BUTTON_SEARCH_BAR)}</Button>
                                </Col>
                            </Row>
                            <Row className="mt-2 text-md-right text-sm-left">
                                <Col>
                                    <div className="d-inline-block">
                                        <AddNewButton
                                            title={formatMessage(MSG.DRIVER_BUTTON_TITLE_ADD_NEW)}
                                            fieldMap={this.updatedFieldMap}
                                            formValues={formValues}
                                            errors={errors}
                                            invalidFields={invalidFields}
                                            handleChangeInput={onUpdateInput}
                                            handleResetForm={onResetForm}
                                            handleSubmit={this.handleSubmitAdd}
                                            addSuccessFlag={addSuccessFlag}
                                            handleCloseSuccessPopup={() => {
                                                onUpdateSuccessFlag(false);
                                                this.handleSearch();
                                            }}
                                            successMsg={formatMessage(MSG.DRIVER_POPUP_MESSAGE_ADD_SUCCESS)}
                                        />
                                    </div>
                                    <div className="ml-3 d-inline-block d-md-none">
                                        <div className="table-toolbar-button">
                                            <div className="icon">
                                                <i className="fas fa-times" />
                                            </div>
                                            {/* <span className="ml-2">{formatMessage(MSG.COMMON_BUTTON_DELETE_ABOVE_TABLE)}</span> */}
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col>
                                    <Table
                                        columns={columns}
                                        data={initialState.data}
                                        onPaging={() => {}}
                                        showPagination
                                        pageSizeOptions={[5, 10, 20, 25, 50, 100]}
                                        defaultPageSize={pagination.perPage}
                                        noDataText={formatMessage(MSG.NO_DATA_TEXT)}
                                        PaginationComponent={() => (
                                            <Pagination itemsCountPerPage={pagination.perPage} totalItemCount={pagination.total} currentPage={pagination.currentPage} goToPage={this.handlePaging} />
                                        )}
                                    />
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                </Row>
            </Fragment>
        );
    }
}
// ==========================

// ===== PROP TYPES =====
DriversManagement.propTypes = {
    searchAction: PropTypes.func.isRequired,
    driver: PropTypes.object,
    history: PropTypes.object,
    intl: PropTypes.object,
    formValues: PropTypes.object,
    errors: PropTypes.arrayOf(PropTypes.string),
    invalidFields: PropTypes.arrayOf(PropTypes.string),
    addSuccessFlag: PropTypes.bool,
    onUpdateDialCode: PropTypes.func,
    onUpdatePhone: PropTypes.func,
    onUpdateErrors: PropTypes.func,
    onAddNewDriver: PropTypes.func,
    onUpdateInput: PropTypes.func,
    onResetForm: PropTypes.func,
    onUpdateSuccessFlag: PropTypes.func,
    onResetState: PropTypes.func,
};
// ======================

// ===== INJECTIONS =====
const mapStateToProps = createStructuredSelector({
    driver: selectDrivers(),
    formValues: selectFormValues,
    errors: selectErrors,
    invalidFields: selectInvalidFields,
    addSuccessFlag: selectSuccessFlag,
});

export const mapDispatchToProps = dispatch => ({
    searchAction: params => dispatch(loadDriverAction(params)),
    onAddNewDriver: params => dispatch(addNewDriver(params)),
    onUpdateSuccessFlag: bool => dispatch(updateSuccessFlag(bool)),
    onResetForm: () => dispatch(resetForm()),
    onUpdateErrors: (errors, invalidFields) => dispatch(updateErrors(errors, invalidFields)),
    onUpdateInput: (name, value) => dispatch(updateInput(name, value)),
    onUpdateDialCode: (bool, value) => dispatch(updateInput('dialCode', value.dialCode)),
    onUpdatePhone: (bool, value) => dispatch(updateInput('phone', value)),
    onResetState: () => dispatch(resetState()),
});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'driver', reducer });
const withSaga = injectSaga({ key: 'driver', saga });

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(injectIntl(withRouter(DriversManagement)));

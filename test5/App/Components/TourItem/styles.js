import { StyleSheet } from 'react-native';
// Colors
import { Colors, Fonts } from '../../Themes';

const height = 135;

const textRowStyle = {
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 10,
  },
  text: {
    fontSize: Fonts.size.small,
    lineHeight: Fonts.size.small + 2,
    paddingLeft: 5,
  },
};

export default StyleSheet.create({
  tourItem: {
    width: '100%',
    height,
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  // Timeline on the left
  timeLineTree: {
    height,
    width: 20,
    marginRight: 10,
  },
  timeLineTreeContainer: {
    position: 'absolute',
    top: 20,
  },
  timeLineIcon: {
    width: 20,
    height: 20,
  },
  timeLineLine: {
    width: 5,
    height: height - 20, // timeLineContainer.height - timeLineIcon.height,
    backgroundColor: Colors.blue,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  detail: {
    paddingBottom: 15,
    flex: 1,
    height,
  },
  // Detail card on the right
  detailContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: Colors.lavender,
  },
  leftDetailContainer: {
    flex: 1,
  },
  rightDetailContainer: {
    width: 100,
  },
  detailRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  detailTypeLabel: {
    height: 17,
    backgroundColor: Colors.blue,
    paddingHorizontal: 10,
    borderRadius: 8,
    marginRight: 5,
  },
  detailTypeText: {
    fontSize: Fonts.size.tiny,
    color: Colors.white,
    textAlign: 'center',
    lineHeight: 17,
    fontWeight: 'bold',
  },
  detailTime: {
    fontSize: Fonts.size.tiny,
    lineHeight: 17,
    marginLeft: 5,
    color: Colors.blue,
    fontWeight: 'bold',
  },
  detailCompanyName: {
    fontWeight: 'bold',
    marginVertical: 5,
    color: Colors.black,
  },
  detailLocationRow: {
    ...textRowStyle.row,
  },
  detailLocationText: {
    ...textRowStyle.text,
    color: Colors.black,
    width: '100%',
    maxHeight: 40, // Maximum line is 3(Fonts.size.small * 3)
    overflow: 'hidden',
  },
  detailIDRow: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    flex: 1,
  },
  // detailID: {
  //   ...textRowStyle.row,
  //   width: null,
  //   flex: 1,
  // },
  // detailIDText: {
  //   ...textRowStyle.text,
  //   marginRight: 5,
  // },
  detailReportIssueBtn: {
    backgroundColor: Colors.main,
    paddingVertical: 5,
    paddingHorizontal: 7,
    borderRadius: 14,
  },
  detailReportIssueBtnText: {
    fontSize: Fonts.size.tiny,
    lineHeight: Fonts.size.small,
    textAlign: 'center',
    color: Colors.white,
    fontWeight: '200',
  },
  navigateBtn: {
    paddingTop: 7,
    paddingBottom: 5,
    paddingLeft: 5,
    paddingRight: 7,
    backgroundColor: Colors.blue,
    borderRadius: 13,
  },
  __dropOffTimeLineLine: {
    backgroundColor: Colors.darkBlue,
  },
  __dropOffDetailTypeLabel: {
    backgroundColor: Colors.darkBlue,
  },
  __dropOffDetailContainer: {
    backgroundColor: Colors.lightBlue,
  },
  __dropOffDetailTime: {
    color: Colors.darkBlue,
  },
  __dropOffNavigateBtn: {
    backgroundColor: Colors.darkBlue,
  },
  __passedTimeLineLine: {
    backgroundColor: Colors.shuttleGrey,
  },
  __passedDetailTypeLabel: {
    backgroundColor: Colors.shuttleGrey,
  },
  __passedDetailContainer: {
    backgroundColor: Colors.lightShuttleGrey,
  },
  __passedDetailTime: {
    color: Colors.shuttleGrey,
  },
  __passedNavigateBtn: {
    backgroundColor: Colors.shuttleGrey,
  },
  __passedDetailReportIssueBtn: {
    backgroundColor: Colors.shuttleGrey,
  },
});

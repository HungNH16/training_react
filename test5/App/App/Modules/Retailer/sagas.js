import { put, takeLatest, call } from 'redux-saga/effects';
// Actions, reducers
import RetailerActions, { RetailerTypes } from './reducer';
// Services
import RetailerApi from '../../Services/DetailTour/index';
import Logger from '../../Services/Logger';

function* loadRetailer() {
  try {
    yield put(RetailerActions.setIsFetchingRetailer(true));
    const Retailer = yield call(RetailerApi.fetchRetailer);

    yield put(RetailerActions.setRetailer(Retailer));
  } catch (error) {
    Logger.log(error);
  } finally {
    yield put(RetailerActions.setIsFetchingRetailer(false));
  }
}

export default [
  takeLatest(RetailerTypes.LOAD_RETAILER, loadRetailer),
];

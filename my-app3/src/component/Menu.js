import React, { Component } from 'react';
import Item from './Item';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { logout } from '../action/Action'
import Search from './Search';
class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.state.filterText = "";
    }
    handleUserInput(filterText) {
        this.setState({ filterText: filterText });
    };
    render() {
        var { isLogin } = this.props
        return (
            <div className='container'>
                {isLogin ?
                    <div>
                        <Link to='/Cart'>
                            <button>Cart</button>
                        </Link>
                        <button
                            onClick={this.props.handleLogout}>Logout</button>
                        <span style={{ float: 'right' }}>Hello, {this.props.user}</span>
                    </div>
                    :
                    <Link to='/Login'>
                        <button>Login</button>
                    </Link>
                }
                <h2 style={{ textAlign: "center" }}>SHOP</h2>
                <Search
                    filterText={this.state.filterText}
                    onUserInput={this.handleUserInput.bind(this)} />
                <table className='table' style={{ margin: 50 }}>
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Category</td>
                            <td>Price</td>
                            <td>Name</td>
                            <td>Add</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.leftList.map(item => {
                            return <Item key={item.id} item={item} isLogin={this.props.isLogin}
                            filterText={this.state.filterText} />
                        })}
                   
                    </tbody>
                    
                </table>
            </div>
        );
    }
}

export default connect(
    (state) => {
        return {
            leftList: state.left_item,
            isLogin: state.isLogin,
            user: state.user
        }
    },
    (dispatch) => {
        return {
            handleLogout: () => dispatch(logout())
        }
    }
)(Menu);
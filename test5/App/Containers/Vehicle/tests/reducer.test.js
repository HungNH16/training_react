import { fromJS } from 'immutable';
import vehicleReducer, { initialState, initialForm } from '../reducer';
import {
    ADD_VEHICLE,
    ADD_VEHICLE_FAILED,
    ADD_VEHICLE_SUCCESS,
    UPDATE_INPUT,
    UPDATE_ERRORS,
    UPDATE_SUCCESS_FLAG,
    RESET_FORM,
    LOAD_VEHICLE_REQUEST_ACTION,
    LOAD_VEHICLE_SUCCESS_ACTION,
} from '../constants';

describe('Vehicle Reducer', () => {
    it('resets success flag when requesting action ADD_VEHICLE', () => {
        const expectedState = fromJS(initialState).set('addSuccessFlag', false);
        expect(vehicleReducer(undefined, { type: ADD_VEHICLE })).toEqual(expectedState);
    });

    it('shows success popup by flag in action ADD_VEHICLE_SUCCESS', () => {
        const licensePlate = '30-N4-2310';
        const actionData = {
            type: ADD_VEHICLE_SUCCESS,
            response: {
                data: {
                    licensePlate,
                },
            },
        };
        const expectedState = fromJS(initialState).set('addSuccessFlag', !!licensePlate);
        expect(vehicleReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('returns error message(s) & corresponding invalid field(s) in action ADD_VEHICLE_FAILED', () => {
        const input = {
            errors: ['Email Address already existed.'],
            invalidFields: ['email'],
        };
        const actionData = {
            type: ADD_VEHICLE_FAILED,
            ...input,
        };
        const expectedState = fromJS(initialState)
            .set('errors', input.errors)
            .set('invalidFields', input.invalidFields);
        expect(vehicleReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('adds new value for a field in action UPDATE_INPUT', () => {
        const input = {
            name: 'license',
            value: '30-N4-2310',
        };
        const actionData = {
            type: UPDATE_INPUT,
            ...input,
        };
        const expectedState = fromJS(initialState).setIn(['formValues', 'license'], '30-N4-2310');
        expectedState.setIn(['formValues', input.name], input.value).set('invalidFields', expectedState.get('invalidFields').filter(field => field !== input.name));
        expect(vehicleReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('store new error message(s) into state in action UPDATE_ERRORS', () => {
        const input = {
            errors: ['Please complete all required fields.'],
            invalidFields: ['license', 'weight'],
        };
        const actionData = {
            type: UPDATE_ERRORS,
            ...input,
        };
        const expectedState = fromJS(initialState)
            .set('errors', input.errors)
            .set('invalidFields', input.invalidFields);
        expect(vehicleReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('activates/deactivates success flag in action UPDATE_SUCCESS_FLAG', () => {
        const bool = true;
        const actionData = {
            type: UPDATE_SUCCESS_FLAG,
            bool,
        };
        const expectedState = fromJS(initialState).set('addSuccessFlag', bool);
        expect(vehicleReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('clears all values & errors in action RESET_FORM', () => {
        const input = {
            errors: [],
            invalidFields: [],
        };
        const actionData = {
            type: RESET_FORM,
        };
        const expectedState = fromJS(initialState)
            .set('formValues', fromJS(initialForm))
            .set('errors', input.errors)
            .set('invalidFields', input.invalidFields);
        expect(vehicleReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('returns the state with default page = "1" in action LOAD_VEHICLE_REQUEST_ACTION ', () => {
        const expectedState = fromJS(initialState.toJS()).setIn(['pagination', 'currentPage'], 1);
        expect(vehicleReducer(undefined, { type: LOAD_VEHICLE_REQUEST_ACTION })).toEqual(expectedState);
    });

    it('returns the state with vehicle data & default table pagination in action LOAD_VEHICLE_SUCCESS_ACTION ', () => {
        const vehicles = [{}];
        const pagination = {
            currentPage: 0,
            perPage: 10,
            total: 1,
        };
        const actionData = {
            type: LOAD_VEHICLE_SUCCESS_ACTION,
            data: {
                data: vehicles,
                meta: {
                    pagination,
                },
            },
        };
        const expectedState = fromJS(initialState)
            .set('vehicles', vehicles)
            .set('pagination', fromJS(pagination));
        expect(vehicleReducer(undefined, actionData)).toEqual(expectedState);
    });
    it('default action', () => {
        const actionData = {
        };
        const expectedState = fromJS(initialState);
        expect(vehicleReducer(undefined, actionData)).toEqual(expectedState);
    });
});

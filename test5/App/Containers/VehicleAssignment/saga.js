/**
 *
 * Vehicle Assignment Saga
 *
 * This implements all asynchronous methods for Vehicle Assignment screen
 * to communicate with server
 */
// ===== IMPORTS =====
// Modules
import { call, put, takeLatest } from 'redux-saga/effects';
import { get as _get } from 'lodash';
// Utilities, Constants & Styles
import request, { fetchAxios } from '../../utils/request';
import { convertResponseAlerts } from '../../utils';
import { getToken } from '../../../services/auth';
import { loadVehicleAssignmentDataSuccess, loadDriverDataSuccess, loadVehicleDataSuccess, submitVehicleAssignmentDataSuccess, updateErrors } from './actions';
import { LOAD_DATA, LOAD_DRIVER, LOAD_VEHICLE, SUBMIT_DATA } from './constants';
import { API } from '../../constants';
// ===================

// ===== SAGA =====
export function* loadVehicleAssignment() {
    try {
        const response = yield call(request, API.FLEET_VEHICLE_ASSIGNMENTS_LIST, { method: 'GET', headers: { token: getToken() } });
        if (response.data) {
            yield put(loadVehicleAssignmentDataSuccess(response));
        } else if (response.alerts) {
            yield put(updateErrors(_get(response, 'alerts', []).map(item => item.message), []));
        } else {
            yield put(updateErrors(['Unexpected response structure!'], []));
        }
    } catch {
        yield put(updateErrors(['Internal server error!'], []));
    }
}

export function* loadDriver() {
    try {
        const response = yield call(request, API.FLEET_DRIVERS, { method: 'GET', headers: { token: getToken() } });
        if (response.data) {
            yield put(loadDriverDataSuccess(response));
        } else if (response.alerts) {
            yield put(updateErrors(_get(response, 'alerts', []).map(item => item.message), []));
        } else {
            yield put(updateErrors(['Unexpected response structure!'], []));
        }
    } catch {
        yield put(updateErrors(['Internal server error!'], []));
    }
}

export function* loadVehicle() {
    try {
        const response = yield call(request, API.FLEET_VEHICLES, { method: 'GET', headers: { token: getToken() } });
        if (response.data) {
            yield put(loadVehicleDataSuccess(response));
        } else if (response.alerts) {
            yield put(updateErrors(_get(response, 'alerts', []).map(item => item.message), []));
        } else {
            yield put(updateErrors(['Unexpected response structure!'], []));
        }
    } catch {
        yield put(updateErrors(['Internal server error!'], []));
    }
}

export function* updateVehicleAssignment(actionData) {
    try {
        const { data } = actionData;
        const response = yield call(fetchAxios, {
            url: API.FLEET_VEHICLE_ASSIGNMENTS_SUBMIT,
            method: 'post',
            headers: {
                token: getToken(),
                'Content-Type': 'application/json',
            },
            responseType: 'json',
            data,
        });
        yield put(submitVehicleAssignmentDataSuccess(response));
    } catch (error) {
        const errorObj = convertResponseAlerts(error);
        yield put(updateErrors(errorObj.errors, errorObj.invalidFields));
    }
}

// Root saga manages watcher lifecycle
export default function* vehicleAssignmentSaga() {
    yield takeLatest(LOAD_DATA, loadVehicleAssignment);
    yield takeLatest(LOAD_DRIVER, loadDriver);
    yield takeLatest(LOAD_VEHICLE, loadVehicle);
    yield takeLatest(SUBMIT_DATA, updateVehicleAssignment);
}
// ================

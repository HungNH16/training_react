import { StyleSheet } from 'react-native';
import { Colors, Fonts } from '../../Themes';


export default StyleSheet.create({
    RetailerBtn: {
        backgroundColor: Colors.main,
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 7,
        borderRadius: 14,
        width: 90,
    },
    RetailerBtnText: {
        fontSize: Fonts.size.tiny,
        lineHeight: Fonts.size.small,
        textAlign: 'center',
        color: Colors.white,
        fontWeight: '200', 

    },
    Call: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    marketalt: {
        flexDirection: 'row',
        paddingTop:10
    },
    cube: {
        flexDirection: 'row',
        paddingTop: 10
    },
    typeText:{
        color: Colors.black,
        fontWeight: 'bold',
        fontSize: Fonts.size.small,
    },
    name:{
        color: Colors.black,
        fontWeight: 'bold',
        fontSize: Fonts.size.small,
    },
    marketaltText:{
        color: Colors.midNight,
        paddingLeft:5,
        paddingRight: 20,
        fontSize: Fonts.size.small,
    },
    packages:{
        paddingLeft:5,
        color: Colors.midNight,
        fontSize: Fonts.size.small
    }
});
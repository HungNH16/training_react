import { takeLatest, call, put, select } from 'redux-saga/effects';
import { get as _get } from 'lodash';
import request, { fetchAxios } from 'utils/request';
import { loadDriverSuccessAction, loadDriverFailureAction, addNewDriverSuccess, addNewDriverFailed } from './actions';
import { LOAD_DRIVER_REQUEST_ACTION, ADD_DRIVER } from './constants';
import { selectDriverDomain } from './selectors';
import { getToken } from '../../../services/auth';
import { API, ENUM } from '../../constants';

export function* searchDriver(requestData) {
    const {
        params: { page },
    } = requestData;

    const dataState = yield select(selectDriverDomain);
    const currentPage = dataState.getIn(['pagination', 'currentPage']) || page;

    try {
        const response = yield call(request, `${API.FLEET_DRIVERS}?page=${currentPage}`, { method: 'GET', headers: { token: getToken() } });
        yield put(loadDriverSuccessAction(response));
    } catch (error) {
        yield put(loadDriverFailureAction('internal server'));
    }
}

export function* addDriver(actionData) {
    try {
        const response = yield call(fetchAxios, { url: API.FLEET_DRIVERS, method: 'post', headers: { token: getToken(), 'Content-Type': 'application/json' }, responseType: 'json', data: actionData.data });
        yield put(addNewDriverSuccess(response));
    } catch (error) {
        const responseErrors = _get(error, 'response.data.alerts', []);
        const errors = [];
        responseErrors.map(err => {
            if (!errors.includes(err.message)) {
                errors.push(err.message);
            }

            return err;
        });
        const invalidFields = responseErrors.map(err => {
            const key = _get(err, 'meta.field', '');
            return ENUM.DRIVER_FIELD_MAP[key] ? ENUM.DRIVER_FIELD_MAP[key] : key;
        });
        yield put(addNewDriverFailed(errors, invalidFields));
    }
}

// Root saga manages watcher lifecycle
export default function* driverSaga() {
    yield takeLatest(LOAD_DRIVER_REQUEST_ACTION, searchDriver);
    yield takeLatest(ADD_DRIVER, addDriver);
}

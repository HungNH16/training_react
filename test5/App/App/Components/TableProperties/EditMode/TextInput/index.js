import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'reactstrap';
import { get as _get } from 'lodash';

const TextInput = ({ type = 'text', name, handleChangeInput, rowId, isInvalid, ...remains }) => {
    const onChange = event => handleChangeInput(rowId, name, _get(event, 'target.value', ''));

    return <Input type={type} name={name} onChange={onChange} invalid={isInvalid} {...remains} />;
};

TextInput.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    handleChangeInput: PropTypes.func,
    rowId: PropTypes.number,
    markFieldInvalid: PropTypes.func,
};

export default TextInput;

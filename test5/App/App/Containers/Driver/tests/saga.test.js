/**
 * Tests for HomePage sagas
 */

import { put, takeLatest } from 'redux-saga/effects';
import { fromJS } from 'immutable';

import { LOAD_DRIVER_REQUEST_ACTION, ADD_DRIVER } from '../constants';
import { loadDriverSuccessAction, loadDriverFailureAction, addNewDriverSuccess, addNewDriverFailed } from '../actions';
import driverSaga, { searchDriver, addDriver } from '../saga';

describe('searchDriver Saga', () => {
    let searchDriverGenerator;
    beforeEach(() => {
        searchDriverGenerator = searchDriver({ params: { page: 1 } });
        const pagination = { pagination: { currentPage: 1 } };
        const selectDescriptor = searchDriverGenerator.next().value;
        expect(selectDescriptor).toMatchSnapshot();

        const callDescriptor = searchDriverGenerator.next(fromJS(pagination)).value;
        expect(callDescriptor).toMatchSnapshot();
    });

    it('loadDriverSuccessAction successfully', () => {
        const response = [];
        const putDescriptor = searchDriverGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(loadDriverSuccessAction(response)));
    });

    it('should call the loadDriverFailureAction action if the response errors', () => {
        const putDescriptor = searchDriverGenerator.throw().value;
        expect(putDescriptor).toEqual(put(loadDriverFailureAction('internal server')));
    });
});

describe('addDriver Saga', () => {
    let addDriverGenerator;
    beforeEach(() => {
        addDriverGenerator = addDriver({ params: { page: 1 } });
        const selectDescriptor = addDriverGenerator.next().value;
        expect(selectDescriptor).toMatchSnapshot();
    });

    it('addNewDriverSuccess successfully', () => {
        const response = [
            {
                driverID: '',
            },
        ];
        const putDescriptor = addDriverGenerator.next(response).value;
        expect(putDescriptor).toEqual(put(addNewDriverSuccess(response)));
    });

    it('should call the addNewDriverFailed action if the response errors', () => {
        const putDescriptor = addDriverGenerator.throw({ response: { data: { errors: [] } } }).value;
        expect(putDescriptor).toEqual(put(addNewDriverFailed([], [])));
    });
});

describe('driverSagaRoot Saga', () => {
    const driverSagaRoot = driverSaga();

    it('should start task to watch for ADD_DRIVER action', () => {
        const takeLatestDescriptor = driverSagaRoot.next().value;
        expect(takeLatestDescriptor).toEqual(takeLatest(LOAD_DRIVER_REQUEST_ACTION, searchDriver));

        const takeLatestAddVehicle = driverSagaRoot.next().value;
        expect(takeLatestAddVehicle).toEqual(takeLatest(ADD_DRIVER, addDriver));
    });
});

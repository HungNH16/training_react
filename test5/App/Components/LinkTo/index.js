import React from 'react';
import { TouchableOpacity, ViewPropTypes, Text } from 'react-native';
import PropTypes from 'prop-types';
// Styles
import styles from './styles';

const LinkTo = ({ value, style, onPress }) => (
  <TouchableOpacity style={styles.linkContainer} onPress={onPress}>
    <Text style={[styles.linkValue, style]}>{value}</Text>
  </TouchableOpacity>
);

LinkTo.propTypes = {
  value: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  style: ViewPropTypes.style,
};

LinkTo.defaultProps = {
  onPress: () => {},
  style: {},
};

export default LinkTo;

import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the vehicle state domain
 */

const selectVehicleDomain = state => state.get('vehicle', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by Vehicle
 */

const makeSelectVehicle = () => createSelector(selectVehicleDomain, substate => substate.toJS());

const selectFormValues = createSelector(selectVehicleDomain, obj => obj.toJS().formValues);

const selectErrors = createSelector(selectVehicleDomain, obj => obj.toJS().errors);

const selectInvalidFields = createSelector(selectVehicleDomain, obj => obj.toJS().invalidFields);

const selectSuccessFlag = createSelector(selectVehicleDomain, obj => obj.toJS().addSuccessFlag);

export default makeSelectVehicle;
export { selectVehicleDomain, selectFormValues, selectErrors, selectInvalidFields, selectSuccessFlag };

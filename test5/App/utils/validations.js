/**
 *
 * Validation utilities
 *
 * This file contains all validation methods for all types of field,
 * which if check conditions aren't satisfied will return error messages.
 * List of validations:
 * -    Mandatory field.
 * -    Email format.
 * -    Phone number format.
 * -    Max length.
 * -    Min length.
 */
// ===== IMPORTS =====
import { isEmpty as _isEmpty, isNumber as _isNumber  } from 'lodash';
// ===================

// ===== SUPPORT VARIABLES =====
const phoneRegex = /^\+\([0-9]{1,2}\) [0-9]*$/;
const emailRegex = /^[^ @]{1,63}@[^ @]{1,255}$/;
const tagRegex = /^#[^#, ]+(,[ ]*#[^#, ]+)*$/;
// =============================

// ===== VALIDATIONS =====
const isRequired = data => {
    const errors = {};

    data.forEach(field => {
        if (_isNumber(field.value)) {
            if ([0, NaN].includes(field.value)) {
                errors[field.name] = 'Please complete all required fields.';
            }
        } else if (_isEmpty(field.value)) {
            errors[field.name] = 'Please complete all required fields.';
        }
    });

    return errors;
};

const isPhoneValid = ({ name, value }) =>
    value && !phoneRegex.test(value)
        ? {
            [name]: 'Please enter a valid phone number.',
        }
        : {};

const isEmailValid = ({ name, value }) =>
    value && !emailRegex.test(value)
        ? {
            [name]: 'Please enter a valid email address.',
        }
        : {};

const isTagValid = ({ name, value }) =>
    value && !tagRegex.test(value)
        ? {
            [name]: 'Please enter valid tag(s).',
        }
        : {};

const isMaxlength = input =>
    !_isEmpty(input.value) && input.value.length > input.max
        ? {
            [input.name]: `Maximum length not met for ${input.label}.`,
        }
        : {};

const isMinLength = input =>
    !_isEmpty(input.value) && input.value.length < input.min
        ? {
            [input.name]: `Mininum length not met for ${input.label}.`,
        }
        : {};

const isMaxNumber = input =>
    !_isEmpty(input.value) && Number(input.value) > input.max
        ? {
            [input.name]: `Maximum value not met for ${input.label}.`,
        }
        : {};

const isMinNumber = input =>
    !_isEmpty(input.value) && Number(input.value) < input.min
        ? {
            [input.name]: `Minimum value not met for ${input.label}.`,
        }
        : {};
// =======================

export default {
    isRequired,
    isPhoneValid,
    isEmailValid,
    isMaxlength,
    isMinLength,
    isTagValid,
    isMaxNumber,
    isMinNumber,
};

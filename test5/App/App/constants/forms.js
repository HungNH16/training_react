/**
 *
 * Form Configurations
 *
 * This contains configurations for all kinds of form in Dispatcher Platform
 */
// ===== Imports =====
import { roundDecimals } from '../utils';
import LIST from './lists';
// ===================

// ===== CONFIGURATIONS =====
// Create-new form configuration
const VEHICLE_FORM = [
    {
        fieldId: 'vehicle_license',
        label: 'License Plate',
        type: 'text',
        name: 'license',
        placeholder: 'EA123A',
    },
    {
        fieldId: 'vehicle_make_model',
        label: 'Vehicle Make & Model',
        type: 'select',
        name: 'type',
    },
    {
        fieldId: 'vehicle_volume',
        label: 'Volume',
        name: 'volume',
        inputs: [
            {
                fieldId: 'vehicle_length',
                type: 'number',
                name: 'length',
                placeholder: 'Length (cm)',
                transformValue: roundDecimals,
                colSize: 4,
            },
            {
                fieldId: 'vehicle_width',
                type: 'number',
                name: 'width',
                placeholder: 'Width (cm)',
                transformValue: roundDecimals,
                colSize: 4,
            },
            {
                fieldId: 'vehicle_height',
                type: 'number',
                name: 'height',
                placeholder: 'Height (cm)',
                transformValue: roundDecimals,
                colSize: 4,
            },
        ],
    },
    {
        fieldId: 'vehicle_weight',
        label: 'Weight',
        type: 'number',
        name: 'weight',
        transformValue: roundDecimals,
        placeholder: 'Kg',
    },
    {
        fieldId: 'vehicle_status',
        label: 'Status',
        type: 'select',
        name: 'status',
        options: LIST.VEHICLE_STATUSES,
    },
    {
        fieldId: 'vehicle_max_working_hour',
        label: 'Maximum Working Hours',
        type: 'number',
        name: 'maximumWorkingHours',
    },
    {
        fieldId: 'vehicle_tag',
        label: 'Tag(s)',
        type: 'text',
        name: 'tags',
        placeholder: 'e.g #west, #north',
    },
    {
        fieldId: 'vehicle_characteristics',
        label: 'Characteristics',
        type: 'select',
        name: 'characteristics',
        placeholder: 'None',
        options: LIST.VEHICLE_CHARACTERISTICS,
        multiple: false,
    },
];

const DRIVER_FORM = [
    {
        fieldId: 'driver_name',
        label: 'Driver Name',
        type: 'text',
        name: 'name',
        placeholder: 'e.g. John',
    },
    {
        fieldId: 'driver_phone',
        label: 'Mobile No.',
        type: 'tel',
        name: 'phone',
    },
    {
        fieldId: 'driver_working_hours',
        label: 'Working Hours',
        type: 'workingHours',
        name: 'workingHours',
        options: [],
    },
    {
        fieldId: 'driver_status',
        label: 'Status',
        type: 'select',
        name: 'status',
        options: LIST.DRIVER_STATUSES,
    },
    {
        fieldId: 'driver_tags',
        label: 'Tag(s)',
        type: 'text',
        name: 'tags',
        placeholder: '#south, #west',
    },
];
// ==========================

export default {
    VEHICLE_FORM,
    DRIVER_FORM,
};

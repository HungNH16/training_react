import React from 'react';
import { createStackNavigator, createAppContainer, createBottomTabNavigator } from 'react-navigation';
import FAI from 'react-native-vector-icons/FontAwesome';
// Header bar and bottom navigation icons
import HeaderBar from '../Components/HeaderBar';
// Screen
import PlanScreen from '../Containers/PlanScreen';
import LaunchScreen from '../Containers/LaunchScreen';
import LoginScreen from '../Containers/LoginScreen';

const MainScreen = createBottomTabNavigator({
  Screen: createStackNavigator({
    PlanScreen: { screen: PlanScreen },
  }, {
    initialRouteName: 'PlanScreen',
    headerMode: 'screen',
    defaultNavigationOptions: {
      header: HeaderBar,
    },
  }),
},
{
  defaultNavigationOptions: ({ navigation }) => ({
    initialRouteName: 'PlanScreen',
    tabBarIcon: ({ focused, horizontal, tintColor }) => null,
  }),
  tabBarOptions: {
    activeTintColor: 'tomato',
    inactiveTintColor: 'gray',
  },
});

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  LaunchScreen: { screen: LaunchScreen },
  LoginScreen: { screen: LoginScreen },
  MainScreen,
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'MainScreen',
});

export default createAppContainer(PrimaryNav);

/*
 *
 * Drivers Management actions
 *
 */
import {
    LOAD_DRIVER_REQUEST_ACTION,
    LOAD_DRIVER_SUCCESS_ACTION,
    LOAD_DRIVER_FAILURE_ACTION,
    ADD_DRIVER,
    ADD_DRIVER_SUCCESS,
    ADD_DRIVER_FAILED,
    UPDATE_INPUT,
    UPDATE_ERRORS,
    RESET_FORM,
    UPDATE_SUCCESS_FLAG,
    RESET_STATE,
} from './constants';

export function addNewDriver(data) {
    return {
        type: ADD_DRIVER,
        data,
    };
}

export function addNewDriverSuccess(response) {
    return {
        type: ADD_DRIVER_SUCCESS,
        response,
    };
}

export function addNewDriverFailed(errors, invalidFields) {
    return {
        type: ADD_DRIVER_FAILED,
        errors,
        invalidFields,
    };
}

export function updateInput(name, value) {
    return {
        type: UPDATE_INPUT,
        name,
        value,
    };
}

export function updateErrors(errors, invalidFields) {
    return {
        type: UPDATE_ERRORS,
        errors,
        invalidFields,
    };
}

export function resetForm() {
    return {
        type: RESET_FORM,
    };
}

export function updateSuccessFlag(bool) {
    return {
        type: UPDATE_SUCCESS_FLAG,
        bool,
    };
}

export function loadDriverAction(params) {
    return {
        type: LOAD_DRIVER_REQUEST_ACTION,
        params,
    };
}
export function loadDriverSuccessAction(data) {
    return {
        type: LOAD_DRIVER_SUCCESS_ACTION,
        data,
    };
}

export function loadDriverFailureAction(data) {
    return {
        type: LOAD_DRIVER_FAILURE_ACTION,
        data,
    };
}

export function resetState() {
    return {
        type: RESET_STATE,
    };
}

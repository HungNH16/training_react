import Metrics from './Metrics';
import Colors from './Colors';

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {
  screen: {
    mainContainer: { // Fullwidth container
      flex: 1,
      backgroundColor: Colors.background,
    },
    centerContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.background,
    },
    container: { // Container with padding
      width: Metrics.screenWidth,
      paddingHorizontal: Metrics.marginHorizontal,
    },
    logo: {
      width: Metrics.images.logo,
      resizeMode: 'contain',
    },
  },
};

export default ApplicationStyles;

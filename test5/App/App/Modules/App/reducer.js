import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setLoading: ['payload'],
  shouldShowPopUp: ['payload'],
  setPopUpContent: ['payload'],
  bootStrapApp: null,
});

export const AppTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

const INITIAL_STATE = Immutable({
  isLoading: false, // Loading indicator
  isPopUpShowed: false, // Show popup or not
  popUpContent: null, // Popup content(can be text or component)
});

/* ------------- Reducers ------------- */

const setLoading = (state, { payload: isLoading }) => state.merge({ isLoading });
const setShowPopUp = (state, { payload: isPopUpShowed }) => state.merge({ isPopUpShowed });
const setPopUpContent = (state, { payload: popUpContent }) => state.merge({ popUpContent });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_LOADING]: setLoading,
  [Types.SHOULD_SHOW_POP_UP]: setShowPopUp,
  [Types.SET_POP_UP_CONTENT]: setPopUpContent,
});

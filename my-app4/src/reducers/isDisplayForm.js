import * as types from './../constants/ActionTypes';

var initialState = {
    form: false,
};
var myReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.TOGGLE_FORM:
            return {
                ...state,
                form: !state.form
            };
        case types.OPEN_FORM:
            return {
                form:true,
            }
        case types.CLOSE_FORM:
            return {form:false}

        default: return state;
    }
};
export default myReducer;
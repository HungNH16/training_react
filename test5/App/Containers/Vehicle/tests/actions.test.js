import {
    addNewVehicle,
    addNewVehicleSuccess,
    addNewVehicleFailed,
    updateInput,
    updateErrors,
    updateSuccessFlag,
    resetForm,
    loadVehicleAction,
    loadVehicleSuccessAction,
} from '../actions';
import {
    ADD_VEHICLE,
    ADD_VEHICLE_SUCCESS,
    ADD_VEHICLE_FAILED,
    UPDATE_INPUT,
    UPDATE_ERRORS,
    UPDATE_SUCCESS_FLAG,
    RESET_FORM,
    LOAD_VEHICLE_REQUEST_ACTION,
    LOAD_VEHICLE_SUCCESS_ACTION,
} from '../constants';

describe('Vehicles Management actions', () => {
    it('has a type of ADD_VEHICLE', () => {
        const data = {
            '3plID': 1,
            licensePlate: '30-N4-2310',
            makeModel: {
                model: 'Honda',
                make: 'Wave RSX',
            },
            volume: {
                length: 2.45,
                width: 0.45,
                height: 0.8,
            },
            weight: 175,
            status: 1,
            tags: ['#honda', '#bike'],
            characteristics: ['Cold Storage', 'Storage Restriction - South'],
        };
        const expected = {
            type: ADD_VEHICLE,
            data,
        };
        expect(addNewVehicle(data)).toEqual(expected);
    });

    it('has a type of ADD_VEHICLE_SUCCESS', () => {
        const response = {
            data: {
                licensePlate: '30-N4-2310',
            },
        };
        const expected = {
            type: ADD_VEHICLE_SUCCESS,
            response,
        };
        expect(addNewVehicleSuccess(response)).toEqual(expected);
    });

    it('has a type of ADD_VEHICLE_FAILED', () => {
        const response = {
            errors: ['License Plate already existed.'],
            invalidFields: ['license'],
        };
        const expected = {
            type: ADD_VEHICLE_FAILED,
            ...response,
        };
        expect(addNewVehicleFailed(response.errors, response.invalidFields)).toEqual(expected);
    });

    it('has a type of UPDATE_INPUT', () => {
        const input = {
            name: 'license',
            value: '30-N4-2310',
        };
        const expected = {
            type: UPDATE_INPUT,
            ...input,
        };
        expect(updateInput(input.name, input.value)).toEqual(expected);
    });

    it('has a type of UPDATE_ERRORS', () => {
        const input = {
            errors: ['Please complete all required fields.'],
            invalidFields: ['license', 'model'],
        };
        const expected = {
            type: UPDATE_ERRORS,
            ...input,
        };
        expect(updateErrors(input.errors, input.invalidFields)).toEqual(expected);
    });

    it('has a type of UPDATE_SUCCESS_FLAG', () => {
        const bool = true;
        const expected = {
            type: UPDATE_SUCCESS_FLAG,
            bool,
        };
        expect(updateSuccessFlag(bool)).toEqual(expected);
    });

    it('has a type of RESET_FORM', () => {
        expect(resetForm()).toEqual({ type: RESET_FORM });
    });

    it('has a type of LOAD_VEHICLE_REQUEST_ACTION', () => {
        const expected = {
            type: LOAD_VEHICLE_REQUEST_ACTION,
        };
        expect(loadVehicleAction()).toEqual(expected);
    });

    it('has a type of LOAD_VEHICLE_SUCCESS_ACTION', () => {
        const data = {};
        const expected = {
            type: LOAD_VEHICLE_SUCCESS_ACTION,
            data,
        };
        expect(loadVehicleSuccessAction(data)).toEqual(expected);
    });

});

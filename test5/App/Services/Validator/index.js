import R from 'ramda';

export const NOT_EMPTY = 'could_not_empty';
const validators = {
  [NOT_EMPTY]: value => !R.isEmpty(value),
};

const isValid = (data, schema) => {
  const errors = {};
  Object
    .keys(schema)
    .forEach((schemaKey) => {
      const ruleList = schema[schemaKey];
      ruleList.forEach((rule) => {
        if (!validators[rule](data[schemaKey])) {
          errors[schemaKey] = `${schemaKey} ${rule}`;
        }
      });
    });

  return errors;
};

export default isValid;

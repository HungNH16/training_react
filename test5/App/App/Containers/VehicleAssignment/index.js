/**
 *
 * Vehicle Assignment Table
 *
 * This container defines a table of driver-vehicle mappings in current 3PL account only
 */
// ===== IMPORTS =====
// Modules
import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { isEqual as _isEqual, get as _get, isEmpty as _isEmpty, uniq as _uniq } from 'lodash';
import { EditModeTable } from 'cf-web-ui-kit-components';
// Custom Components
import NoDataComponent from '../../components/TableProperties/NoDataComponent';
import { SuccessPopup } from '../../components/Popup';
// Utilities, Constants & Styles
import reducer from './reducer';
import saga from './saga';
import { selectData, selectErrors, selectDrivers, selectInvalidFields, selectSuccessFlag, selectVehicles, selectRowValues, selectOriginalValues } from './selectors';
import {
    loadVehicleAssignmentData,
    resetState,
    loadDriverData,
    loadVehicleData,
    addRow,
    deleteRow,
    revertData,
    updateDriver,
    updateVehicle,
    updateStatus,
    updateErrors,
    updateSuccessFlag,
    submitVehicleAssignmentData,
} from './actions';
import { VEHICLE_ASSIGNMENT_DOMAIN, VEHICLE_ASSIGNMENT_COLUMN_MAP } from './constants';
import { VehicleAssignmentConfig } from '../../components/TableProperties/Configurations';
import { injectReducer, injectSaga, validations } from '../../utils';
import { MSG } from '../../constants';
import '../../scss/containers/common.scss';
import '../../scss/containers/vehicle_assignment.scss';
// ===================

// ===== MAIN COMPONENT =====
class VehicleAssignmentTable extends Component {
    constructor(props) {
        super(props);

        this.viewColumns = [
            VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME,
            VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS,
            VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE,
            VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS,
        ];
        this.editColumns = Object.values(VEHICLE_ASSIGNMENT_COLUMN_MAP);

        this.handleLoadData = this.handleLoadData.bind(this);
        this.filterDriverOptions = this.filterDriverOptions.bind(this);
        this.filterVehicleOptions = this.filterVehicleOptions.bind(this);
        this.markFieldInvalid = this.markFieldInvalid.bind(this);
        this.handleChangeDriver = this.handleChangeDriver.bind(this);
        this.handleChangeVehicle = this.handleChangeVehicle.bind(this);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);
        this.handleAddRow = this.handleAddRow.bind(this);
        this.handleDeleteRow = this.handleDeleteRow.bind(this);
        this.handleRevertData = this.handleRevertData.bind(this);
        this.validateForm = this.validateForm.bind(this);
        this.getSubmitRequest = this.getSubmitRequest.bind(this);
        this.handleSubmitData = this.handleSubmitData.bind(this);
        this.handleCloseSuccessPopup = this.handleCloseSuccessPopup.bind(this);
        this.enableSubmitButton = this.enableSubmitButton.bind(this);
        this.enableAddRowButton = this.enableAddRowButton.bind(this);
    }

    componentDidMount() {
        this.handleLoadData();
        this.handleLoadDrivers();
        this.handleLoadVehices();
    }

    componentDidUpdate({ successFlag: prevSuccessFlag }) {
        const { successFlag } = this.props;
        if (!prevSuccessFlag && successFlag) {
            this.handleLoadData();
            this.handleLoadDrivers();
            this.handleLoadVehices();
        }
    }

    componentWillUnmount() {
        this.handleResetState();
    }

    handleLoadData() {
        const { onLoadVehicleAssignmentData } = this.props;
        onLoadVehicleAssignmentData();
    }

    handleLoadDrivers() {
        const { onLoadDrivers } = this.props;
        onLoadDrivers();
    }

    handleLoadVehices() {
        const { onLoadVehicles } = this.props;
        onLoadVehicles();
    }

    handleResetState() {
        const { onResetState } = this.props;
        onResetState();
    }

    filterDriverOptions(rowId) {
        const { drivers, rowValues } = this.props;
        return drivers.filter(opt => !rowValues.some((row, index) => index !== rowId && _isEqual(row.driverName, opt)));
    }

    filterVehicleOptions(rowId) {
        const { vehicles, rowValues } = this.props;
        return vehicles.filter(opt => !rowValues.some((row, index) => index !== rowId && _isEqual(row.vehicle, opt)));
    }

    handleChangeDriver(rowId, _, value) {
        const { onUpdateDriver } = this.props;
        onUpdateDriver(rowId, value);
    }

    handleChangeVehicle(rowId, _, value) {
        const { onUpdateVehicle } = this.props;
        onUpdateVehicle(rowId, value);
    }

    handleChangeStatus(rowId, name, value) {
        const { onUpdateStatus } = this.props;
        onUpdateStatus(rowId, name, value);
    }

    markFieldInvalid(name) {
        const { invalidFields } = this.props;
        return invalidFields.includes(name);
    }

    handleAddRow() {
        const { onAddRow } = this.props;
        if (this.enableAddRowButton()) {
            onAddRow();
        }
    }

    handleDeleteRow({ index: rowId }) {
        const { onDeleteRow } = this.props;
        onDeleteRow(rowId);
    }

    handleRevertData() {
        const { onRevertData } = this.props;
        onRevertData();
    }

    validateForm() {
        const { isRequired } = validations;
        const { rowValues } = this.props;
        let errors = {};

        rowValues.forEach((row, index) => {
            errors = {
                ...errors,
                ...isRequired([
                    { name: `data_${index}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME}`, value: row[VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME] },
                    { name: `data_${index}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS}`, value: row[VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS] },
                    { name: `data_${index}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE}`, value: row[VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE] },
                    { name: `data_${index}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS}`, value: row[VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS] },
                ]),
            };
        });

        return errors;
    }

    getSubmitRequest() {
        const { rowValues } = this.props;

        return {
            status: 'Published',
            details: rowValues.map(row => ({
                driverID: _get(row, [VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME, 'value'], ''),
                driverStatus: _get(row, [VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS, 'value'], 0),
                vehicleLicensePlate: _get(row, [VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE, 'value'], ''),
                vehicleStatus: _get(row, [VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS, 'value'], 0),
            })),
        };
    }

    handleSubmitData() {
        const { onUpdateErrors, onSubmitData } = this.props;
        const errorList = this.validateForm();

        if (!_isEmpty(errorList)) {
            onUpdateErrors(_uniq(Object.values(errorList)), Object.keys(errorList));
        } else {
            onSubmitData(this.getSubmitRequest());
        }
    }

    handleCloseSuccessPopup() {
        const { onUpdateSuccessFlag } = this.props;
        onUpdateSuccessFlag(false);
    }

    enableSubmitButton() {
        const { originalValues, rowValues } = this.props;
        return (
            !_isEqual(originalValues, rowValues) &&
            !rowValues.some(row => _get(row, [VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS, 'value']) === 5 || _get(row, [VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS, 'value']) === 5)
        );
    }

    enableAddRowButton() {
        const { drivers = [], vehicles = [], rowValues = [] } = this.props;

        if (drivers.length > vehicles.length) {
            return rowValues.length < vehicles.length;
        }
        return rowValues.length < drivers.length;
    }

    render() {
        const {
            intl: { formatMessage },
            data,
            errors,
            rowValues,
            successFlag,
        } = this.props;
        const isSubmittable = this.enableSubmitButton();
        const canAddRow = this.enableAddRowButton();

        return (
            <Fragment>
                <Helmet>
                    <title>{formatMessage(MSG.VEHICLE_ASSIGNMENT_TITLE)}</title>
                </Helmet>
                <EditModeTable
                    data={data}
                    editValues={rowValues}
                    errors={errors}
                    tableTitle={formatMessage(MSG.VEHICLE_ASSIGNMENT_TABLE_TITLE)}
                    openEditLabel={formatMessage(MSG.VEHICLE_ASSIGNMENT_TABLE_BUTTON_OPEN_EDIT)}
                    addRowLabel={formatMessage(MSG.VEHICLE_ASSIGNMENT_TABLE_BUTTON_ADD_ROW)}
                    updateTableConfig={VehicleAssignmentConfig}
                    getConfigProps={{
                        formatMessage,
                        filterDriverOptions: this.filterDriverOptions,
                        filterVehicleOptions: this.filterVehicleOptions,
                        markFieldInvalid: this.markFieldInvalid,
                        changeDriver: this.handleChangeDriver,
                        changeVehicle: this.handleChangeVehicle,
                        changeStatus: this.handleChangeStatus,
                    }}
                    NoDataComponent={() => <NoDataComponent>{formatMessage(MSG.NO_DATA_TEXT)}</NoDataComponent>}
                    tableClassName="cf-dispatcher-vehicle-assignment mb-2"
                    addRow={this.handleAddRow}
                    deleteRow={this.handleDeleteRow}
                    cancelEdit={this.handleRevertData}
                    submitData={isSubmittable && this.handleSubmitData}
                    viewColumns={this.viewColumns}
                    editColumns={this.editColumns}
                    isSuccess={successFlag}
                    isSubmittable={isSubmittable}
                    canAddRow={canAddRow}
                />
                <SuccessPopup isOpen={successFlag} successMsg={formatMessage(MSG.VEHICLE_ASSIGNMENT_POPUP_SUCCESS_MESSAGE)} handleExecution={this.handleCloseSuccessPopup} />
            </Fragment>
        );
    }
}
// ==========================

// ===== PROP TYPES =====
VehicleAssignmentTable.propTypes = {
    intl: PropTypes.object,
    data: PropTypes.arrayOf(PropTypes.object),
    originalValues: PropTypes.arrayOf(PropTypes.object),
    rowValues: PropTypes.arrayOf(PropTypes.object),
    drivers: PropTypes.arrayOf(PropTypes.object),
    vehicles: PropTypes.arrayOf(PropTypes.object),
    errors: PropTypes.arrayOf(PropTypes.string),
    invalidFields: PropTypes.arrayOf(PropTypes.string),
    successFlag: PropTypes.bool,
    onLoadVehicleAssignmentData: PropTypes.func,
    onLoadDrivers: PropTypes.func,
    onLoadVehicles: PropTypes.func,
    onAddRow: PropTypes.func,
    onDeleteRow: PropTypes.func,
    onRevertData: PropTypes.func,
    onUpdateDriver: PropTypes.func,
    onUpdateVehicle: PropTypes.func,
    onUpdateStatus: PropTypes.func,
    onUpdateErrors: PropTypes.func,
    onSubmitData: PropTypes.func,
    onUpdateSuccessFlag: PropTypes.func,
    onResetState: PropTypes.func,
};
// ======================

// ===== INJECTIONS =====
const mapStateToProps = createStructuredSelector({
    data: selectData,
    originalValues: selectOriginalValues,
    rowValues: selectRowValues,
    drivers: selectDrivers,
    errors: selectErrors,
    invalidFields: selectInvalidFields,
    successFlag: selectSuccessFlag,
    vehicles: selectVehicles,
});

const mapDispatchToProps = dispatch => ({
    onLoadVehicleAssignmentData: () => dispatch(loadVehicleAssignmentData()),
    onLoadDrivers: () => dispatch(loadDriverData()),
    onLoadVehicles: () => dispatch(loadVehicleData()),
    onAddRow: () => dispatch(addRow()),
    onDeleteRow: id => dispatch(deleteRow(id)),
    onRevertData: () => dispatch(revertData()),
    onUpdateDriver: (rowId, value) => dispatch(updateDriver(rowId, value)),
    onUpdateVehicle: (rowId, value) => dispatch(updateVehicle(rowId, value)),
    onUpdateStatus: (rowId, name, value) => dispatch(updateStatus(rowId, name, value)),
    onUpdateErrors: (errors, invalidFields) => dispatch(updateErrors(errors, invalidFields)),
    onSubmitData: data => dispatch(submitVehicleAssignmentData(data)),
    onUpdateSuccessFlag: bool => dispatch(updateSuccessFlag(bool)),
    onResetState: () => dispatch(resetState()),
});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

const withReducer = injectReducer({ key: VEHICLE_ASSIGNMENT_DOMAIN, reducer });
const withSaga = injectSaga({ key: VEHICLE_ASSIGNMENT_DOMAIN, saga });
// ======================

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(injectIntl(withRouter(VehicleAssignmentTable)));

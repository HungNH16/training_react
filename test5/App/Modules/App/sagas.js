import {
  takeLatest,
  call,
  put,
} from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { NavigationActions } from 'react-navigation';
// Services
import AuthApi from '../../Services/Auth';
// Actions
import { AppTypes } from './reducer';

function* bootStrapApp() {
  const user = yield call(AuthApi.getUser);
  let routeName = 'LoginScreen';

  yield call(delay, 1000);

  if (user) {
    routeName = 'MainScreen';
  }

  yield put(NavigationActions.navigate({ routeName }));
}

export default [
  takeLatest(AppTypes.BOOT_STRAP_APP, bootStrapApp),
];

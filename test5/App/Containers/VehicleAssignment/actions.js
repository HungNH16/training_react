/**
 *
 * Vehicle Assignment Actions
 *
 * This contains Redux action for Vehicle Assignment screen
 */
// ===== IMPORTS =====
// Utilities, Constants & Styles
import {
    ADD_ROW,
    DELETE_ROW,
    LOAD_DATA,
    LOAD_DATA_SUCCESS,
    LOAD_DRIVER,
    LOAD_DRIVER_SUCCESS,
    LOAD_VEHICLE,
    LOAD_VEHICLE_SUCCESS,
    RESET_STATE,
    REVERT_DATA,
    SUBMIT_DATA,
    SUBMIT_DATA_SUCCESS,
    UPDATE_DRIVER,
    UPDATE_ERRORS,
    UPDATE_STATUS,
    UPDATE_SUCCESS_FLAG,
    UPDATE_VEHICLE,
} from './constants';
// ===================

// ===== ACTIONS =====
export const addRow = () => ({
    type: ADD_ROW,
});

export const deleteRow = id => ({
    type: DELETE_ROW,
    id,
});

export const loadVehicleAssignmentData = () => ({
    type: LOAD_DATA,
});

export const loadVehicleAssignmentDataSuccess = response => ({
    type: LOAD_DATA_SUCCESS,
    response,
});

export const loadDriverData = () => ({
    type: LOAD_DRIVER,
});

export const loadDriverDataSuccess = response => ({
    type: LOAD_DRIVER_SUCCESS,
    response,
});

export const loadVehicleData = () => ({
    type: LOAD_VEHICLE,
});

export const loadVehicleDataSuccess = response => ({
    type: LOAD_VEHICLE_SUCCESS,
    response,
});

export const resetState = () => ({
    type: RESET_STATE,
});

export const revertData = () => ({
    type: REVERT_DATA,
});

export const submitVehicleAssignmentData = data => ({
    type: SUBMIT_DATA,
    data,
});

export const submitVehicleAssignmentDataSuccess = response => ({
    type: SUBMIT_DATA_SUCCESS,
    response,
});

export const updateDriver = (rowId, value) => ({
    type: UPDATE_DRIVER,
    rowId,
    value,
});

export const updateErrors = (errors, invalidFields) => ({
    type: UPDATE_ERRORS,
    errors,
    invalidFields,
});

export const updateStatus = (rowId, name, value) => ({
    type: UPDATE_STATUS,
    rowId,
    name,
    value,
});

export const updateSuccessFlag = bool => ({
    type: UPDATE_SUCCESS_FLAG,
    bool,
});

export const updateVehicle = (rowId, value) => ({
    type: UPDATE_VEHICLE,
    rowId,
    value,
});
// ===================

# Bước 1: Chia UI thành component theo bậc
Nên phân chia cấu trúc của component và các subcomponent như sau:

* FilterableProductTable
            
            SearchBar
            ProductTable
                 ProductCategoryRow
                 ProductRow
# Bước 2: Xác định mức tối thiểu (nhưng hoàn chỉnh)

Để UI của bạn tương tác tốt, bạn cần xác định các thay đổi đến dữ liệu thuộc tầng dưới (underlying data model) . Hãy sử dụng state của React.

Nên sử dụng state cho:
* Search text mà user nhập vào
* Giá trị của checkbox khi user sử dụng
# Bước 3:Xác định nơi sinh sống của state
Hãy nhớ rằng, React là luồng dữ liệu một chiều, phụ thuộc theo luồng của các cấp componen.

Để hình dung ra một cách rõ ràng, hãy theo các bước sau:

* Phân loại mỗi component sẽ render ra những gì dựa vào state đó.
* Tìm thử trong component cha
* Nếu component cha không có, tìm tiếp ở component cao hơn component cha đó
* Nếu bạn không tìm ra được component đang giữ state, thử tạo một conponent mới để giữ state đó và thêm nó ở đâu đó trong cấp thư mục cao hơn thư mục chứa component cha.
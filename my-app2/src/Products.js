import React from 'react';
import SearchBar from './SearchBar';
import ProductTable from './ProductTable';
class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.state.filterText = "";
        this.state.products = [
            {
                id: 1,
                category: 'sport',
                price: "500",
                qty: "12",
                name: 'football',
            }, {
                id: 2,
                category: 'sport',
                price: "300",
                qty: "15",
                name: 'baseball',
            }, {
                id: 3,
                category: 'sport',
                price: "100",
                qty: "14",
                name: 'basketball',
            }, {
                id: 4,
                category: 'electronics',
                price: "400",
                qty: "35",
                name: 'iphone7',
            }, {
                id: 5,
                category: 'electronics',
                price: "200",
                qty: "26",
                name: 'ipod',
            }
        ];
    }
    handleUserInput(filterText) {
        this.setState({ filterText: filterText });
    };
    handleRowDel(product) {
        var index = this.state.products.indexOf(product);
        this.state.products.splice(index, 1);
        this.setState(this.state.products);
    };

    handleAddEvent(evt) {
        var id = (+ new Date() + Math.floor(Math.random() * 999999)).toString(36);
        var product = {
            id: id,
            name: "",
            price: "",
            category: "",
            qty: 0
        }
        this.state.products.push(product);
        this.setState(this.state.products);

    }

    handleProductTable(evt) {
        var item = {
            id: evt.target.id,
            name: evt.target.name,
            value: evt.target.value
        };
        var products = this.state.products.slice();
        var newProducts = products.map(function (product) {

            for (var key in product) {
                if (key === item.name && product.id === item.id) {
                    product[key] = item.value;

                }
            }
            return product;
        });
        this.setState({ products: newProducts });
    };
    render() {

        return (
            <div >
                <SearchBar
                    filterText={this.state.filterText}
                    onUserInput={this.handleUserInput.bind(this)} />
                <ProductTable
                    onProductTableUpdate={this.handleProductTable.bind(this)}
                    onRowAdd={this.handleAddEvent.bind(this)}
                    onRowDel={this.handleRowDel.bind(this)}
                    products={this.state.products}
                    filterText={this.state.filterText} />
            </div>
        );

    }

}
export default Product;

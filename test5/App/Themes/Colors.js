const colors = {
  background: '#0D1D2D',
  black: '#000000',
  blue: '#497ff8',
  darkBlue: '#004980',
  lavender: '#edf0ff',
  main: '#FF9B00',
  midNight: '#233042',
  transparent: 'rgba(0,0,0,0)',
  text: '#E0D7E5',
  shuttleGrey: '#59626a',
  white: '#ffffff',
  lightBlue: '#e1eaf3',
  lightShuttleGrey: '#f5f5f5',
};

export default colors;

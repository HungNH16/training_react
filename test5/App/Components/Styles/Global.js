import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
    html {
        height: 100%;
        width: 100%;
        body {
            // Common styles
            height: 100%;
            width: 100%;
            font-family: Ubuntu;
            &.fontLoaded {
                font-family: Ubuntu;
            }
            #app {
                background-color: #fafafa;
                min-height: 100%;
                min-width: 100%;
                .action-icon .wrap-icon {
                    margin-left: 10px;
                    i {
                        font-size: 12px !important;
                        position: absolute;
                        top: 7px !important;
                        left: 7px !important;
                        text-align: center;
                        &.fa-times {
                            left: 9px !important;
                        }
                    }
                }
            }
            p, label {
                font-family: Ubuntu;
                line-height: 1.5em;
            }
            .form-control {
                border-radius: 0px;
                height: 40px;
                &.is-invalid {
                    background: none;
                    background-color: #ffffff;
                    padding-right: .75rem;
                }
            }
            
            .pagination .pagination-info {
                cursor: default;
            }
            h3 {
                font-size: 24px;
                color: #111;
            }
            button {
                text-transform: uppercase !important;
            }

            // Form Pop-up
            .modal-dialog {
                input,
                select {
                    font-size: 16px;
                    height: 40px;
                    border-radius: 0;
                }
                .cf-select__control {
                    border-radius: 0;
                    &:hover {
                        border-color: #ced4da;
                        box-shadow: none;
                    }
                    &.cf-select__control--is-focused,
                    &.cf-select__control--is-focused:hover {
                        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
                        border-color: #80bdff;
                    }
                    .cf-select__value-container:not(.cf-select__value-container--is-multi) {
                        height: 40px;
                        position: initial;
                        display: block;
                    }
                }
                .cf-select__menu {
                    z-index: 2;
                }
            
                .btn-icon {
                    display: inline-block;
                    position: relative;
                    text-align: center;
                    vertical-align: middle;
                    &.btn-icon--small {
                        font-size: 13px;
                        &:hover {
                            cursor: pointer;
                        }
                        .icon {
                            display: inline-block;
                            width: 22px;
                            height: 22px;
                            border: 2px solid;
                            border-radius: 50%;
                        }
                    }
                }
            }

            // Custom React Table
            .ReactTable {
                .rt-thead {
                    color: #111;
                }
                .rt-tbody .rt-td {
                    color: #111;
            
                    &.cf-table__cell--wrap {
                        white-space: initial !important;
                    }
                    &.cf-table__cell--visible {
                        overflow: visible !important;
                        white-space: initial !important;
                    }
                }
            }

            // Table toolbar
            .table-toolbar-button {
                display: inline-block;
                position: relative;
                text-align: center;
                vertical-align: middle;
                font-size: 13px;
                &:hover {
                    cursor: pointer;
                }
                .icon {
                    display: inline-block;
                    width: 22px;
                    height: 22px;
                    border: 2px solid;
                    border-radius: 50%;
                }
            }
            
            // Custom select input for Dispatcher Platform
            .cf-select__control {
                border-radius: 0 !important;
                &:hover {
                    border-color: #ced4da;
                    box-shadow: none;
                }
                &.cf-select__control--is-focused,
                &.cf-select__control--is-focused:hover {
                    box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
                    border-color: #80bdff;
                }
                .cf-select__value-container:not(.cf-select__value-container--is-multi) {
                    height: 40px;
                    position: initial;
                    display: block;
                    .cf-select__single-value {
                        width: calc(100% - 50px);
                    }
                }
                .cf-select__value-container.cf-select__value-container--is-multi {
                    min-height: 40px;
                }
            }
            .cf-select--is-invalid .cf-select__control {
                border-color: #dc3545;
                &.cf-select__control--is-focused,
                &.cf-select__control--is-focused:hover {
                    border-color: #dc3545;
                    box-shadow: 0 0 0 0.2rem rgba(220, 53, 69, 0.25);
                }
                &:hover {
                    border-color: #dc3545;
                    box-shadow: none;
                }
            }
        }
    }
`;

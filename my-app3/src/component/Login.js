import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom';
import { login } from '../action/Action'
import { connect } from 'react-redux';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: false
        };
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        if (this.refs.user.value === 'hungnh16' && this.refs.pass.value === '123456') {
            this.setState({
                error: false
            })
            this.props.handleLogin(this.refs.user.value)
        } else {
            this.setState({
                error: true
            })
        }
    }
    render() {
        const { isLogin } = this.props
        const { error } = this.state
        return (
            <div>
                {error ? <h4>Wrong User</h4> : ""}
                {!isLogin ?
                    <div>
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            User:
                            <input type='text' placeholder='Username' ref='user' required />
                            <br />
                            Pass:
                            <input type='password' placeholder='Password' ref='pass' required />
                            <br />
                            <input type='submit' value='Login' />
                            <Link to='/'><span style={{ marginLeft: 10 }}>Cancel</span></Link>
                        </form>
                        <br />
                    </div> : <Redirect to='/' />}
            </div>
        )
    }
}
export default connect(
    (state) => {
        return {
            isLogin: state.isLogin
        }
    },
    (dispatch) => {
        return {
            handleLogin: (user) => dispatch(login(user))
        }
    }
)(Login);




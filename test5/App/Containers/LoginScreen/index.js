import React, { Component } from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Image,
  Text,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Components
import Input from '../../Components/Input';
import Button from '../../Components/Button';
import Checkbox from '../../Components/Checkbox';
import LinkTo from '../../Components/LinkTo';
import PhoneInput from '../../Components/PhoneInput';
// Actions
import AuthActions from '../../Modules/Auth/reducer';
import { getError } from '../../Modules/Auth/selectors';
// Images, Fonts
import { Images } from '../../Themes';
// Styles
import styles from './styles';

class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: '',
      password: '',
      areaCode: '+84',
    };
    this.makeOnInputChange = this.makeOnInputChange.bind(this);
    this.onUserNameChange = this.makeOnInputChange('userName').bind(this);
    this.onPasswordChange = this.makeOnInputChange('password').bind(this);
    this.onLoginBtnPressed = this.onLoginBtnPressed.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ password: '' });
    }
  }

  onLoginBtnPressed() {
    const { dispatchLogin } = this.props;
    const { userName, areaCode, password } = this.state;

    dispatchLogin({ areaCode, login: userName, password });
  }

  makeOnInputChange(type) {
    switch (type) {
      case 'userName':
        return ({ areaCode, value }) => this.setState({ userName: value, areaCode });
      default:
        return value => this.setState({ password: value });
    }
  }

  render() {
    const { password, userName } = this.state;
    const { error } = this.props;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} style={styles.mainContainer}>
        <View style={styles.centerContainer}>
          <View style={styles.logoContainer}>
            <Image source={Images.launch} style={styles.logo} />
          </View>
          {error ? (<Text style={styles.error}>{error}</Text>) : null}
          <View style={[styles.container, styles.inputContainer]}>
            <PhoneInput
              onChangeText={this.onUserNameChange}
              placeholder="Phone number"
              value={userName}
            />
            <Input
              onChangeText={this.onPasswordChange}
              placeholder="Password"
              value={password}
              secureTextEntry
            />
          </View>
          <View style={[styles.container, styles.buttonContainer]}>
            <Button onPress={this.onLoginBtnPressed} value="LOGIN" />
          </View>
          <View style={[styles.container, styles.rememberMeContainer]}>
            <Checkbox
              name="remember"
              label="Remember me"
            />
            <Text style={styles.seperator}>|</Text>
            <LinkTo style={{ paddingLeft: 10 }} value="Forgot password" />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

LoginScreen.defaultProps = {
  error: null,
};

LoginScreen.propTypes = {
  dispatchLogin: PropTypes.func.isRequired,
  error: PropTypes.string,
};

const mapStateToProps = state => ({
  error: getError(state),
  disableReduxShallow: new Date().getTime(),
});

const mapDispatchToProps = dispatch => ({
  dispatchLogin: payload => dispatch(AuthActions.login(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

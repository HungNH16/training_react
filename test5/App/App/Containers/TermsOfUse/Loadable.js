/**
 *
 * Asynchronously loads the component for Terms
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));

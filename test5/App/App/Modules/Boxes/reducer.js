import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------------Types and Action Creators-----------------*/

const { Types, Creators } = createActions({
  setBoxes: ['payload'],
  setIsFetchingBoxes: ['payload'],
  loadBoxes: ['payload'],
});

export const BoxesTypes = Types;
export default Creators;

/* ------------------Initial State-----------------*/

export const INITIAL_STATE = Immutable({
  Boxes: [],
  isFetchingBoxes: true,
});

/* ------------------Reducers-----------------*/

// eslint-disable-next-line no-undef
const setBoxes = (state, { payload: Boxes }) => state.merge({ Boxes });
const setFetchingBoxes = (state, { payload: isFetchingBoxes }) => state.merge({ isFetchingBoxes });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_BOXES]: setBoxes,
  [Types.SET_IS_FETCHING_BOXES]: setFetchingBoxes,
});

import { put, takeLatest, call } from 'redux-saga/effects';
// Actions, reducers
import TourActions, { TourTypes } from './reducer';
// Services
import Tour1Api from '../../Services/DetailTour/index';
import Logger from '../../Services/Logger';

function* loadtour1() {
  try {
    yield put(TourActions.setIsFetchingTour1(true));
    const tour1 = yield call(Tour1Api.fetchTour1);

    yield put(TourActions.setTour1(tour1));
  } catch (error) {
    Logger.log(error);
  } finally {
    yield put(TourActions.setIsFetchingTour1(false));
  }
}

export default [
  takeLatest(TourTypes.LOAD_TOUR1, loadtour1),
];

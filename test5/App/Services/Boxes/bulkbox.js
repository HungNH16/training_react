export const fetchBoxes = () => ([
  {
    name: 'Bulk Box 1',
    text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
  },
  {
    name: 'Bulk Box 2',
    text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
  },
  {
    name: 'Bulk Box 3',
    text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
  },
  {
    name: 'Bulk Box 4',
    text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
  },
  {
    name: 'Bulk Box 5',
    text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
  },
  {
    name: 'Bulk Box 6',
    text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
  },
  {
    name: 'Bulk Box 7',
    text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
  },
  {
    name: 'Bulk Box',
    text: 'Lorem ipsum dolor sit amed sed do eius LxWxH 1,5kg',
  },
]);

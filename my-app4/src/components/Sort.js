import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';
import * as actions from '../actions/index';
class Sort extends Component {

    onClick = (sortBy, sortValue) => {
        this.props.onSort({
            by: sortBy,
            value: sortValue
        });
    }

    render() {
        return (
            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div className="dropdown" >
                    <Button
                        className="btn btn-primary dropdown-toggle"
                        type="button"
                        id="dropdownMenu1"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        Sắp xếp<span className="fa fa-caret-square-o-down ml-5"></span>
                    </Button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li onClick={() => this.onClick('name', 1)}>
                            <a role="button" className={(this.props.sort.by=== 'name' && this.props.sort.value === 1) ? 'sort_selected' : ''} href='#'>
                                <span className = "pr-5">
                                    <i className="fas fa-sort-alpha-down"></i>
                                    Tên A-Z
                               </span>
                            </a>
                        </li>
                        <li onClick={() => this.onClick('name', -1)}>
                            <a role="button" className={(this.props.sort.by=== 'name' && this.props.sort.value === -1) ? 'sort_selected' : ''} href='#'>
                                <span>
                                <i className="fas fa-sort-alpha-up"></i>
                                    Tên Z-A
                                </span>
                            </a>
                        </li>
                        <li role="separator" className="divider"></li>
                        <li onClick={() => this.onClick('status', 1)} className={"dropdown-item"}>
                            <a role="button" className={(this.props.sort.by === 'status' && this.props.sort.value === 1) ? 'sort_selected' : ''} href='#'>
                                <span>Trạng thái kích hoạt</span>
                            </a>
                        </li>
                        <li onClick={() => this.onClick('status', -1)} className={"dropdown-item"}>
                            <a role="button" className={(this.props.sort.by === 'status' && this.props.sort.value === -1) ? 'sort_selected' : ''} href='#'>
                                Trạng thái ẩn
                            </a>
                        </li>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        sort : state.sort
    };
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onSort: (sort) => {
            dispatch(actions.sortTask(sort))
        }     
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Sort);
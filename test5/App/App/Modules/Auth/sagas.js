import { put, takeLatest, call } from 'redux-saga/effects';
import { NavigationActions } from 'react-navigation';
import Ramda from 'ramda';
// Actions, reducers
import AuthActions, { AuthTypes } from './reducer';
import AppActions from '../App/reducer';
// Services
import AuthApi from '../../Services/Auth';
import Logger from '../../Services/Logger';
import Validator from '../../Services/Validator';
// Schema
import { loginSchema } from './schemas';

function* login({ payload }) {
  try {
    yield put(AppActions.setLoading(true));
    const { areaCode, login: phoneNumber, password } = payload;
    const validatorErrors = Validator(payload, loginSchema);

    if (!Ramda.isEmpty(validatorErrors)) {
      let errorMessage;

      switch (true) {
        case !!(validatorErrors.login && validatorErrors.password):
          errorMessage = 'Please enter your phonenumber and password.';
          break;
        case !!(validatorErrors.login):
          errorMessage = 'Please enter your phonenumber.';
          break;
        default:
          errorMessage = 'Please enter your password.';
      }

      const error = new Error(errorMessage);
      error.isCustomError = true;

      throw error;
    }

    const { token } = yield call(AuthApi.authenticateUser, `${areaCode}${phoneNumber}`, password);
    yield call(AuthApi.setUserToken, token);
    yield put(NavigationActions.navigate({ routeName: 'MainScreen' }));
  } catch (error) {
    if (error.isCustomError) {
      yield put(AuthActions.setError(error.message));
    } else {
      yield put(AuthActions.setError('Invalid username or password. Please try again.'));
    }

    Logger.log(error);
  } finally {
    yield put(AppActions.setLoading(false));
  }
}

export default [
  takeLatest(AuthTypes.LOGIN, login),
];

import { StyleSheet } from 'react-native';
// Theme: Metrics...
import { Metrics } from '../../Themes';

export default StyleSheet.create({
  container: {
    width: Metrics.screenWidth,
  },
  tabContainer: {
    width: Metrics.screenWidth,
    flexDirection: 'row',
  },
});

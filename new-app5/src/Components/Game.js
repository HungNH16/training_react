import React from 'react';
import Board from './Board';
import '../index.css';
class Game extends React.Component {
    constructor() {
        super();
        this.state = {
            history:[{
            squares: Array(9).fill(null),
            moveLocation: '',
        }],//Thêm mảng history vào,history lưu object squares
            xIsNext: true,
            stepNumber: 0, //Khởi tạo
        };
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(i) {
        // //var newSquares = this.state.squares.slice();
        // var s = [...this.state.squares]; //Luu y
        // s[i] = 'X'
        // this.setState({
        //     squares: s
        // })
        // console.log(this.state.squares)   
        const history =  this.state.history.slice(0,this.state.stepNumber +1);  //clone history ra bản phụ tránh làm ảnh hưởng bản chính
        const current = history[history.length - 1]; //Lấy history lần gần nhất
        const squares = current.squares.slice(); //clone object squares của current 
        if(calculateWinner(squares) || squares[i]){
            return;
        }
        const matrixSize = Math.sqrt(history[0].squares.length);
        const moveLocation = [Math.floor(i / matrixSize) + 1,(i % matrixSize) +1].join(", ");
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{  //thêm history mỗi khi click vào ô trống
                squares: squares,
                moveLocation: moveLocation,
            }]),
            xIsNext: !this.state.xIsNext,
            stepNumber: history.length //Vì stepNumber bằng vs độ dài của history
        });
    }
    jumpTo(move){
        this.setState({
            stepNumber: move,
            xIsNext: (move % 2) ? false : true,
        });
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const squares = current.squares;
        const winner = calculateWinner(squares);
        let status;
        if(winner){
            status = "Winner is: " + winner; //Winner có giá trị thì sẽ hiển thị người thắng
        }else if(this.state.stepNumber === 9){
            status = "Hòa";
        }else{
            status = "Next player is: " + (this.state.xIsNext ? 'X' : 'O');
        }
        const moves = history.map((step, move) => {
            const description = move ? `Move #${move} (${step.moveLocation})` : 'Game start'; // move = 0 là lúc game mới start.
            return <li key={move}><a href="#" onClick={() => this.jumpTo(move)}>{description}</a></li>
          });
        return (
            <div>
                <div className="game"><Board squares={squares} onClick={i => this.handleClick(i)} /></div>
                <div className="game-info">
                    <p>{status}</p>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}
function calculateWinner(squares){
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for(let i=0;i < lines.length;i++){
        const [a, b, c] = lines[i];
        if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]){
            return squares[a];
        }
    }
    return null;
}

export default Game;
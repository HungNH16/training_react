import { combineReducers } from 'redux';
import products from './products';
import ItemEdit from './ItemEdit';

const myReducer = combineReducers({
    products,
    ItemEdit
});
 export default myReducer;
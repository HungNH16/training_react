import { fromJS } from 'immutable';
import { initialState } from '../reducer';
import { selectVehicleAssignmentDomain, selectData, selectDrivers, selectErrors, selectInvalidFields, selectOriginalValues, selectRowValues, selectSuccessFlag, selectVehicles } from '../selectors';
import { VEHICLE_ASSIGNMENT_DOMAIN } from '../constants';

describe('Select vehicle assignment domain', () => {
    it('expect to have selectVehicleAssignmentDomain', () => {
        const mockedState = fromJS({
            [VEHICLE_ASSIGNMENT_DOMAIN]: initialState,
        });
        expect(selectVehicleAssignmentDomain(mockedState)).toEqual(initialState);
    });
});

describe('Select table data', () => {
    it('expect to have selectData', () => {
        expect(selectData(initialState)).toEqual(initialState.toJS().data);
    });
});

describe('Select driver options', () => {
    it('expect to have selectDrivers', () => {
        expect(selectDrivers(initialState)).toEqual(initialState.toJS().drivers);
    });
});

describe('Select error messages', () => {
    it('expect to have selectErrors', () => {
        expect(selectErrors(initialState)).toEqual(initialState.toJS().errors);
    });
});

describe('Select invalid fields', () => {
    it('expect to have selectInvalidFields', () => {
        expect(selectInvalidFields(initialState)).toEqual(initialState.toJS().invalidFields);
    });
});

describe('Select original editing values', () => {
    it('expect to have selectOriginalValues', () => {
        expect(selectOriginalValues(initialState)).toEqual(initialState.toJS().originalValues);
    });
});

describe('Select current editing values', () => {
    it('expect to have selectRowValues', () => {
        expect(selectRowValues(initialState)).toEqual(initialState.toJS().rowValues);
    });
});

describe('Select pop-up success flag', () => {
    it('expect to have selectSuccessFlag', () => {
        expect(selectSuccessFlag(initialState)).toEqual(initialState.toJS().successFlag);
    });
});

describe('Select vehicle options', () => {
    it('expect to have selectVehicles', () => {
        expect(selectVehicles(initialState)).toEqual(initialState.toJS().vehicles);
    });
});

import React, { Component } from 'react';
import TaskItem from './TaskItem';
import { connect } from 'react-redux';
import * as actions from './../actions/index';
class TaskList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterName: '',
            filterStatus: -1
        }
    }
    onChange = (event) => {
        var target = event.target;
        var name = target.name;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        var filter = {
            name: name === 'filterName' ? value : this.state.filterName,
            status: name === 'filterStatus' ? value : this.state.filterStatus
        };
        this.props.onFilterTable(filter);
        this.setState({
            [name]: value
        });
    }
    render() {
        var { tasks, filterTable, keyword, sort} = this.props;//var tasks = this.props.tasks
        if (filterTable.name) {
            tasks = tasks.filter((tasks) => {
                return tasks.name.toLowerCase().indexOf(filterTable.name) !== -1;
            });
        }
        tasks = tasks.filter((tasks) => {
            if (filterTable.filterStatus === -1) {
                return tasks;
            } else {
                return tasks.status === (filterTable.filterStatus === 1 ? true : false)
            }
        });
        //Search
            tasks = tasks.filter((tasks) => {
                return tasks.name.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
            });
        //Sort
               
        if (sort.by === 'name') {
            tasks.sort((a, b) => {
                if (a.name > b.name) return sort.value;
                else if (a.name < b.name) return -sort.value;
                else return 0;
            });
        }
        else {
            tasks.sort((a, b) => {
                if (a.status > b.status) return -sort.value;
                else if (a.status < b.status) return sort.value;
                else return 0;
            });
        }
        var elm = tasks.map((tasks, index) => {
            return <TaskItem
                key={tasks.id}
                index={index}
                tasks={tasks}
            />
        });
        return (
            <div className="row mt-15">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table className="table table-bordered table-hover mt-15">
                        <thead>
                            <tr>
                                <th className="text-center">STT</th>
                                <th className="text-center">Tên</th>
                                <th className="text-center">Trạng Thái</th>
                                <th className="text-center">Hành Động</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="filterName"
                                        value={this.state.filterName}
                                        onChange={this.onChange}
                                    >
                                    </input>
                                </td>
                                <td>
                                    <select
                                        className="form-control"
                                        name="filterStatus"
                                        value={this.state.filterStatus}
                                        onChange={this.onChange}
                                    >
                                        <option value={-1}>Tất Cả</option>
                                        <option value={0}>Ẩn</option>
                                        <option value={1}>Kích Hoạt</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            {
                                tasks &&
                                elm
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        tasks: state.tasks,
        filterTable: state.filterTable,
         keyword: state.search,
         sort : state.sort
    }
};
const mapDispatchToProps = (dispatch, props) => {
    return {
        onFilterTable: (filter) => {
            dispatch(actions.filterTask(filter));
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
import { StyleSheet } from 'react-native';
import { ApplicationStyles, Fonts } from '../../Themes';

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    ...ApplicationStyles.screen.container,
    paddingHorizontal: 20,
  },
  logoContainer: {
    alignItems: 'center',
  },
  inputContainer: {
    paddingTop: 20,
    height: 115,
    justifyContent: 'space-between',
  },
  buttonContainer: {
    paddingTop: 15,
  },
  rememberMeContainer: {
    paddingTop: 15,
    paddingBottom: 20,
    flexDirection: 'row',
  },
  seperator: {
    color: '#ffffff',
    paddingLeft: 10,
  },
  error: {
    color: 'red',
    fontSize: Fonts.size.medium,
    textAlign: 'center',
  },
});

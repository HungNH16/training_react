import React, { Component } from 'react';
import { CardGroup, Card, CardImg, CardTitle, CardText, Button, CardBody, CardLink, Collapse }
    from 'reactstrap';
class Card extends Component {
    constructor(props) {
        super(props);
        this.onEntering = this.onEntering.bind(this);
        this.onEntered = this.onEntered.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
        this.toggle = this.toggle.bind(this);
        this.state = {
            Collapse: false, status: 'Closed'
        };
    }
    onEntering = () => {
        this.setState({ status: 'Opening...' })
    }

    onEntered = () => {
        this.setState({ status: 'Opened' })
    }

    onExiting = () => {
        this.setState({ status: 'Closing...' })
    }

    onExited = () => {
        this.setState({ status: 'Closed' })
    }

    toggle = () => {
        this.setState(state => ({ Collapse: !state.Collapse }))
    }
    render() {
        return (
            <div>
                <Button color="primary" onClick={this.toggle}>toggle</Button>
                <h5>Current state: {this.state.status}</h5>
                <Collapse
                    isOpen={this.state.Collapse}
                    onEntering={this.onEntering}
                    onEntered={this.onEntered}
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                >
                    <CardGroup>
                        <Card>
                            <CardImg top width="10%" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa1d%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa1d%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22285.921875%22%20y%3D%22218.3%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" />
                            <CardBody>
                                <CardTitle>Card Title</CardTitle>
                                <CardText>Card Text</CardText>
                                <Button>Button</Button>
                                <CardLink>Card Link</CardLink>
                            </CardBody>
                        </Card>
                    </CardGroup>
                </Collapse>
            </div>
        );
    }
}

export default Card;
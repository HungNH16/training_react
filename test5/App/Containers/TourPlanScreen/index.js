import React, { Component } from 'react';
import { Text, FlatList, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Components
import TabList from '../../Components/TabList';
import TourItem from '../../Components/TourItem';
import TourItemFooter from '../../Components/TourItemFooter';
// Actions
import TourActions from '../../Modules/Tour/reducer';
// Selectors
import { getIsFetchingTour, getTour } from '../../Modules/Tour/selectors';
// Styles
import styles from './styles';

const fakeDataSource = Array(10).fill('123');

class TourPlanScreen extends Component {
  componentDidMount() {
    const { fetchTourPlan } = this.props;

    fetchTourPlan('fakeID');
  }

  render() {
    const { tour } = this.props;

    return (
      <TabList tabs={{
        today: {
          label: 'TODAY',
          render: () => (
            <FlatList
              style={styles.container}
              data={tour}
              keyExtractor={(item, index) => `${index}`}
              ListHeaderComponent={() => <Text style={styles.listHeader}>Thur 17, Jan 2019</Text>}
              ListFooterComponent={() => <TourItemFooter />}
              renderItem={({ item }) => (<TourItem {...item} />)}
            />
          ),
        },
        scheduled: {
          label: 'SCHEDULED',
          render: () => (
            <FlatList
              style={styles.container}
              data={tour.filter(location => !location.passed)}
              keyExtractor={(item, index) => `${index}`}
              ListHeaderComponent={() => <Text style={styles.listHeader}>Thur 17, Jan 2019</Text>}
              ListFooterComponent={() => <TourItemFooter />}
              renderItem={({ item }) => (<TourItem {...item} />)}
            />
          ),
        },
      }}
      />
    );
  }
}

TourPlanScreen.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  tour: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  isFetching: getIsFetchingTour(state),
  tour: getTour(state),
});

const mapDispatchToProps = dispatch => ({
  fetchTourPlan: id => dispatch(TourActions.fetchTour(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TourPlanScreen);

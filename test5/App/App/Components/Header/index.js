// ===== IMPORTS =====
// Modules
import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { CardImg, DropdownItem, DropdownMenu, DropdownToggle, Navbar, Nav, NavItem, NavLink, UncontrolledDropdown, NavbarBrand } from 'reactstrap';
import _ from 'lodash';
// Constants & Styles
import NavbarStyle from './NavBar';
import { compareUrl } from '../../utils/miscellaneous';
import MENU from '../../mockData/menu';
import urgentIcon from '../../assets/images/icons/urgent_notifications.png';
import normalIcon from '../../assets/images/icons/normal_notifications.png';
import { getUser } from '../../../services/auth';

import fullLogo from '../../assets/images/logo/CF_Logo.png';
// ===================

// ===== MAIN COMPONENT =====
class Header extends React.Component {
    constructor(props) {
        super(props);

        this.clearToken = this.clearToken.bind(this);
        this.onShowSidebar = this.onShowSidebar.bind(this);
    }

    clearToken() {
        const {
            history: { push },
        } = this.props;
        localStorage.removeItem('access_token');
        localStorage.removeItem('signoutTime');

        push('/');
    }

    onShowSidebar() {
        document.getElementById('mobileSidebar').style.width = '80%';
        document.getElementById('sidebarShadow').style.visibility = 'visible';
    }

    render() {
        const activeUrl = this.props.location.pathname;
        const userInfo = getUser();
        const userName = userInfo.email.split('@')[0];
        // -------------------------------------
        const subMenu = _.get(MENU[userInfo.role].find(main => compareUrl(main.url, activeUrl)), 'subMenu', []);
        return (
            <NavbarStyle>
                <Navbar expand className="navbar-desktop d-none d-lg-flex">
                    <Nav tabs className="mt-auto">
                        {subMenu.map((sub, subIdx) => (
                            <NavItem key={`subMenu_${subIdx + 1}`} className={sub.url === activeUrl ? 'active' : ''}>
                                <NavLink>
                                    <Link to={sub.url}>{sub.label}</Link>
                                </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink>
                                <CardImg src={urgentIcon} />
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink>
                                <CardImg src={normalIcon} />
                            </NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                <i className="far fa-user" />
                                {userName}
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>Change Password</DropdownItem>
                                <DropdownItem onClick={this.clearToken}>Log out</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                </Navbar>
                <Navbar expand className="navbar-mobile d-lg-none">
                    <NavbarBrand onClick={this.onShowSidebar}>
                        <i className="fas fa-bars" />
                    </NavbarBrand>
                    <CardImg src={fullLogo} className="logo" />
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink>
                                <CardImg src={urgentIcon} />
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink>
                                <CardImg src={normalIcon} />
                            </NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>
            </NavbarStyle>
        );
    }
}
// ==========================

// ===== PROP TYPES =====
Header.propTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
};
// ======================

export default withRouter(Header);

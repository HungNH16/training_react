/*
 *
 * Vehicle reducer
 *
 */

import { fromJS } from 'immutable';
import { get as _get } from 'lodash';
import queryString from 'query-string';
import {
    ADD_VEHICLE,
    ADD_VEHICLE_SUCCESS,
    UPDATE_INPUT,
    UPDATE_ERRORS,
    RESET_FORM,
    UPDATE_SUCCESS_FLAG,
    ADD_VEHICLE_FAILED,
    LOAD_VEHICLE_REQUEST_ACTION,
    LOAD_VEHICLE_SUCCESS_ACTION,
    RESET_STATE,
    LOAD_MAKE_MODEL_SUCCESS_ACTION,
} from './constants';

export const initialForm = {
    license: '',
    type: '',
    length: '',
    width: '',
    height: '',
    weight: '',
    status: '',
    maximumWorkingHours: '',
    tags: '',
    characteristics: [],
};

export const initialState = fromJS({
    vehicles: [],
    typeOptions: [],
    pagination: {
        perPage: 10,
        total: 0,
        currentPage: 0,
    },
    addSuccessFlag: false,
    formValues: fromJS(initialForm),
    invalidFields: [],
    errors: [],
});

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_VEHICLE:
            return state.set('addSuccessFlag', false);
        case ADD_VEHICLE_SUCCESS:
            if (!_get(action, 'response.data.licensePlate')) {
                return state.set('errors', ['Error ....']);
            }
            return state.set('addSuccessFlag', !!_get(action, 'response.data.licensePlate'));
        case ADD_VEHICLE_FAILED:
            return state.set('errors', action.errors).set('invalidFields', action.invalidFields);
        case UPDATE_INPUT: {
            if (action.name === 'type') {
                const typeOptions = state.get('typeOptions');
                const formValues = state.get('formValues').toJS();
                formValues[action.name] = action.value;
                typeOptions.map(option => {
                    if (option.id === action.value.id) {
                        ['length', 'width', 'height'].map(field => {
                            formValues[field] = option.volume[field];
                            return field;
                        });
                        formValues.weight = option.weight;
                    }
                    return option;
                })
                return state.set('formValues', fromJS(formValues)).set('invalidFields', state.get('invalidFields').filter(field => field !== action.name));
            }
            return state.setIn(['formValues', action.name], action.value).set('invalidFields', state.get('invalidFields').filter(field => field !== action.name));
        }
        case UPDATE_ERRORS:
            return state.set('errors', action.errors).set('invalidFields', action.invalidFields);
        case RESET_FORM:
            return state
                .set('formValues', fromJS(initialForm))
                .set('errors', [])
                .set('invalidFields', []);
        case UPDATE_SUCCESS_FLAG:
            return state.set('addSuccessFlag', action.bool);
        case LOAD_VEHICLE_REQUEST_ACTION: {
            const parsed = queryString.parse(window.location.search);
            const currentPage = _get(parsed, 'page', 1);
            return state.setIn(['pagination', 'currentPage'], currentPage);
        }
        case LOAD_VEHICLE_SUCCESS_ACTION: {
            const vehicles = _get(action, 'data.data', []);
            let pagination = _get(action, 'data.meta.pagination', {});

            if (vehicles.length === 0) {
                pagination = {
                    perPage: 10,
                    total: 0,
                    currentPage: 0,
                };
            }
            return state.set('vehicles', vehicles).set('pagination', fromJS(pagination));
        }
        case LOAD_MAKE_MODEL_SUCCESS_ACTION: {
            const typeOptions = _get(action, 'data', []);
            return state.set('typeOptions', typeOptions);
        }
        case RESET_STATE:
            return initialState;
        default:
            return state;
    }
};

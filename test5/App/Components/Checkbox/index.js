import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import FAI from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
// Styles
import styles from './styles';
// Theme config
import { Colors } from '../../Themes';

class Checkbox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isChecked: props.isChecked,
    };

    this.onCheckboxToggle = this.onCheckboxToggle.bind(this);
  }

  onCheckboxToggle() {
    const { onPress, name } = this.props;
    const { isChecked } = this.state;

    this.setState({ isChecked: !isChecked });
    onPress({ name, isChecked: !isChecked });
  }

  render() {
    const { isChecked } = this.state;
    const { label } = this.props;

    return (
      <TouchableOpacity onPress={this.onCheckboxToggle} style={styles.container}>
        <View style={styles.checkboxBorder}>
          {isChecked ? <FAI name="check" color={Colors.main} size={10} /> : null}
        </View>
        <Text style={styles.checkboxLabel}>{label}</Text>
      </TouchableOpacity>
    );
  }
}

Checkbox.defaultProps = {
  name: '',
  isChecked: false,
  onPress: () => {},
  label: '',
};

Checkbox.propTypes = {
  name: PropTypes.string,
  isChecked: PropTypes.bool,
  onPress: PropTypes.func,
  label: PropTypes.string,
};

export default Checkbox;

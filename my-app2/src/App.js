
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';

import React, { Component } from 'react';
import Product from './Products';

class App extends Component {
    render() {
        return (
            <Provider>
                <Route exact path='/' component={Product}></Route>
                <Route path='/Login' component={Login} />
            </Provider>
        );
    }
}

export default App;
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Image, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import * as Animatable from 'react-native-animatable';
// Actions
import AppActions from '../../Modules/App/reducer';
// Images
import { Images } from '../../Themes';
// Styles
import styles from './styles';

class LaunchScreen extends Component {
  componentDidMount() {
    const { bootStrap } = this.props;
    bootStrap();
  }

  render() {
    const { style } = this.props;

    return (
      <View style={[styles.mainContainer, style]}>
        <Animatable.View
          style={styles.centered}
          animation="fadeIn"
          duration={2000}
          iterationCount="infinite"
        >
          <Image source={Images.launch} style={styles.logo} />
        </Animatable.View>
      </View>
    );
  }
}

LaunchScreen.defaultProps = {
  style: {},
};

LaunchScreen.propTypes = {
  bootStrap: PropTypes.func.isRequired,
  style: ViewPropTypes.style,
};

const mapDispatchToProps = dispatch => ({
  bootStrap: () => dispatch(AppActions.bootStrapApp()),
});

export default connect(null, mapDispatchToProps)(LaunchScreen);

import { fromJS } from 'immutable';
import makeSelectLogin, { selectLoginDomain } from '../selectors';

describe('selectLoginDomain', () => {
    it('should select the login state', () => {
        const loginState = fromJS({});
        const mockedState = fromJS({
            login: loginState,
        });
        expect(selectLoginDomain(mockedState)).toEqual(loginState);
    });
});

describe('makeSelectLogin', () => {
    it('makeSelectLogin', () => {
        const makeSelectLoginSelector = makeSelectLogin();
        const loginState = fromJS({});
        expect(makeSelectLoginSelector(loginState)).toEqual({});
    });
});

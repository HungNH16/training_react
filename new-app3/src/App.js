
import React, { Component } from 'react';

class FormComp extends Component {
    constructor(props) {
        super(props);
        this.getInitialState = this.getInitialState.bind(this)
        this.handleFNameChange = this.handleFNameChange.bind(this)
        this.handleLNameChange = this.handleLNameChange.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.state = this.getInitialState();
      }
    getInitialState(){
        return {lName: '',fName:'',Name:''};
    }
    handleFNameChange(event){
        this.setState({fName: event.target.value});
    }
    handleLNameChange(event){
        this.setState({lName: event.target.value});
    }
    handleClick(){
        var fullName = this.state.fName + ' ' + this.state.lName;
        this.setState({Name:fullName});
    }
    render() {
        return (
            <div>
            <label>First Name: </label>
            <input type="text" value = {this.state.fName} onChange={this.handleFNameChange}  />
            <br />

            <label>Last Name: </label>
            <input type="text" value = {this.state.lName} onChange={this.handleLNameChange} />
            <br />

            <input type="button" onClick={this.handleClick} value="Submit" />

            <hr />

            <h3>Your full name is </h3> {this.state.Name}
        </div>
        );
    }
}

export default FormComp;
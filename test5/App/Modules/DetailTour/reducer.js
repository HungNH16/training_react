import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------------Types and Action Creators-----------------*/

const { Types, Creators } = createActions({
  setTour1: ['payload'],
  setIsFetchingTour1: ['payload'],
  loadTour1: ['payload'],
});

export const TourTypes = Types;
export default Creators;

/* ------------------Initial State-----------------*/

export const INITIAL_STATE = Immutable({
  tour1: [],
  isFetchingTour1: true,
});

/* ------------------Reducers-----------------*/

// eslint-disable-next-line no-undef
const setTour1 = (state, { payload: tour1 }) => state.merge({ tour1 });
const setFetchingTourStatus1 = (state, { payload: isFetchingTour1 }) => state.merge({ isFetchingTour1 });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_TOUR1]: setTour1,
  [Types.SET_IS_FETCHING_TOUR1]: setFetchingTourStatus1,
});

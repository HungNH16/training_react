/*
 * Login Messages
 *
 * This contains all the text for the Login container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Login';

export default defineMessages({
    header: {
        id: `${scope}.header`,
        defaultMessage: 'This is the Login container!',
    },
    email_address: {
        id: `${scope}.email_address`,
        defaultMessage: 'Email Address',
    },
    password: {
        id: `${scope}.password`,
        defaultMessage: 'Password',
    },
    remember_me: {
        id: `${scope}.remember_me`,
        defaultMessage: 'Remember Me',
    },
    forgot_password: {
        id: `${scope}.forgot_password`,
        defaultMessage: 'Forgot Password',
    },
    login: {
        id: `${scope}.login`,
        defaultMessage: 'Login',
    },
    invalid_username_password: {
        id: `${scope}.invalid_username_password`,
        defaultMessage: 'Invalid username or password. Please try again.',
    },
    enter_username: {
        id: `${scope}.enter_username`,
        defaultMessage: 'Please enter your username.',
    },
    enter_password: {
        id: `${scope}.enter_password`,
        defaultMessage: 'Please enter your password.',
    },
    enter_username_password: {
        id: `${scope}.enter_email_password`,
        defaultMessage: 'Please enter your username and password.',
    },
    dispatcher_platform: {
        id: `${scope}.dispatcher_platform`,
        defaultMessage: 'Dispatcher Platform',
    },

});

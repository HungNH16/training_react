import React, { Component } from 'react';
import { View, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// Custom components
import LoadingIndicator from '../../Components/LoadingIndicator';
// Selectors
import { getLoading } from '../../Modules/App/selectors';
// Styles
import styles from './styles';
import ReduxNavigation from '../../Navigation/ReduxNavigation';
import StartupActions from '../../Redux/StartupRedux';

// Styles

class RootContainer extends Component {
  componentDidMount() {
    const { startup } = this.props;
    startup();
  }

  render() {
    const { isLoading } = this.props;

    return (
      <View style={styles.applicationView}>
        <StatusBar hidden barStyle="light-content" />
        <ReduxNavigation />
        <LoadingIndicator isShown={isLoading} style={styles.loadingIndicator} />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: getLoading(state),
});

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup()),
});

RootContainer.propTypes = {
  startup: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);

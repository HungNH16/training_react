import { fromJS } from 'immutable';
import { get as _get } from 'lodash';
import { alphabeticalSort } from '../../../utils';
import vehicleAssignmentReducer, { initialForm, initialState, convertToEditValues } from '../reducer';
import {
    ADD_ROW,
    DELETE_ROW,
    LOAD_DATA,
    LOAD_DATA_SUCCESS,
    LOAD_DRIVER,
    LOAD_DRIVER_SUCCESS,
    LOAD_VEHICLE,
    LOAD_VEHICLE_SUCCESS,
    RESET_STATE,
    REVERT_DATA,
    SUBMIT_DATA,
    SUBMIT_DATA_SUCCESS,
    UPDATE_DRIVER,
    UPDATE_ERRORS,
    UPDATE_STATUS,
    UPDATE_SUCCESS_FLAG,
    UPDATE_VEHICLE,
    VEHICLE_ASSIGNMENT_COLUMN_MAP,
} from '../constants';
import { LIST } from '../../../constants';

describe('Vehicle Assignment Reducer', () => {
    it('add new empty row in action ADD_ROW', () => {
        const actionData = {
            type: ADD_ROW,
        };
        const expectedState = fromJS(initialState).update('rowValues', rows => rows.concat(initialForm));
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('delete a specific row in action DELETE_ROW', () => {
        const id = 1;
        const actionData = {
            type: DELETE_ROW,
            id,
        };
        const expectedState = fromJS(initialState).update('rowValues', rows => rows.filter((_, index) => index !== id));
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('request vehicle assignment data from server in action LOAD_DATA', () => {
        const actionData = {
            type: LOAD_DATA,
        };
        const expectedState = fromJS(initialState)
            .set('data', [])
            .set('errors', [])
            .set('invalidFields', []);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('store vehicle assignment data in action LOAD_DATA_SUCCESS', () => {
        const response = {
            data: [],
        };
        const originalValues = convertToEditValues(response.data);
        const actionData = {
            type: LOAD_DATA_SUCCESS,
            response,
        };
        const expectedState = fromJS(initialState)
            .set('data', response.data)
            .set('rowValues', originalValues)
            .set('originalValues', originalValues);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('request driver options from server in action LOAD_DRIVER', () => {
        const actionData = {
            type: LOAD_DRIVER,
        };
        const expectedState = fromJS(initialState).set('drivers', []);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('store driver options in action LOAD_DRIVER_SUCCESS', () => {
        const response = {
            data: [],
        };
        const actionData = {
            type: LOAD_DRIVER_SUCCESS,
            response,
        };
        const expectedState = fromJS(initialState).set(
            'drivers',
            alphabeticalSort(
                response.data.map(row => ({
                    label: row.driverName,
                    value: row.driverID,
                    status: +row.status,
                })),
                ['label'],
            ),
        );
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('request vehicle options from server in action LOAD_VEHICLE', () => {
        const actionData = {
            type: LOAD_VEHICLE,
        };
        const expectedState = fromJS(initialState).set('vehicles', []);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('store vehicle options from server in action LOAD_VEHICLE_SUCCESS', () => {
        const response = {
            data: [],
        };
        const actionData = {
            type: LOAD_VEHICLE_SUCCESS,
            response,
        };
        const expectedState = fromJS(initialState).set(
            'vehicles',
            alphabeticalSort(
                response.data.map(row => ({
                    label: `${_get(row, 'licensePlate', '')}${row.makeModel ? ` - ${_get(row, 'makeModel.make', '')} ${_get(row, 'makeModel.model', '')}` : ''}`,
                    value: row.licensePlate,
                    status: +row.status,
                })),
                ['label'],
            ),
        );
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('reset vehicle assignment state in action RESET_STATE', () => {
        const actionData = {
            type: RESET_STATE,
        };
        const expectedState = fromJS(initialState);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('revert vehicle assignment editing values in action REVERT_DATA', () => {
        const actionData = {
            type: REVERT_DATA,
        };
        const expectedState = fromJS(initialState)
            .set('rowValues', convertToEditValues(fromJS(initialState).get('data')))
            .set('errors', [])
            .set('invalidFields', []);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('prepare for data submission in action SUBMIT_DATA', () => {
        const data = {
            status: 'Published',
            details: [
                {
                    driverID: 'DR000001',
                    driverStatus: 4,
                    vehicleLicensePlate: 'LP000001',
                    vehicleStatus: 4,
                },
            ],
        };
        const actionData = {
            type: SUBMIT_DATA,
            data,
        };
        const expectedState = fromJS(initialState)
            .set('errors', [])
            .set('invalidFields', [])
            .set('successFlag', false);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('show success pop-up in action SUBMIT_DATA_SUCCESS', () => {
        const response = {
            data: {
                categoryID: 1,
            },
        };
        const actionData = {
            type: SUBMIT_DATA_SUCCESS,
            response,
        };
        const expectedState = fromJS(initialState).set('successFlag', !!response.data.categoryID);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('update driver & driver status in action UPDATE_DRIVER', () => {
        const rowId = 1;
        const value = { label: 'Driver 1', value: 'DR000001', status: 2 };
        const actionData = {
            type: UPDATE_DRIVER,
            rowId,
            value,
        };
        const expectedState = fromJS(initialState)
            .update('rowValues', rows =>
                rows.map((row, rowIdx) => {
                    if (rowIdx === rowId) {
                        return {
                            ...row,
                            driverName: value,
                            driverStatus: [1, 2, 3, 4].includes(value.status) ? LIST.VA_DRIVER_STATUSES[3] : LIST.VA_DRIVER_STATUSES[4],
                        };
                    }
                    return row;
                }),
            )
            .update('invalidFields', fields =>
                fields.filter(field => field !== `data_${rowId}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_NAME}` && field !== `data_${rowId}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.DRIVER_STATUS}`),
            );
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('update error messages & invalid field list in action UPDATE_ERRORS', () => {
        const errors = [];
        const invalidFields = [];
        const actionData = {
            type: UPDATE_ERRORS,
            errors,
            invalidFields,
        };
        const expectedState = fromJS(initialState)
            .set('errors', errors)
            .set('invalidFields', invalidFields);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('update driver/vehicle status change in action UPDATE_STATUS', () => {
        const rowId = 1;
        const name = 'driverStatus';
        const value = 4;
        const actionData = {
            type: UPDATE_STATUS,
            rowId,
            name,
            value,
        };
        const expectedState = fromJS(initialState).update('rowValues', rows =>
            rows.map(
                (row, rowIdx) =>
                    rowIdx === rowId
                        ? {
                            ...row,
                            [name]: value,
                        }
                        : row,
            ),
        );
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('toggle success pop-up in action UPDATE_SUCCESS_FLAG', () => {
        const bool = false;
        const actionData = {
            type: UPDATE_SUCCESS_FLAG,
            bool,
        };
        const expectedState = fromJS(initialState).set('successFlag', bool);
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });

    it('update vehicle & vehicle status in action UPDATE_VEHICLE', () => {
        const rowId = 1;
        const value = { label: 'A1A123 - Audi Sedan', value: 'A1A123', status: 4 };
        const actionData = {
            type: UPDATE_VEHICLE,
            rowId,
            value,
        };
        const expectedState = fromJS(initialState)
            .update('rowValues', rows =>
                rows.map((row, rowIdx) => {
                    if (rowIdx === rowId) {
                        return {
                            ...row,
                            vehicle: value,
                            vehicleStatus: [1, 2, 3, 4].includes(value.status) ? LIST.VA_VEHICLE_STATUSES[3] : LIST.VA_VEHICLE_STATUSES[4],
                        };
                    }
                    return row;
                }),
            )
            .update('invalidFields', fields =>
                fields.filter(field => field !== `data_${rowId}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE}` && field !== `data_${rowId}_${VEHICLE_ASSIGNMENT_COLUMN_MAP.VEHICLE_STATUS}`),
            );
        expect(vehicleAssignmentReducer(undefined, actionData)).toEqual(expectedState);
    });
});
